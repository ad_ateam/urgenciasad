
package com.iespacomolla.urg.servicio;

import com.iespacomolla.urg.entidades.Medico;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Adrian
 */

public class MedicoServiceTest
{
    static String login = "root";
    static String pass = "root";
    static String url = "jdbc:mysql://localhost/urgenciasTest";
    static String deleteMedicos = "DELETE from Medicos;";

    private static MedicoService sf = null;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
        sf = new MedicoService();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception
    {
        sf.finalizar();
    }

    @Before
    public void setUp() throws Exception
    {
        Connection conn = null;
        PreparedStatement ps = null;

        try
        {
            conn = DriverManager.getConnection(url, login, pass);
            ps = conn.prepareStatement(deleteMedicos);
            ps.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new Exception("No se han podido iniciar los tests" + e);
        }
        finally
        {
            try
            {
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            }
            catch (SQLException sqle)
            {  
                throw new Exception("No se ha podido cerrar la base de datos" + sqle);
            }
        }
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testNuevoMedicoOK()
    {
        try
        {
            sf.nuevoMedico("Manolo");

            List<Medico> medicos = sf.ObtenerTodosMedicos();

            if (medicos.size() != 1)
            {
                fail("TEST NO SUPERADO: No se ha insertado el medico");
            }

            assertNotNull(medicos.get(0).getIdMedico());
            assertNotNull(medicos.get(0).getNombre());
            assertNotNull(medicos.get(0).getEstado());

        }
        catch (MedicoServiceException e)
        {
            fail("TEST NO SUPERADO: No se han insertado el usuario al producirse una excepcion: "
                 + e.getLocalizedMessage());
        }
    }

    @Test(expected = MedicoServiceException.class)
    public void testNuevoMedicoConNombreCorto() throws Exception
    {
        sf.nuevoMedico("Manu");
    }

    @Test(expected = UsuarioServiceException.class)
    public void testActualizarMedico() throws Exception
    {
        sf.actualizarMedico(1,"Manuel","DISPONIBLE");
    }

    private int llenarMedicos() throws Exception
    {
        try
        {
            sf.nuevoMedico("Angel");
            sf.nuevoMedico("Adrian");
            sf.nuevoMedico("Saulencio");
            sf.nuevoMedico("Pedro");

            return 4;
        }
        catch (MedicoServiceException e)
        {
            throw new Exception("Error al llenar", e);
        }
    }

    @Test
    public void testGetMedicos()
    {
        try
        {
            int size = llenarMedicos();

            List<Medico> medicos = sf.ObtenerTodosMedicos();

            // No se han guardado
            if (medicos.size() != size)
            {
                fail("TEST NO SUPERADO: No se han obtenido los medicos esperados (" + size + ")");
            }

            for (Medico u : medicos)
            {
                assertNotNull(u.getIdMedico());
                assertNotNull(u.getNombre());
                assertNotNull(u.getEstado());
            }

        }
        catch (Exception e)
        {
            fail("TEST NO SUPERADO: No se han obtenido los medicos al producirse una excepcion: "
                 + e.getLocalizedMessage());
        }
    }


    @Test
    public void testEliminaMedico()
    {
        try
        {
            int size = llenarMedicos();

            List<Medico> medicos = sf.ObtenerTodosMedicos();

            if (medicos.size() == size)
            {
                sf.eliminaMedico(medicos.get(0).getIdMedico());

                medicos = sf.ObtenerTodosMedicos();

                if (medicos.size() != size - 1)
                {
                    fail("TEST NO SUPERADO: No se ha borrado el medico esperado");
                }
            }
            else
            {
                fail("TEST NO SUPERADO: No se han obtenido medicos");
            }

        }
        catch (Exception e)
        {
            fail("TEST NO SUPERADO: No se ha eliminado el medico al producirse una excepcion: "
                 + e.getLocalizedMessage());
        }
    }
    
    @Test
    public void testObtenerMedicosServicio()
    {
        try
        {
            llenarMedicos();

            int contMedicos = 0;

            List<Medico> medicos = sf.ObtenerTodosMedicos();

            for (Medico u : medicos)
            {
                if(contMedicos < 1)
                {
                    sf.actualizarMedico(u.getIdMedico(), u.getNombre(), "DISPONIBLE");
                }
                if(contMedicos==1)
                {
                    sf.actualizarMedico(u.getIdMedico(), u.getNombre(), "OCUPADO");  
                }
                contMedicos++;
            }

            List<Medico> medicosActivos = sf.ObtenerMedicosServicio();



            // No se han guardado
            if (medicosActivos.size() != 2)
            {
                fail("TEST NO SUPERADO: No se han obtenido los medicos esperados (" + 2 + ")");
            }
            }
            catch (Exception e)
            {
                fail("TEST NO SUPERADO: No se han obtenido los medicos al producirse una excepcion: "
                                 + e.getLocalizedMessage());
            }
    }
}
