package com.iespacomolla.urg.servicio;

import com.iespacomolla.urg.entidades.TipoRol;
import com.iespacomolla.urg.entidades.Usuario;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Saul, Angel
 */
public class UsuarioServiceTest
{
    static String login = "root";
    static String pass = "root";
    static String url = "jdbc:mysql://localhost/urgenciasTest";
    static String deleteUsuarios = "DELETE from Usuarios;";
    
    private static UsuarioService sf = null;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
        sf = new UsuarioService();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception
    {
        sf.finalizar();
    }

    @Before
    public void setUp() throws Exception
    {
        Connection conn = null;
	PreparedStatement ps = null;
        
        try
        {
            conn = DriverManager.getConnection(url, login, pass);
            ps = conn.prepareStatement(deleteUsuarios);
            ps.executeUpdate();
	} 
        catch (SQLException e) 
        {
	    throw new Exception("No se han podido iniciar los tests" + e);
        }
        finally 
        {
            try 
            {
                if (ps != null)
                    ps.close();
                if (conn != null)
                    conn.close();
            } 
            catch (SQLException sqle) 
            {	
                throw new Exception("No se ha podido cerrar la base de datos" + sqle);
            }
        }
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testNuevoUsuarioOK()
    {
        try
        {
            sf.nuevoUsuario("Bartolo", "Bartolin", "Bartolopassword", "ADMINISTRATIVO");

            List<Usuario> usuarios = sf.obtenerTodosUsuarios();

            if (usuarios.size() != 1)
            {
                fail("TEST NO SUPERADO: No se ha insertado el usuario");
            }

            assertNotNull(usuarios.get(0).getId());
            assertNotNull(usuarios.get(0).getNombre());
            assertNotNull(usuarios.get(0).getUsu());
            assertNotNull(usuarios.get(0).getPwd());

        }
        catch (UsuarioServiceException e)
        {
            fail("TEST NO SUPERADO: No se han insertado el usuario al producirse una excepcion: "
                 + e.getLocalizedMessage());
        }
    }

    @Test(expected = UsuarioServiceException.class)
    public void testNuevoUsuarioConNombreCorto() throws Exception
    {
        sf.nuevoUsuario("Bart", "Bartolin", "Bartolopassword", "ADMINISTRATIVO");
    }

    @Test(expected = UsuarioServiceException.class)
    public void testNuevoUsuarioConUsuarioCorto() throws Exception
    {
        sf.nuevoUsuario("Bartolo", "Bart", "Bartolopassword", "ADMINISTRATIVO");
    }

    @Test(expected = UsuarioServiceException.class)
    public void testNuevoUsuarioConContraseñaCorta() throws Exception
    {
        sf.nuevoUsuario("Bartolo", "Bartolin", "123", "ADMINISTRATIVO");
    }

    @Test(expected = UsuarioServiceException.class)
    public void testNuevoUsuarioConRolNoValido() throws Exception
    {
        sf.nuevoUsuario("Bart", "Bartolin", "Bartolopassword", "JEFE");
    }

    public void testActualizarUsuarioConContraseñaCorta()
    {
        try
        {
            int id = sf.nuevoUsuario("Nombre", "Usuario", "contrasena", TipoRol.SANITARIO.name());
            sf.actualizarUsuario(id, "Bartolo", "Bartolin", "123", "ADMINISTRATIVO");
        }
        catch (UsuarioServiceException e)
        {
            fail("Se ha producido una excepción: " + e.getMessage());
        }
    }

    @Test(expected = UsuarioServiceException.class)
    public void testActualizarUsuarioConNombreCorto() throws Exception
    {
        sf.actualizarUsuario(7, "Bart", "Bartolin", "123456", "ADMINISTRATIVO");
    }

    private int llenarUsuarios() throws Exception
    {
        try
        {
            sf.nuevoUsuario("Angel", "aag84", "12345", TipoRol.SANITARIO.name());
            sf.nuevoUsuario("Adrian", "adr33", "12345", TipoRol.ADMINISTRATIVO.name());
            sf.nuevoUsuario("Saulencio", "saulencio", "12345", TipoRol.SANITARIO.name());
            sf.nuevoUsuario("Pedro", "pserena", "12345", TipoRol.DIRECTIVO.name());

            return 4;
        }
        catch (UsuarioServiceException e)
        {
            throw new Exception("Error al llenar", e);
        }
    }

    @Test
    public void testGetUsuarios()
    {
        try
        {
            int size = llenarUsuarios();

            List<Usuario> usuarios = sf.obtenerTodosUsuarios();

            // No se han guardado
            if (usuarios.size() != size)
            {
                fail("TEST NO SUPERADO: No se han obtenido los usuarios esperados (" + size + ")");
            }

            for (Usuario u : usuarios)
            {
                assertNotNull(u.getId());
                assertNotNull(u.getNombre());
                assertNotNull(u.getUsu());
                assertNotNull(u.getPwd());
            }

        }
        catch (Exception e)
        {
            fail("TEST NO SUPERADO: No se han obtenido los usuarios al producirse una excepcion: "
                 + e.getLocalizedMessage());
        }
    }
    
    @Test
    public void testGetUsuariosAdministrativos()
    {
        try
        {
            llenarUsuarios();

            List<Usuario> usuarios = sf.obtenerUsuariosAdministradores();

            // No se han guardado
            if (usuarios.size() != 1)
            {
                fail("TEST NO SUPERADO: No se han obtenido los usuarios esperados (1)");
            }
        }
        catch (Exception e)
        {
            fail("TEST NO SUPERADO: No se han obtenido los usuarios al producirse una excepcion: "
                 + e.getLocalizedMessage());
        }
    }
    
    @Test(expected = UsuarioServiceException.class)
    public void testEliminarAdministrativo() throws UsuarioServiceException
    {
        int id = 0;
        try
        {
            id = sf.nuevoUsuario("Nombre", "Usuario", "12345", TipoRol.ADMINISTRATIVO.name());
        }
        catch (UsuarioServiceException e)
        {
            fail("Se ha producido una excepcion: " + e.getLocalizedMessage());
        }
        
        // Al ejecutar esto tendra que dar una excepción
        sf.eliminarUsuario(id);
    }

    @Test
    public void testEliminarUsuario()
    {
        try
        {
            int size = llenarUsuarios();

            List<Usuario> usuarios = sf.obtenerTodosUsuarios();

            if (usuarios.size() == size)
            {
                sf.eliminarUsuario(usuarios.get(0).getId());

                usuarios = sf.obtenerTodosUsuarios();
                
                if (usuarios.size() != size - 1)
                {
                    fail("TEST NO SUPERADO: No se ha borrado el usuario esperado");
                }
            }
            else
            {
                fail("TEST NO SUPERADO: No se han obtenido usuarios");
            }

        }
        catch (Exception e)
        {
            fail("TEST NO SUPERADO: No se ha eliminado el usuario al producirse una excepcion: "
                 + e.getLocalizedMessage());
        }
    }

}
