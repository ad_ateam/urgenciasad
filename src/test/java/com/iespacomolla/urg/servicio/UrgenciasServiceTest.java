/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iespacomolla.urg.servicio;

import com.iespacomolla.urg.datos.IncidenciaDAO;
import com.iespacomolla.urg.datos.IncidenciaDAOException;
import com.iespacomolla.urg.datos.PacienteDAO;
import com.iespacomolla.urg.entidades.EstadoIncidencia;
import static com.iespacomolla.urg.entidades.EstadoIncidencia.ACTIVA;
import static com.iespacomolla.urg.entidades.EstadoIncidencia.HISTORICA;
import com.iespacomolla.urg.entidades.EstadoMedico;
import com.iespacomolla.urg.entidades.Incidencia;
import com.iespacomolla.urg.entidades.Medico;
import com.iespacomolla.urg.entidades.Paciente;
import com.iespacomolla.urg.entidades.TipoIncidencia;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.hql.internal.antlr.HqlSqlTokenTypes;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ANDRES Angel
 */
public class UrgenciasServiceTest
{
    private final String base = "urgenciasTest";
    private final String login = "root";
    private final String pass = "root";
    private final String url = "jdbc:mysql://localhost/" + base;
    private final String deleteIncidencias = "DELETE FROM Incidencias;";
    private final String deleteMedicos = "DELETE FROM Medicos;";
    private final String deletePacientes = "DELETE FROM Pacientes;";
    private UrgenciasService us = null;
    private IncidenciaDAO idao = null;

    public UrgenciasServiceTest()
    {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {

    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() throws Exception
    {
        Connection conn = null;
        PreparedStatement prs = null;

        try
        {
            conn = DriverManager.getConnection(url, login, pass);

            prs = conn.prepareStatement(deleteIncidencias);
            prs.executeUpdate();
            prs.close();

            prs = conn.prepareStatement(deleteMedicos);
            prs.executeUpdate();
            prs.close();

            prs = conn.prepareStatement(deletePacientes);
            prs.executeUpdate();
            prs.close();
        }
        catch (SQLException e)
        {
            throw new Exception("No se han podido iniciar los tests", e);
        }
        finally
        {
            try
            {
                conn.close();
            }
            catch (SQLException sqle)
            {
                throw new Exception("No se ha podido cerrar la base de datos", sqle);
            }
        }

        try
        {
            us = new UrgenciasService();
            idao = new IncidenciaDAO();
        }
        catch (UrgenciasServiceException ex)
        {
            throw new Exception("No se pudo instanciar el UrgenciasService", ex);
        }
    }

    @After
    public void tearDown() throws Exception
    {
        try
        {
            us.finalizar();
            idao.finalizar();
        }
        catch (Exception ex)
        {
            throw new Exception("Error al finalizar UrgenciasService", ex);
        }
    }

    /**
     * Test of ComenzarTurno
     */
    @Test
    public void testComenzarTurno()
    {
        System.out.println("ComenzarTurno");

        try
        {
            //Creo un nuevo medico
            int im = us.getServicioMedico().nuevoMedico("Fernando");
            //Inicio un turno para el nuevo medico
            us.ComenzarTurno(im);

            Medico m = us.getServicioMedico().ObtenerMedico(im);
            if (m.getEstado() != EstadoMedico.DISPONIBLE)
                fail("El médico no ha comenzado su turno");                

            int ip = us.getServicioPaciente().nuevoPaciente("Paciente1", "111111111111");
            us.registrarIncidenciaGrave(ip, "bla bla bla");
            
            try
            {
                us.ComenzarTurno(im);
                fail("No puede comenzar el turno, esta atendiendo una incidencia");
            }
            catch (UrgenciasServiceException ex)
            {
                // Aqui todo bien
            }
        }
        
        catch (MedicoServiceException | PacienteServiceException | UrgenciasServiceException e)
        {
            fail("Error en comenzar turno " + e.getLocalizedMessage());
        }
    }
    
    @Test
    public void testComenzarTurnoConIncidenciasEnEspera()
    {
        System.out.println("ComenzarTurno");

        try
        {
            int im1 = us.getServicioMedico().nuevoMedico("Fernando");
            int im2 = us.getServicioMedico().nuevoMedico("Perico");
         
            // Solo el medico 1 comienza el turno
            us.ComenzarTurno(im1);
            
            // Paciente 1 atendido por el medico 1
            int ip1 = us.getServicioPaciente().nuevoPaciente("Paciente1", "111111111111");
            us.registrarIncidenciaGrave(ip1, "bla bla bla");
            
            Medico m = us.getServicioMedico().ObtenerMedico(im1);
            if (m.getEstado() != EstadoMedico.OCUPADO)
                fail("El médico no ha comenzado su turno");  
            
            // Paciente 2 en incidencia en espera
            int ip2 = us.getServicioPaciente().nuevoPaciente("Paciente1", "111111111111");
            us.registrarIncidenciaLeve(ip2, "bla bla bla 2");

            //Inicio un turno para el nuevo medico
            us.ComenzarTurno(im2);

            m = us.getServicioMedico().ObtenerMedico(im2);
            if (m.getEstado() == EstadoMedico.DISPONIBLE)
                fail("El médico no ha atendido la incidencia 2");                
        }
        
        catch (MedicoServiceException | PacienteServiceException | UrgenciasServiceException e)
        {
            fail("Error en comenzar turno " + e.getLocalizedMessage());
        }
    }

    /**
     * Test of FinalizarTurno method, of class UrgenciasService.
     */
    @Test
    public void testFinalizarTurno()
    {
        try
        {
            System.out.println("FinalizarTurno");
            //Creo un medico m que esta en su turno
            //Medico m = new Medico(5,"Pepe",EstadoMedico.OCUPADO);
            int i = us.getServicioMedico().nuevoMedico("Perico");
            us.getServicioMedico().actualizarMedico(i, "Perico", EstadoMedico.DISPONIBLE.toString());

            //Fializo el turno de el medico credo
            us.FinalizarTurno(i);

            Medico m = us.getServicioMedico().ObtenerMedico(i);
            if (m.getEstado() != EstadoMedico.FUERA_SERV)
            {
                fail("El médico no ha terminado su turno");
            }

        }
        catch (MedicoServiceException | UrgenciasServiceException e)
        {
            fail("TEST NO SUPERADO: No se ha podido cerrar la icidencia del medico");
        }
    }

    /**
     * Test of EstaAten
     */
    @Test
    public void testEstaAtendiendo()
    {
        try
        {
            // Creamos el paciente objeto de la incidencia
            int ip = us.getServicioPaciente().nuevoPaciente("Pepillo Gomez", "234322346532");

            // Alta del medico disponible para asignarlo a la futura incidencia
            int im = us.getServicioMedico().nuevoMedico("Enrique Jimenez");
            us.getServicioMedico().actualizarMedico(im, "Enrique Jimenez", EstadoMedico.DISPONIBLE.toString());

            // Crea una incidencia para el paciente anterior y debe asignar al medico DISPONIBLE
            us.registrarIncidenciaLeve(ip, "bla bla");

            // Si el medico no esta atendiendo la incidencia no se ha relacionado correctamente
            if (us.EstaAtendiendo(im) == false)
            {
                fail("TEST FAIL: No se pudo determinar si el medico indicado estaba atendiendo una incidencia con exito");
            }
        }

        catch (Exception e)
        {
            fail("TEST FAIL: No se pudo realizar la comprobacion debido a una excepcion" + e);
        }
    }

    /**
     * Test of finalizar method, of class UrgenciasService.
     */
    @Test
    public void testFinalizar() throws Exception
    {
        System.out.println("finalizar");

        us.finalizar();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of registrarIncidenciaLeve method, of class UrgenciasService.
     */
    @Test
    public void testRegistrarIncidenciaLeve() throws Exception
    {
        try
        {
            System.out.println("registrarIncidenciaLeve");
            //Paciente p= new Paciente();
            
            int ip = us.getServicioPaciente().nuevoPaciente("Juan Lopez","234322346532");
            
            List<Paciente> listaPacientes=us.getServicioPaciente().obtenerTodosPacientes();
            
            if (listaPacientes.isEmpty())
                fail("No se ha registrado paciente");
            
            /*for(Paciente pa: listaPacientes)
            {
                p=pa;
                break;
            }*/
        
            us.registrarIncidenciaLeve(ip, "Se ha roto la nariz");
            
            List<Incidencia>incidencias=us.obtenerTodasIncidencias();
            
            if(incidencias.size()!=1)
                fail("TEST FAIL: No se dio de alta la incidencia con exito");
            
            if(incidencias.get(0).getTipo() != TipoIncidencia.LEVE)
            {
                fail("TEST FAIL: La incidencia no se creo con los parametros oportunos");
            }
            
            /*for(Incidencia in : incidencias)
            {
                if(in.getTipo()!=TipoIncidencia.LEVE)
                {
                    fail("TEST FAIL: La incidencia no se creo con los parametros oportunos");
                    break;
                }
            }*/
        }
        catch(Exception e)
        {
            fail("TEST FAIL: No se ha podido registrar la incidencia Leve");
        }
    }

    /**
     * Test of registrarIncidenciaGrave method, of class UrgenciasService.
     */
    @Test
    public void testRegistrarIncidenciaGrave() throws Exception
    {
        try
        {
            int ip = us.getServicioPaciente().nuevoPaciente("Pepillo Gomez", "234322346532");

            int im = us.getServicioMedico().nuevoMedico("Enrique Jimenez");
            us.ComenzarTurno(im);

            us.registrarIncidenciaGrave(ip, "bla bla GRAVE");
            List<Incidencia> incidencias = us.obtenerTodasIncidencias();

            if (incidencias.size() != 1)
            {
                fail("TEST FAIL: No se dio de alta la incidencia con exito");
            }

            if (incidencias.get(0).getTipo() != TipoIncidencia.GRAVE ||
                incidencias.get(0).getEstado() != EstadoIncidencia.ACTIVA)
            {
                fail("TEST FAIL: La incidencia no se creo con los parametros oportunos");
            }
        }

        catch (Exception e)
        {
            fail("TEST FAIL: No se pudo registrar la incidencia debido a una excepcion" + e);
        }
    }

    @Test
    public void testRegistrarIncidenciaCritica_MedicoLibre()
    {
        try
        {
            System.out.println("testRegistrarIncidenciaCritica_MedicoLibre");

            // Alta de 2 medicos
            int im1 = us.getServicioMedico().nuevoMedico("Medicastro1");
            int im2 = us.getServicioMedico().nuevoMedico("Medicastro2");

            // Solo el medico 2 comienza su turno
            us.ComenzarTurno(im2);

            // Nuevo paciente a atender
            int ip = us.getServicioPaciente().nuevoPaciente("El enfermito", "123456789012");

            // Registra la urgencia para el paciente, deberá asignarlo al medico 2
            int iug = us.registrarIncidenciaCritica(ip, "Esta que se muere");

            // Busca la incidencia en criticas y activas
            List<Incidencia> l_ca = us.obtenerIncidencias(TipoIncidencia.CRITICA, EstadoIncidencia.ACTIVA);
            if (l_ca.isEmpty())
            {
                fail("La incidencia no ha sido registrada");
            }

            Incidencia i = l_ca.get(0);

            // Comprueba que los datos del medico asociado a la incidencia. Debe ser el 2
            assertNotNull(i.getMedico());
            assertEquals(i.getMedico().getIdMedico(), im2);
            assertEquals(i.getMedico().getEstado(), EstadoMedico.OCUPADO);

            assertNotNull(i.getPaciente());
            assertNotNull(i.getFechaEntrada());
            assertNotNull(i.getFechaAtencion());
        }
        catch (MedicoServiceException | UrgenciasServiceException | PacienteServiceException e)
        {
            fail("Exception : " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testRegistrarIncidenciaCritica_MedicoEnLeves()
    {
        try
        {
            System.out.println("testRegistrarIncidenciaCritica_MedicoEnLeves");

            int im = us.getServicioMedico().nuevoMedico("Medicastro");
            us.ComenzarTurno(im);

            int ip1 = us.getServicioPaciente().nuevoPaciente("El enfermito", "123456789012");
            us.registrarIncidenciaLeve(ip1, "No esta mal");

            int ip2 = us.getServicioPaciente().nuevoPaciente("El que se muere", "123456789013");
            // Aqui deberá pasar la incidencia leve a espera y meterla en la cola
            us.registrarIncidenciaCritica(ip2, "Esta que se muere");

            // Lista de criticas activas
            List<Incidencia> l_ca = us.obtenerIncidencias(TipoIncidencia.CRITICA, EstadoIncidencia.ACTIVA);
            if (l_ca.isEmpty())
            {
                fail("La incidencia no ha sido registrada");
            }

            Incidencia i = l_ca.get(0);

            assertNotNull(i.getMedico());
            assertEquals(i.getMedico().getEstado(), EstadoMedico.OCUPADO);

            assertNotNull(i.getPaciente());
            assertNotNull(i.getFechaEntrada());
            assertNotNull(i.getFechaAtencion());

            // List de incidenicas leves en espera
            List<Incidencia> l_e = us.obtenerIncidencias(TipoIncidencia.LEVE, EstadoIncidencia.ESPERA);
            if (l_e.isEmpty())
            {
                fail("La incidencia leve no esta en espera");
            }

            if (us.getColaIncidenciasNormales().size() == 0)
            {
                fail();
            }

            assertEquals(us.getColaIncidenciasNormales().peek().getIdIncidencia(), l_e.get(0).getIdIncidencia());
        }
        catch (MedicoServiceException | UrgenciasServiceException | PacienteServiceException e)
        {
            fail("Exception : " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testRegistrarIncidenciaCritica_MedicoEnGraves()
    {
        try
        {
            System.out.println("testRegistrarIncidenciaCritica_MedicoEnGraves");

            int im = us.getServicioMedico().nuevoMedico("Medicastro");
            us.ComenzarTurno(im);

            int ip1 = us.getServicioPaciente().nuevoPaciente("El enfermito grave", "123456789012");
            us.registrarIncidenciaGrave(ip1, "Esta jodidillo");

            int ip2 = us.getServicioPaciente().nuevoPaciente("El que se muere", "123456789013");
            // Aqui deberá pasar la incidencia grave a espera y meterla en la cola
            us.registrarIncidenciaCritica(ip2, "Esta que se muere");

            // Lista de criticas activas
            List<Incidencia> l_ca = us.obtenerIncidencias(TipoIncidencia.CRITICA, EstadoIncidencia.ACTIVA);
            if (l_ca.isEmpty())
            {
                fail("La incidencia no ha sido registrada");
            }

            Incidencia i = l_ca.get(0);

            assertNotNull(i.getMedico());
            assertEquals(i.getMedico().getEstado(), EstadoMedico.OCUPADO);

            assertNotNull(i.getPaciente());
            assertNotNull(i.getFechaEntrada());
            assertNotNull(i.getFechaAtencion());

            // List de incidenicas graves en espera
            List<Incidencia> l_e = us.obtenerIncidencias(TipoIncidencia.GRAVE, EstadoIncidencia.ESPERA);
            if (l_e.isEmpty())
            {
                fail("La incidencia grae no esta en espera");
            }

            if (us.getColaIncidenciasGraves().size() == 0)
            {
                fail();
            }

            assertEquals(us.getColaIncidenciasGraves().peek().getIdIncidencia(), l_e.get(0).getIdIncidencia());
        }
        catch (MedicoServiceException | UrgenciasServiceException | PacienteServiceException e)
        {
            fail("Exception : " + e.getLocalizedMessage());
        }
    }

    @Test
    public void testRegistrarIncidenciaCritica_SinMedicos()
    {
        System.out.println("RegistrarIncidenciaCritica_SinMedicos");
        
        int ip1 = 0, ip2 = 0, im = 0;
        try
        {
            ip1 = us.getServicioPaciente().nuevoPaciente("El que se muere", "123456789012");
            ip2 = us.getServicioPaciente().nuevoPaciente("El que se muere tambien", "123456789013");
            
            // Alta de medico, por defecto fuera de servicio
            im = us.getServicioMedico().nuevoMedico("Medicastro");
        }
        catch (PacienteServiceException | MedicoServiceException e)
        {
            fail("Error en preparación del test: " + e.getLocalizedMessage());
        }
        
        try
        {
            us.registrarIncidenciaCritica(ip1, "Esta que se muere");
            fail("Error: Sin medicos no debe registrar incidencia");
        }
        catch (UrgenciasServiceException e)
        {
            // Si salta el error es correcto
        }
        
        try
        {
            us.ComenzarTurno(im);
            us.registrarIncidenciaCritica(ip1, "Ahora si debe registrar la incidencia");
        }
        catch (UrgenciasServiceException ex)
        {
            fail("Error: la incidencia si debe registrarse, hay medico disponible. " + ex.getLocalizedMessage());
        }
        
        try
        {
            us.registrarIncidenciaCritica(ip2, "Este se muere fijo, no hay medicos");
            fail("Error, la segunda incidencia no debe registrarse. Solo hay 1 medico y esta ocupado con una incidencia critica");
        }
        catch (UrgenciasServiceException ex)
        {
            // Aqui todo bien
        }
    }

    /**
     * Test of obtenerTodasIncidencias method, of class UrgenciasService.
     */
    @Test
    public void testObtenerTodasIncidencias() throws Exception
    {
        System.out.println("obtenerTodasIncidencias");
        try
        {
            int size = llenarIncidencias();

            List<Incidencia> incidencias = us.obtenerTodasIncidencias();

            // No se han guardado
            if (incidencias.size() != size)
            {
                fail("TEST NO SUPERADO: No se han obtenido las incidencias (" + size + ")");
            }

            for (Incidencia i : incidencias)
            {
                assertNotNull(i.getPaciente());
                assertNotNull(i.getEstado());
                assertNotNull(i.getDescripcion());
                assertNotNull(i.getFechaEntrada());
                //No se si tengo que poner el resto de cosas, porque algunas pueden ser null

            }

            if (incidencias.get(0).getTipo() != TipoIncidencia.LEVE ||
                incidencias.get(1).getTipo() != TipoIncidencia.LEVE ||
                incidencias.get(2).getTipo() != TipoIncidencia.GRAVE ||
                incidencias.get(3).getTipo() != TipoIncidencia.CRITICA)
                fail("Las incidencias no se han registrado con el tipo correcto");
        }
        catch (Exception e)
        {
            fail("TEST NO SUPERADO: No se han obtenido las incidencias al producirse una excepcion: "
                 + e.getLocalizedMessage());
        }

    }

    /**
     * Test of obtenerIncidencias method, of class UrgenciasService.
     */
    @Test
    public void testObtenerIncidencias() throws Exception
    {

        int i;
        int j;
        EstadoIncidencia estado;
        TipoIncidencia tipo;
        Incidencia in;
        List<Incidencia> listaIncidencias;

        try
        {
            for (i = 0; i < EstadoIncidencia.values().length; i++)
            {
                estado = EstadoIncidencia.values()[i];

                for (j = 0; j < TipoIncidencia.values().length; j++)
                {
                    tipo = TipoIncidencia.values()[j];
                    in = new Incidencia();
                    in.setEstado(estado);
                    in.setTipo(tipo);
                    idao.addIncidencia(in);
                }
            }

            for (i = 0; i < EstadoIncidencia.values().length; i++)
            {
                estado = EstadoIncidencia.values()[i];

                for (j = 0; j < TipoIncidencia.values().length; j++)
                {
                    tipo = TipoIncidencia.values()[j];
                    listaIncidencias = us.obtenerIncidencias(tipo, estado);

                    if (listaIncidencias.size() != 1)
                    {
                        fail("TEST FAIL: El numero de incidencias almacenadas de tipo " + tipo.toString() + " y con estado " + estado.toString() + " no es el correcto");
                    }
                }
            }
        }

        catch (Exception e)
        {
            fail("No se pudieron obtener las incidencias debido a un error" + e);
        }
    }

    /**
     * Test of listaIncidenciasResueltasMedico method, of class UrgenciasService.
     */
    @Test
    public void testListaIncidenciasResueltasMedico() throws Exception
    {
        System.out.println("Listar incidencias resultas de un medico");

        List<Incidencia> incidenciasResueltas=null;
       
        int p1 = 0, p2 = 0, p3 = 0, m = 0;
        try{
            //incidenciasResueltas=new ArrayList<>();
            
            // Alta de medico, por defecto fuera de servicio
            //y a continuacion comenzamos su turno
            m = us.getServicioMedico().nuevoMedico("Best medico");
            us.ComenzarTurno(m);
            
            //Creamos 3 pacientes 
            p1 = us.getServicioPaciente().nuevoPaciente("El chalao", "111111111111");
            p2 = us.getServicioPaciente().nuevoPaciente("El otro", "222222222222");
            p3 = us.getServicioPaciente().nuevoPaciente("El tuerto", "333333333333");
            
            //Registramos las 3 incidencias de los nuevos pacientes
            us.registrarIncidenciaLeve(p1, "Esta chalao");
            us.finalizarIncidencia(p1);
            us.registrarIncidenciaGrave(p2, "otro chalao");
            us.finalizarIncidencia(p2);
            us.registrarIncidenciaLeve(p3, "Esta tuerto");
            
            incidenciasResueltas=us.listaIncidenciasResueltasMedico(m);
            
            if(incidenciasResueltas.size()!=2)
            {
                fail("No se han obtenido las incidencias esperadas");
            }
        }catch(Exception e){
            throw new UrgenciasServiceException("No se pudo obtener la lista de incidencias resueltas por el medico indicado",e);
        }
    }

    /**
     * Test of ListaIncidenciasEnEspera method, of class UrgenciasService.
     */
    @Test
    public void testListaIncidenciasEnEspera() throws Exception
    {
        try
        {
            System.out.println("ListaIncidenciasEnEspera");
            
            List<Incidencia> lista = new ArrayList<Incidencia>();
            int p1,p2,p3;
            p1 = us.getServicioPaciente().nuevoPaciente("Marcos Lopez","234322346532");
            p2 = us.getServicioPaciente().nuevoPaciente("Alvaro Gomez","234322346532");
            p3 = us.getServicioPaciente().nuevoPaciente("Alvaro Gomez","234322346532");
            
            int idmed=us.getServicioMedico().nuevoMedico("Nick Riviera");
            us.getServicioMedico().actualizarMedico(idmed, "Nick Riviera", EstadoMedico.DISPONIBLE.toString());
            idmed=us.getServicioMedico().nuevoMedico("Fred Lovejoy");
            us.getServicioMedico().actualizarMedico(idmed, "Fred Lovejoy", EstadoMedico.DISPONIBLE.toString());
            us.registrarIncidenciaLeve(p1, "Se ha roto la mano");
            us.registrarIncidenciaGrave(p2, "Se ha roto la espalda");
        
            lista = us.ListaIncidenciasEnEspera();
            if(lista.size() != 0)
                fail("TEST FAIL: La lista de espera ha tenido un error");
            
            us.registrarIncidenciaCritica(p3, "Que se nos muere!");
            
            lista = us.ListaIncidenciasEnEspera();
            if(lista.size() != 1)
                fail("TEST FAIL: La lista de espera ha tenido un error");
        }
        catch(Exception e)
        {        
            fail("TEST FAIL: No se han podido obtener las incidencias en espera debido a una excepcion" + e.getLocalizedMessage());
        }
    }

    /**
     * Test of finalizarIncidencia method, of class UrgenciasService.
     */
    @Test
    public void testFinalizarIncidencia()
    {
        try
        { 
            int idmed=us.getServicioMedico().nuevoMedico("Nick Riviera");
            us.getServicioMedico().actualizarMedico(idmed, "Nick Riviera", EstadoMedico.DISPONIBLE.toString());
        
            idmed=us.getServicioMedico().nuevoMedico("Dr Strange");
            us.getServicioMedico().actualizarMedico(idmed, "Dr Strange", EstadoMedico.DISPONIBLE.toString());
        
            int idpac=us.getServicioPaciente().nuevoPaciente("Perico Palotes", "232162910541");
            int idinc=us.registrarIncidenciaLeve(idpac, "Le duele la patita");
            us.finalizarIncidencia(idinc);
        
            idpac=us.getServicioPaciente().nuevoPaciente("John Mc Klein", "485823563219");
            idinc=us.registrarIncidenciaGrave(idpac,"Agujero de bala en el pie");
            us.finalizarIncidencia(idinc);
        
            List<Incidencia> incidenciasFinalizadas=us.obtenerIncidencias(null, EstadoIncidencia.HISTORICA);
        
            if(incidenciasFinalizadas.size()!=2)
                fail("TEST FAIL: El numero de incidencias finalizadas no es el correcto");
        }
        catch(Exception e)
        {
            fail("TEST FAIL: El test ha fallado al producirse una excepcion" + e.getLocalizedMessage());
        }
    }

    @Test
    public void testFinalizarIncidenciaFinTurno()
    {
        try
        { 
            int idmed=us.getServicioMedico().nuevoMedico("Nick Riviera");
            us.getServicioMedico().actualizarMedico(idmed, "Nick Riviera", EstadoMedico.DISPONIBLE.toString());
        
            int idmed2=us.getServicioMedico().nuevoMedico("Dr Strange");
            us.getServicioMedico().actualizarMedico(idmed2, "Dr Strange", EstadoMedico.DISPONIBLE.toString());
        
            int idpac=us.getServicioPaciente().nuevoPaciente("Perico Palotes", "232162910541");
            int idinc=us.registrarIncidenciaLeve(idpac, "Le duele la patita");
            us.finalizarIncidenciaFinTurno(idinc);
        
            idpac=us.getServicioPaciente().nuevoPaciente("John Mc Klein", "485823563219");
            idinc=us.registrarIncidenciaGrave(idpac,"Agujero de bala en el pie");
            us.finalizarIncidenciaFinTurno(idinc);
        
            List<Incidencia> incidenciasFinalizadas=us.obtenerIncidencias(null, EstadoIncidencia.HISTORICA);
        
            if(incidenciasFinalizadas.size()!=2)
                fail("TEST FAIL: El numero de incidencias finalizadas no es el correcto");
            
            if((us.getServicioMedico().ObtenerMedico(idmed)).getEstado()!=EstadoMedico.FUERA_SERV ||
               (us.getServicioMedico().ObtenerMedico(idmed2)).getEstado()!=EstadoMedico.FUERA_SERV)
                fail("TEST FAIL: Los medicos no han finalizado su turno exitosamente");
        }
        catch(Exception e)
        {
            fail("TEST FAIL: El test ha fallado al producirse una excepcion" + e.getLocalizedMessage());
        }
    }
    
    /**
     * Test of medicoConMasIncidencias method, of class UrgenciasService.
     */
    @Test
    public void testMedicoConMasIncidenciasCriticas()
    {
        try
        {
            int idmed1=us.getServicioMedico().nuevoMedico("Nick Riviera");
            us.getServicioMedico().actualizarMedico(idmed1, "Nick Riviera", EstadoMedico.DISPONIBLE.toString());

            int idpac=us.getServicioPaciente().nuevoPaciente("Perico Palotes", "232162910541");
            int idinc=us.registrarIncidenciaCritica(idpac, "Su esposa le ha dado una patada en las joyas de la corona");
            us.finalizarIncidencia(idinc);
        
            idpac=us.getServicioPaciente().nuevoPaciente("John Mc Klein", "485823563219");
            idinc=us.registrarIncidenciaCritica(idpac,"Agujero de bala en el pie");
            us.finalizarIncidencia(idinc);
            
            us.FinalizarTurno(idmed1);
            
            int idmed2=us.getServicioMedico().nuevoMedico("Dr Strange");
            us.getServicioMedico().actualizarMedico(idmed2, "Dr Strange", EstadoMedico.DISPONIBLE.toString());
            
            idinc=us.registrarIncidenciaCritica(idpac,"Herida de arma blanca");
            us.finalizarIncidencia(idinc);
            
            Medico medMasICR=us.medicoConMasIncidenciasCriticas();
            
            if(medMasICR==null)
                fail("TEST FAIL: No se pudo obtener el medico con mas incidencias criticas resueltas correctamente");
            
            if (medMasICR.getIdMedico() != idmed1)
                fail("TEST FAIL: El medico con mas incidencias criticas resueltas correctamente debe ser el Nick");
            
            Thread.sleep(1500);
            
            idpac=us.getServicioPaciente().nuevoPaciente("John Mc Kleiner", "485823563219");
            idinc=us.registrarIncidenciaCritica(idpac,"Agujero de bala en el huevs");
            us.finalizarIncidencia(idinc);
            
            medMasICR=us.medicoConMasIncidenciasCriticas();
            
            if (medMasICR.getIdMedico() != idmed2)
                fail("TEST FAIL: El medico con mas incidencias criticas resueltas correctamente debe ser el Strange");
        }
        catch(Exception e)
        {
            fail("TEST FAIL: No se pudo averiguar que medico tiene mas incidencias criticas resueltas debido a una excepcion" + e.getLocalizedMessage());
        }
    }

    /**
     * Llena la base de datos con 4 incidencias conocidas
     * @return 4
     * @throws Exception 
     */
    private int llenarIncidencias() throws Exception
    {
        try
        {
            for (int i = 1; i < 5; i++)
            {
                us.getServicioPaciente().nuevoPaciente("Paciente" + i, "12345678901" + i);
             
                int im = us.getServicioMedico().nuevoMedico("Medicastro" + i);
                us.ComenzarTurno(im);
            }
            
            //Añado 4 incidencias
            us.registrarIncidenciaLeve(1, "Sida");
            us.registrarIncidenciaLeve(2, "Disfuncion erectil");
            us.registrarIncidenciaGrave(3, "Ebola");
            us.registrarIncidenciaCritica(4, "Lepra");

            return 4;
        }
        catch (UrgenciasServiceException e)
        {
            throw new Exception("Error al llenar", e);
        }
    }
}
