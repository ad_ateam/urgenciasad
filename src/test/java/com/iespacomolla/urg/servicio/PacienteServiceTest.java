/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iespacomolla.urg.servicio;

import com.iespacomolla.urg.entidades.EstadoIncidencia;
import com.iespacomolla.urg.entidades.Incidencia;
import com.iespacomolla.urg.entidades.Paciente;
import com.iespacomolla.urg.entidades.TipoIncidencia;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Propietario
 */
public class PacienteServiceTest
{
    private String base = "urgenciasTest";
    private String login = "root";
    private String pass = "root";
    private String url = "jdbc:mysql://localhost/" + base;
    private String deletePacientes = "DELETE from Pacientes;";
    private static PacienteService ps=null;
    
    public PacienteServiceTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception
    {
        try
        {
            ps= new PacienteService();
        }
    
        catch(Exception e)
        {
            throw new Exception("No se pudo instanciar el PacienteService" + e);
        }
    }
        
    
    @AfterClass
    public static void tearDownClass() throws Exception
    {
        try
        {
            ps.finalizar();
        }
    
        catch(Exception e)
        {
            throw new Exception("No se pudo cerrar la sessionFactory" + e);
        }
    }
    
    @Before
    public void setUp() throws Exception
    {
        Connection conn = null;
	PreparedStatement prs = null;
        
        try
        {
            conn = DriverManager.getConnection(url, login, pass);
            prs = conn.prepareStatement(deletePacientes);
            prs.executeUpdate();
	} 
        
        catch (Exception e) 
        {
	    throw new Exception("No se han podido iniciar los tests" + e);
        }
        
        finally 
        {
            try 
            {
		prs.close();
		conn.close();		
            } 
            
            catch (SQLException sqle) 
            {	
                throw new Exception("No se ha podido cerrar la base de datos" + sqle);
            }
        }
    }

    @Test
    public void testObtenerTodosPacientesVacio() 
    {
	try
        {
            List<Paciente> listaPacientes= ps.obtenerTodosPacientes();
            
            if(!listaPacientes.isEmpty())
            {
		fail("TEST FAIL: La base de datos de Pacientes no se encuentra vacia");
            }
	}
        
        catch(Exception e)
        {
            fail("TEST FAIL: No se ha podido recuperar la lista de Pacientes de la base de datos"+ e);
	}
    }

    public int creaPacientes() throws Exception
    {
        try
        {
            ps.nuevoPaciente("Pepito Gomez","243565345321");
            ps.nuevoPaciente("Cornelio Antonio","786948373727");
        }
        
        catch(Exception e)
        {
            throw new Exception("No se pudieron crear los pacientes");
        }
        
        return 2;
    }
    
    @Test
    public void testObtenerTodosPacientesLLeno() 
    {
	try
        {
     
            int size=creaPacientes();
			
            List<Paciente> listaPacientes= ps.obtenerTodosPacientes();
			
            if(listaPacientes.size()!=size)
            {
		fail("TEST FAIL: La lista de Pacientes no tiene el numero de Pacientes introducidos");
            }
			
            for(Paciente pac: listaPacientes) 
            {
		assertNotNull(pac.getId());
		assertNotNull(pac.getNombre());
		assertNotNull(pac.getNSegSoc());
            }		
	}
        
        catch(Exception e)
        {
            fail("TEST FAIL: No se ha podido obtener la lista de Pacientes" + e);
	}
    }
    
    @Test
    public void testNuevoPaciente() 
    {
	try
        {
            ps.nuevoPaciente("Pepito Gomez","243565345321");
          		
            List<Paciente> listaPacientes= ps.obtenerTodosPacientes();
			
            if(listaPacientes.size()!=1)
            {
		fail("TEST FAIL: No se ha podido insertar el paciente");
            }
			
            for(Paciente pac: listaPacientes) 
            {
		assertNotNull(pac.getId());
		assertNotNull(pac.getNombre());
		assertNotNull(pac.getNSegSoc());
            }		
	}
        
        catch(PacienteServiceException pse)
        {
            fail("TEST FAIL: Excepcion al insertar el Paciente" + pse.getMessage());
	}
    }
    
    @Test (expected=PacienteServiceException.class)
    public void testNuevoPacienteNombreCorto() throws Exception
    {
            ps.nuevoPaciente("Pepi","243565345321");
    }
    
    @Test(expected=PacienteServiceException.class)
    public void testNuevoPacienteNSocInadecuado() throws Exception
    {
            ps.nuevoPaciente("Pepito","24356534");
    }
    
    @Test
    public void testActualizarPaciente()
    {
        try
        {
            ps.nuevoPaciente("Pepito Gomez","243565345321");
            
            List<Paciente> listaPacientes=ps.obtenerTodosPacientes();
            
            Paciente p=new Paciente();
            
            for(Paciente pac : listaPacientes)
            {
                p=pac;
            }
            
            ps.actualizarPaciente(p.getId(), "Pepitin Gomez", "232319834532");
        }
        
        catch(PacienteServiceException pse)
        {
            fail("TEST FAIL: No se pudo modificar el paciente con exito" + pse.getMessage());
        }     
    }
    
    @Test(expected=PacienteServiceException.class)
    public void testActualizarPacienteNombreCorto()
    {
        try
        {
            ps.nuevoPaciente("Pepito Gomez","243565345321");
            
            List<Paciente> listaPacientes=ps.obtenerTodosPacientes();
            
            Paciente p=new Paciente();
            
            for(Paciente pac : listaPacientes)
            {
                p=pac;
            }
            
            ps.actualizarPaciente(p.getId(), "Pepi", "232319834532");
        }
        
        catch(Exception e)
        {
            fail("TEST FAIL: No se pudo introducir el paciente en la BD");
        }     
    }
    
    @Test(expected=PacienteServiceException.class)
    public void testActualizarPacienteNSocInadecuado()
    {
        try
        {
            ps.nuevoPaciente("Pepito Gomez","243565345321");
            
            List<Paciente> listaPacientes=ps.obtenerTodosPacientes();
            
            Paciente p=new Paciente();
            
            for(Paciente pac : listaPacientes)
            {
                p=pac;
            }
            
            ps.actualizarPaciente(p.getId(), "Pepitin", "232319");
        }
        
        catch(Exception e)
        {
            fail("TEST FAIL: No se pudo introducir el paciente en la BD");
        }     
    }
   
    @Test
    public void testEliminarPaciente()
    {
        try
        {
            ps.nuevoPaciente("Pepito Gomez","243565345321");
            
            List<Paciente> listaPacientes=ps.obtenerTodosPacientes();
            
            Paciente p=new Paciente();
            
            for(Paciente pac : listaPacientes)
            {
                p=pac;
            }
            
            ps.eliminarPaciente(p.getId());
        }
        
        catch(PacienteServiceException pse)
        {
            fail("TEST FAIL: No se pudo eliminar el paciente con exito" + pse.getMessage());
        } 
    }
    @Test(expected=PacienteServiceException.class)
    public void testEliminarPacienteConIncidencias()
    {
        try
        {
            ps.nuevoPaciente("Pepito Gomez","243565345321");
            
            List<Paciente> listaPacientes=ps.obtenerTodosPacientes();
            
            Paciente p=new Paciente();
            
            for(Paciente pac : listaPacientes)
            {
                p=pac;
            }
            
            Incidencia i=new Incidencia();
            
            List<Incidencia> incidencias=new ArrayList();
            
            incidencias.add(i);
            
            SessionFactory sessionFactory;
            ServiceRegistry serviceRegistry;
            
            Configuration configuration = new Configuration();
	    
            configuration.configure();
	    
            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            
            Session sesion =sessionFactory.openSession();
            Transaction transaccion = sesion.beginTransaction();
                   
            p.setIncidencias(incidencias);
                
            sesion.update(p);   
            transaccion.commit();
            sesion.close();
            sessionFactory.close();
            
            ps.eliminarPaciente(p.getId());
        }
        
        catch(Exception e)
        {
            fail("TEST FAIL: No se pudo eliminar el paciente con exito debido a una excepcion" + e.getMessage());
        } 
    }
    
    @Test
    public void testObtenerPacientesEnEspera()
    {     
        try
        {
            ps.nuevoPaciente("Pepito Gomez","243565345321");
            
            Incidencia i=new Incidencia();
            
            i.setEstado(EstadoIncidencia.ESPERA);
            
            List<Incidencia> incidencias=new ArrayList();
            
            incidencias.add(i);
            
            List<Paciente> listaPacientes=ps.obtenerTodosPacientes();
            
            Paciente p=new Paciente();
            
            for(Paciente pac : listaPacientes)
            {
                p=pac;
            }
            
            SessionFactory sessionFactory;
            ServiceRegistry serviceRegistry;
            
            Configuration configuration = new Configuration();
	    
            configuration.configure();
	    
            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            
            Session sesion =sessionFactory.openSession();
            Transaction transaccion = sesion.beginTransaction();
                   
            p.setIncidencias(incidencias);
                
            sesion.update(p);   
            transaccion.commit();
            sesion.close();
            sessionFactory.close();
            
            listaPacientes=ps.obtenerPacientesEnEspera();
            
            if(listaPacientes.size()!=1)
                fail("TEST FAIL: No se obtuvo el numero de pacientes adecuado");
            
        }
        
        catch(PacienteServiceException pse)
        {
            fail("TEST FAIL: No se pudo obtener la lista de pacientes en espera con exito" + pse.getMessage());
        } 
    }
    
    @Test
    public void testObtenerPacientesEnAtencion()
    {     
        try
        {
            ps.nuevoPaciente("Pepito Gomez","243565345321");
            
            Incidencia i=new Incidencia();
            
            i.setEstado(EstadoIncidencia.ACTIVA);
            
            List<Incidencia> incidencias=new ArrayList();
            
            incidencias.add(i);
            
            List<Paciente> listaPacientes=ps.obtenerTodosPacientes();
            
            Paciente p=new Paciente();
            
            for(Paciente pac : listaPacientes)
            {
                p=pac;
            }
            
            SessionFactory sessionFactory;
            ServiceRegistry serviceRegistry;
            
            Configuration configuration = new Configuration();
	    
            configuration.configure();
	    
            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            
            Session sesion =sessionFactory.openSession();
            Transaction transaccion = sesion.beginTransaction();
                   
            p.setIncidencias(incidencias);
                
            sesion.update(p);   
            transaccion.commit();
            sesion.close();
            sessionFactory.close();
            
            listaPacientes=ps.obtenerPacientesEnAtencion();
            
            if(listaPacientes.size()!=1)
                fail("TEST FAIL: No se obtuvo el numero de pacientes adecuado");
            
        }
        
        catch(PacienteServiceException pse)
        {
            fail("TEST FAIL: No se pudo obtener la lista de pacientes en atencion con exito" + pse.getMessage());
        } 
    }
    
    @Test
    public void testObtenerPacientesEnAtencionCritica()
    {   
        try
        {
            ps.nuevoPaciente("Pepito Gomez","243565345321");
            
            Incidencia i=new Incidencia();
            
            i.setEstado(EstadoIncidencia.ACTIVA);
            
            i.setTipo(TipoIncidencia.CRITICA);
            
            List<Incidencia> incidencias=new ArrayList();
            
            incidencias.add(i);
            
            List<Paciente> listaPacientes=ps.obtenerTodosPacientes();
            
            Paciente p=new Paciente();
            
            for(Paciente pac : listaPacientes)
            {
                p=pac;
            }
            
            SessionFactory sessionFactory;
            ServiceRegistry serviceRegistry;
            
            Configuration configuration = new Configuration();
	    
            configuration.configure();
	    
            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            
            Session sesion =sessionFactory.openSession();
            Transaction transaccion = sesion.beginTransaction();
                   
            p.setIncidencias(incidencias);
                
            sesion.update(p);   
            transaccion.commit();
            sesion.close();
            sessionFactory.close();
            
            listaPacientes=ps.obtenerPacientesEnAtencionCritica();
            
            if(listaPacientes.size()!=1)
                fail("TEST FAIL: No se obtuvo el numero de pacientes adecuado");
            
        }
        
        catch(PacienteServiceException pse)
        {
            fail("TEST FAIL: No se pudo obtener la lista de pacientes en atencion critica con exito" + pse.getMessage());
        } 
    }
    
    @Test
    public void testObtenerIncidenciasPaciente()
    {
        try
        {
            ps.nuevoPaciente("Pepito Gomez","243565345321");
            
            Incidencia i=new Incidencia();
            
            i.setEstado(EstadoIncidencia.ACTIVA);
            
            i.setTipo(TipoIncidencia.CRITICA);
            
            List<Incidencia> incidencias=new ArrayList();
            
            incidencias.add(i);
            
            List<Paciente> listaPacientes=ps.obtenerTodosPacientes();
            
            Paciente p=new Paciente();
            
            for(Paciente pac : listaPacientes)
            {
                p=pac;
            }
            
            SessionFactory sessionFactory;
            ServiceRegistry serviceRegistry;
            
            Configuration configuration = new Configuration();
	    
            configuration.configure();
	    
            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            
            Session sesion =sessionFactory.openSession();
            Transaction transaccion = sesion.beginTransaction();
                   
            p.setIncidencias(incidencias);
                
            sesion.update(p);   
            transaccion.commit();
            sesion.close();
            sessionFactory.close();
            
            incidencias=ps.obtenerIncidenciasPaciente(p.getId());
            
            if(incidencias.size()!=1)
                fail("TEST FAIL: No se obtuvo el numero de incidencias adecuado");
            
        }
        
        catch(PacienteServiceException pse)
        {
            fail("TEST FAIL: No se pudo obtener la lista de incidencias con exito" + pse.getMessage());
        }  
    }
    
    @After
    public void tearDown() throws Exception 
    {
            
    }
}
