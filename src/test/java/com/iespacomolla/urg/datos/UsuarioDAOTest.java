/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iespacomolla.urg.datos;


import com.iespacomolla.urg.entidades.TipoRol;
import com.iespacomolla.urg.entidades.Usuario;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Propietario
 */
public class UsuarioDAOTest
{
    private String base = "urgenciasTest";
    private String login = "root";
    private String pass = "root";
    private String url = "jdbc:mysql://localhost/" + base;
    private String deleteUsuarios = "DELETE from Usuarios;";
    private static IUsuarioDAO idao=null;
    
    public UsuarioDAOTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception
    {
        try
        {
            idao= new UsuarioDAO();
        }
    
        catch(Exception e)
        {
            throw new Exception("No se pudo instanciar el UsuarioDAO" + e);
        }
    }
    
    @AfterClass
    public static void tearDownClass() throws Exception
    {
        try
        {
            idao.finalizar();
        }
    
        catch(Exception e)
        {
            throw new Exception("No se pudo cerrar la sessionFactory" + e);
        }
    }
    
    @Before
    public void setUp() throws Exception
    {
        Connection conn = null;
	PreparedStatement ps = null;
        
        try
        {
            conn = DriverManager.getConnection(url, login, pass);
            ps = conn.prepareStatement(deleteUsuarios);
            ps.executeUpdate();
	} 
        
        catch (Exception e) 
        {
	    throw new Exception("No se han podido iniciar los tests" + e);
        }
        
        finally 
        {
            try 
            {
		ps.close();
		conn.close();		
            } 
            
            catch (SQLException sqle) 
            {	
                throw new Exception("No se ha podido cerrar la base de datos" + sqle);
            }
        }
    }
  
    @Test
    public void testGetUsuariosVacio() 
    {
	try
        {
            List<Usuario> listaUsuarios= idao.getUsuarios();
            
            if(!listaUsuarios.isEmpty())
            {
		fail("TEST FAIL: La base de datos de Usuarios no se encuentra vacia");
            }
	}
        
        catch(Exception e)
        {
            fail("TEST FAIL: No se ha podido recuperar la lista de usuarios de la base de datos"+ e);
	}
    }
  
    @Test
    public void testAddUsuario() 
    {
	try
        {
            Usuario u=new Usuario();
           
            u.setNombre("Pepito Perez");
            u.setPwd("Contrasenya");
            u.setRol(TipoRol.SANITARIO);
            u.setUsu("pepesin");
		
            Usuario u1=new Usuario();
            
            u1.setNombre("Cornelio Gomez");
            u1.setPwd("Sawarudo");
            u1.setRol(TipoRol.DIRECTIVO);
            u1.setUsu("cornelin");
            
            idao.addUsuario(u);
            idao.addUsuario(u1);
	}
        
        catch(Exception e)
        {
            fail("TEST FAIL: No se han podido introducir los usuarios en la base de datos" + e);
	}
    }
   
    @Test
    public void testGetUsuariosLLeno() 
    {
	try
        {
            Usuario u=new Usuario();
           
            u.setNombre("Pepito Perez");
            u.setPwd("Contrasenya");
            u.setRol(TipoRol.SANITARIO);
            u.setUsu("pepesin");
		
            Usuario u1=new Usuario();
            
            u1.setNombre("Cornelio Gomez");
            u1.setPwd("Sawarudo");
            u1.setRol(TipoRol.DIRECTIVO);
            u1.setUsu("cornelin");
            
            idao.addUsuario(u);
            idao.addUsuario(u1);
			
            List<Usuario> listaUsuarios= idao.getUsuarios();
			
            if(listaUsuarios.size()!=2)
            {
		fail("TEST FAIL: La lista de usuarios no tiene el numero de usuarios introducidos");
            }
			
            for(Usuario usu: listaUsuarios) 
            {
		assertNotNull(usu.getId());
		assertNotNull(usu.getNombre());
		assertNotNull(usu.getPwd());
                assertNotNull(usu.getRol());
                assertNotNull(usu.getUsu());
            }
			
	}
        
        catch(Exception e)
        {
            fail("TEST FAIL: No se ha podido obtener la lista de usuarios" + e);
	}
    }
    
    @Test
    public void testGetUsuario() 
    {
	try
        {
            Usuario u=new Usuario();
            
            u.setNombre("Pepito Perez");
            u.setPwd("Contrasenya");
            u.setRol(TipoRol.SANITARIO);
            u.setUsu("pepesin");
            
            idao.addUsuario(u);
            
            Usuario u1=idao.getUsuario(u.getId());
	
            if(!(u1.getId()==u.getId()))
            {
		fail("TEST FAIL: Usuario incorrecto");
            }
            
            if(u1.getNombre()==null)
            {
		fail("TEST FAIL: El usuario obtenido no tiene nombre");
            }
			
            if(!(u1.getNombre().equals(u.getNombre())))
            {
		fail("TEST FAIL: El usuario no tiene el nombre correcto");
            }
			
            if(u1.getPwd()==null)
            {
		fail("TEST FAIL: El usuario no tiene contrasenya");
            }
			
            if(!(u1.getPwd().equals(u.getPwd())))
            {
                fail("TEST FAIL: El usuario no tiene la contrasenya correcta");
            }
	
            if(u1.getRol()==null)
            {
                fail("TEST FAIL: El usuario no tiene rol asignado");
            }
            
            if(!(u1.getRol()==u.getRol()))
            {
                fail("TEST FAIL: El usuario no tiene el rol correcto");
            }
            
            if(u1.getUsu()==null)
            {
		fail("TEST FAIL: El usuario obtenido no tiene nickname");
            }
			
            if(!(u1.getUsu().equals(u.getUsu())))
            {
		fail("TEST FAIL: El usuario no tiene el nickname correcto");
            }   
        }
        
        catch(Exception e)
        {
            fail("TEST FAIL: No se ha podido obtener el usuario" + e);
	}
    }
    
    @Test
    public void testDelUsuario()
    {
        try
        {
        
            Usuario u=new Usuario();
            Usuario u1=new Usuario();
            
            u.setNombre("Pepito Perez");
            u.setPwd("Contrasenya");
            u.setRol(TipoRol.SANITARIO);
            u.setUsu("pepesin");
            
            u1.setNombre("Cornelio Gomez");
            u1.setPwd("Sawarudo");
            u1.setRol(TipoRol.DIRECTIVO);
            u1.setUsu("cornelin");
            
            idao.addUsuario(u);
            idao.addUsuario(u1);
            
            idao.delUsuario(u.getId());
            
            List<Usuario> listaUsuarios=idao.getUsuarios();
            
            if(listaUsuarios.size()!=1)
                fail("TEST FAIL: No se ha eliminado el usuario correctamente");    
        }
    
        catch(Exception e)
        {
            fail("TEST FAIL: No se ha podido eliminar el jugador debido a un motivo externo");
        }
    }
    
    @Test
    public void testUpdateUsuario()
    {
        try
        {
         
            Usuario u=new Usuario();
            Usuario u1=new Usuario();
            
            u.setNombre("Pepito Perez");
            u.setPwd("Contrasenya");
            u.setRol(TipoRol.SANITARIO);
            u.setUsu("pepesin");
            
            u1.setId(u.getId());
            u1.setNombre("Cornelio Gomez");
            u1.setPwd("Sawarudo");
            u1.setRol(TipoRol.DIRECTIVO);
            u1.setUsu("cornelin");
            
            idao.addUsuario(u); 
            
            idao.updateUsuario(u1);
            
            u=idao.getUsuario(u.getId());
            
            if(!(u.getNombre().equals(u1.getNombre())))
            {
		fail("TEST FAIL: No se ha actualizado el nombre correctamente");
            }
           
            if(!(u.getPwd().equals(u1.getPwd())))
            {
                fail("TEST FAIL: No se ha actualizado la contrasenya correctamente");
            }
            
            if(!(u.getRol()==u1.getRol()))
            {
                fail("TEST FAIL: No se ha actualizado el rol correctamente");
            }
        
            if(!(u.getUsu().equals(u1.getUsu())))
            {
		fail("TEST FAIL: No se ha actualizado el nickname correctamente");
            }  
        }
       
        catch(Exception e)
        {
            fail("TEST FAIL: No se ha podido realizar la modificacion del usuario por motivos externos" + e);
        }
    }
    
    @After
    public void tearDown() throws Exception
    {
    }
}
