package com.iespacomolla.urg.datos;

/**
 *
 * @author ANDRES Angel
 */
public class UsuarioDAOException extends Exception
{
    public UsuarioDAOException(String mensaje, Exception causa)
    {
        super(mensaje, causa);
    }
}
