package com.iespacomolla.urg.datos;

/**
 *
 * @author Adrian
 */
public class PacienteDAOException extends Exception
{
    public PacienteDAOException(String mensaje, Exception causa)
    {
        super(mensaje, causa);
    }
}
