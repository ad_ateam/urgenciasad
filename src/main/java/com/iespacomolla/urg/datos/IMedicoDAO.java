/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iespacomolla.urg.datos;

import com.iespacomolla.urg.entidades.Medico;
import java.util.List;

/**
 *
 * @author Propietario
 */
public interface IMedicoDAO
{
    public int addMedico(Medico m) throws MedicoDAOException;
    public void delMedico(int idMedico) throws MedicoDAOException;
    public Medico getMedico(int idMedico) throws MedicoDAOException;
    public void updateMedico(Medico m) throws MedicoDAOException;
    public List<Medico> getMedicos() throws MedicoDAOException;
    public List<Medico> getMedicosServicio() throws MedicoDAOException;
    public void finalizar() throws MedicoDAOException;
}
