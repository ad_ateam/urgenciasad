package com.iespacomolla.urg.datos;

import com.iespacomolla.urg.entidades.Paciente;
import java.util.List;

/**
 *
 * @author Adrian
 */
public interface IPacienteDAO
{
    /**
     * Añade un paciente a la base de datos
     * @param p Paciente a añadir
     * @throws PacienteDAOException Error en la inserción del paciente
     */
    public int addPaciente(Paciente p) throws PacienteDAOException;
    /**
     * Actualiza un paciente a la base de datos
     * @param p Paciente a actualizar
     * @throws PacienteDAOException Error en la actualización del paciente
     */
    public void updatePaciente(Paciente p) throws PacienteDAOException;
    /**
     * Borra un paciente a la base de datos
     * @param idPaciente Identificador de paciente a borrar
     * @throws PacienteDAOException Error en el borrado del paciente
     */
    public void delPaciente(int idPaciente) throws PacienteDAOException;
    /**
     * Devuelve el paciente cuyo id corresponda al indicado
     * @param idPaciente Identificador de paciente 
     * @return Paciente cuyo id coincide con el indicado o null
     * @throws PacienteDAOException Error en la busqueda del paciente
     */
    public Paciente getPaciente(int idPaciente) throws PacienteDAOException;
    /**
     * Devuelve el paciente cuyo numero de la seguridad social corresponda al indicado
     * @param numSS Numero de la seguridad social
     * @return Paciente cuyo numero SS coincide con el indicado o null
     * @throws PacienteDAOException Error en la busqueda del paciente
     */
    public Paciente getPaciente(String numSS) throws PacienteDAOException;
     /**
     * Devuelve todos los pacientes de la base de datos
     * Siempre devuelve una lista
     * @return Lista con los pacientes almacenados
     * @throws PacienteDAOException Error al obtener los pacientes
     */
    public List<Paciente> getPacientes() throws PacienteDAOException;
    /**
     * Devuelve una lista de pacientes con incidencias graves en espera
     * Siempre devuelve una lista
     * @return Lista con los pacientes con incidencias graves en espera
     * @throws PacienteDAOException Error al obtener los pacientes
     */
    public List<Paciente> getPacientesColaIncidenciasGraves() throws PacienteDAOException;
    /**
     * Cierra la conexión con la base de datos
     * @throws PacienteDAOException 
     */
    public void finalizar() throws PacienteDAOException;
}