package com.iespacomolla.urg.datos;


import com.iespacomolla.urg.entidades.EstadoIncidencia;
import com.iespacomolla.urg.entidades.Incidencia;
import com.iespacomolla.urg.entidades.Paciente;
import com.iespacomolla.urg.entidades.TipoIncidencia;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author Adrian
 */

public class PacienteDAO implements IPacienteDAO
{
    //definimos una variable sessionFactory
    private  SessionFactory sessionFactory;
    
    //Constructor de nuestro UsuarioDAO
    public PacienteDAO() throws PacienteDAOException
    {
	try 
        {		
            //Constructor donde definimos nuestra sessionFactory en función una configuracion creada
            ServiceRegistry serviceRegistry;
            Configuration configuration = new Configuration();
	    configuration.configure();
	    serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	} 
        catch (HibernateException e) 
        {
            System.err.println("" + e);
            throw new PacienteDAOException("Error al inicializar el servicio paciente", e);
	}		
    }
    //Método que añade un usuario a nuestra bd
    @Override
    public int addPaciente(Paciente p) throws PacienteDAOException
    {
        try
        {
            //Aquí pedimos una sesión a nuestra sessionFactory y abrimos una transacción
            Session sesion = sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            //Finalmente, guardamos el usuario que se nos ha pasado, realizamos un commit y cerramos la sesión
            sesion.save(p);
            transaccion.commit();
            //sesion.close();
            
            return p.getId();
        }       
        catch(Exception e)
        {
            throw new PacienteDAOException("No se pudo realizar la transacción con éxito",e);     
        }    
    }
    /*Método que modifica los campos de un usuario con los valores del usuario que le
    pasamos por parámetro*/
    @Override
    public void updatePaciente(Paciente p) throws PacienteDAOException
    {
    
        try
        {
            //Aquí sacamos el usuario de la bd en función del id del usuario que se nos pasa
            Paciente pac=getPaciente(p.getId());
            Session sesion =sessionFactory.getCurrentSession(); //
            Transaction transaccion = sesion.beginTransaction();
            //Le ponemos todos los campos en función del usuario que nos han pasado        
            pac.setNombre(p.getNombre());
            pac.setNSegSoc(p.getNSegSoc());
            //Updateamos el usuario, confirmamos la transacción y cerramos la sesión    
            sesion.update(pac);   
            transaccion.commit();
             //sesion.close();
        }
        
        catch(Exception e)
        {
            throw new PacienteDAOException("No se pudo modificar el paciente con id " +p.getId(),e);
        }   
    }
    //Método que borra de la base de datos el usuario perteneciente a la idUsuario que le pasamos
    @Override
    public void delPaciente(int idPaciente) throws PacienteDAOException
    { 
        try
        {
            //Obtenemos el usuario que queremos borrar de la bd por medio del id que nos han pasado
            Paciente p=this.getPaciente(idPaciente);
            Session sesion =sessionFactory.getCurrentSession(); //
            Transaction transaccion = sesion.beginTransaction();
	    //Borramos y como siempre, confirmamos la transacción y cerramos la sesión.
            sesion.delete(p);
            transaccion.commit();
             //sesion.close();
        }
        
        catch(Exception e)
        {
            throw new PacienteDAOException("No se pudo borrar el usuario indicado",e); 
        }
    }
    //Método que devuelve el usuario de la bd correspondiente al idUsuario que le pasamos
    @Override
    public Paciente getPaciente(int idPaciente) throws PacienteDAOException
    {
        Paciente p=null;
        
        try
        {
            Session sesion = sessionFactory.getCurrentSession(); //
	    /*Obtenemos el usuario mediante el id que se nos pasa, indicando la clase	
            correspondiente a la bd de donde queremos sacar el usuario*/
            Transaction transaccion = sesion.beginTransaction();
            p=(Paciente)sesion.get(Paciente.class, idPaciente);
            Hibernate.initialize(p.getIncidencias());
             //sesion.close();
            transaccion.commit();
        
        }	
            
        catch(Exception e)
        {
            throw new PacienteDAOException("No se pudo obtener el usuario indicado",e);
        }    
            
        return p;
    }
    @Override
    public Paciente getPaciente(String numSS) throws PacienteDAOException
    {
        Paciente p = null;
        try
        {
            Session sesion = sessionFactory.getCurrentSession(); //
            Transaction transaccion = sesion.beginTransaction();
            // Busca el paciente por su numero de SS
            Query q = sesion.createQuery("FROM Paciente WHERE nSegSoc LIKE '" + numSS + "'");
            
            List<Paciente> listaPacientes = q.list();
            if (listaPacientes != null && listaPacientes.size() > 0)
            {
                p = listaPacientes.get(0);
                Hibernate.initialize(p.getIncidencias());
            }
            
             //sesion.close();
            transaccion.commit();
            return p;
        }	
            
        catch(Exception e)
        {
            throw new PacienteDAOException("No se pudo obtener el usuario indicado",e);
        }    
    }
    
    /**
     * Método que devuelve una lista de usuarios sacados de nuestra bd.
     * Siempre devuelve una lista
     * @return
     * @throws PacienteDAOException 
     */
    @Override
    public List<Paciente> getPacientes() throws PacienteDAOException
    {
        try
        {
            Session sesion = sessionFactory.getCurrentSession(); //
            Transaction transaccion = sesion.beginTransaction();
            //Realizamos una query para obtener todos los Pacientes
            Query q = sesion.createQuery("FROM Paciente"); //sesion.createQuery("FROM Pacientes");
            //Metemos el resultado en la lista anteriormente definida y cerramos la sesion
            List<Paciente> listaPacientes = q.list();
             //sesion.close();
            
            if (listaPacientes == null)
                listaPacientes = new ArrayList<>();
            else for (Paciente p : listaPacientes)
                Hibernate.initialize(p);
            transaccion.commit();
            return listaPacientes;
        }
        catch(Exception e)
        {
            throw new PacienteDAOException("No se pudo obtener la lista de pacientes",e);
        }
    }
    @Override
    public List<Paciente> getPacientesColaIncidenciasGraves()throws PacienteDAOException
    {
        try
        {
            Session sesion = sessionFactory.getCurrentSession(); //
            Transaction transaccion = sesion.beginTransaction();
            //Realizamos una query para obtener todos los Pacientes
            Query q = sesion.createQuery("FROM Paciente"); //sesion.createQuery("FROM Pacientes");
            //Metemos el resultado en la lista anteriormente definida y cerramos la sesion
            List<Paciente> listaPacientes = q.list();
             //sesion.close();
            //Creamos la lista de pacientes en espera con incidencias graves
            List<Paciente> listaPacEspGrav=new ArrayList<>();
            List<Incidencia> incidencias;
            //Creamos un booleano que nos dirá cuándo hay que meter un paciente en la lista
            boolean correcto=false;
            //Si la lista de pacientes no es nula, la recorremos
            if(listaPacientes!=null)
            {    
                for (Paciente p : listaPacientes)
                    Hibernate.initialize(p);
                //Realizamos el commit 
                transaccion.commit();
                
                for(Paciente p : listaPacientes)
                {
                    //Recogemos la lista de incidencias del paciente en cuestion
                    incidencias=p.getIncidencias();
                    
                    if(incidencias!=null)
                    {    
                        //Si la lista de incidencias no es nula, la recorremos
                        for(Incidencia i: incidencias)
                        {
                            //Si tiene alguna incidencia que sea grave y este en espera, ponemos correcto a true y salimos del bucle
                            if(i.getTipo()==TipoIncidencia.GRAVE && i.getEstado()==EstadoIncidencia.ESPERA)
                            {
                                correcto=true;
                                break;
                            }
                        }
                    }
                    //Si correcto esta a true, añadimos el paciente a la lista y ponemos correcto a false
                    if(correcto==true)
                    {    
                        listaPacEspGrav.add(p);
                        correcto=false;
                    }
                }
            }
            //Devolvemos la lista, este vacia o no
            return listaPacEspGrav;
        }
        catch(Exception e)
        {
            throw new PacienteDAOException("No se pudo obtener la lista de pacientes con Incidencias Graves en Espera",e);
        }
    }

    //Método que cierra una sessionFactory
    @Override
    public void finalizar() throws PacienteDAOException
    {
        try
        {
            //Cerramos la sessionFactory
            sessionFactory.close();
        }
        
        catch (Exception e)
        {
            throw new PacienteDAOException("Error al cerrar la sessionFactory", e);
        }
    }
}
