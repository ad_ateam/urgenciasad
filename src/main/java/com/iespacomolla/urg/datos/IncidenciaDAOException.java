/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iespacomolla.urg.datos;

/**
 *
 * @author Propietario
 */
public class IncidenciaDAOException extends Exception
{
    public IncidenciaDAOException(String mensaje, Exception causa)
    {
        super(mensaje, causa);
    }
}
