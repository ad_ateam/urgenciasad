package com.iespacomolla.urg.datos;

import com.iespacomolla.urg.entidades.Usuario;
import java.util.List;

/**
 *
 * @author ANDRES Angel
 */
public interface IUsuarioDAO
{
    /**
     * Añade un nuevo usuario a la base de datos
     * @param u Usuario a añadir
     * @return Id del usuario añadido
     * @throws UsuarioDAOException Error al añadir el usuario
     */
    public int addUsuario(Usuario u) throws UsuarioDAOException;
    /**
     * Actualiza el usuario incidado
     * @param u Usiario a modificar
     * @throws UsuarioDAOException Error al editar el usuario
     */
    public void updateUsuario(Usuario u) throws UsuarioDAOException;
    /**
     * Borra el usuario cuyo identificador coincida con el indicado
     * @param idUsuario Identificador de usuario a borrar
     * @throws UsuarioDAOException Error en el proceso de borrado de usuario
     */
    public void delUsuario(int idUsuario) throws UsuarioDAOException;
    /**
     * Devuelve el usuario cuyo identificador coincida con el indicado o null
     * @param idUsuario Identificador de usuario a buscar
     * @return {@code Usuario} buscado por id
     * @throws UsuarioDAOException Error en la búsqueda del usuario por id
     */
    public Usuario getUsuario(int idUsuario) throws UsuarioDAOException;
    /**
     * Devuelve todos los usuarios
     * @return Lista de usuarios almacenados en la base de datos
     * @throws UsuarioDAOException Error en la obtención de los usuarios
     */
    public List<Usuario> getUsuarios() throws UsuarioDAOException;
    /**
     * Cierra la conexión con la base de datos
     * @throws UsuarioDAOException Error de cierre de la base de datos
     */
    public void finalizar() throws UsuarioDAOException;
}
