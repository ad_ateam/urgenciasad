/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.iespacomolla.urg.datos;
import com.iespacomolla.urg.datos.IIncidenciaDAO;
import com.iespacomolla.urg.entidades.EstadoIncidencia;
import com.iespacomolla.urg.entidades.Incidencia;
import com.iespacomolla.urg.entidades.Medico;
import com.iespacomolla.urg.entidades.TipoIncidencia;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
/**
 *
 * @author Saul
 */
public class IncidenciaDAO implements IIncidenciaDAO
{
    
    private  SessionFactory sessionFactory;
    
    public IncidenciaDAO() throws IncidenciaDAOException
    {
	try 
        {		
            //Constructor donde definimos nuestra sessionFactory en función una configuracion creada
            ServiceRegistry serviceRegistry;
            Configuration configuration = new Configuration();
	    configuration.configure();
	    serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}
        catch (HibernateException e) 
        {
            throw new IncidenciaDAOException("Error al inicializar el servicio incidencia", e);
	}		
    }
    
    //Metodo que añade una incidencia a la base de datos
    @Override
    public int addIncidencia(Incidencia i) throws IncidenciaDAOException
    {
        try
        {
            Session sesion =sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            sesion.save(i);
            transaccion.commit();
             //sesion.close();
            
            return i.getIdIncidencia();
        }
        catch(HibernateException e)
        {
            throw new IncidenciaDAOException("No se pudo realizar la transacción con éxito",e);     
        }    
    }

    @Override
    public void updateIncidencia(Incidencia i) throws IncidenciaDAOException
    {
        try
        {
            Incidencia inc=getIncidencia(i.getIdIncidencia());
            Session sesion =sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            
            inc.setDescripcion(i.getDescripcion());            
            inc.setFechaEntrada(i.getFechaEntrada());
            inc.setFechaSalida(i.getFechaSalida());
            inc.setEstado(i.getEstado());
            
            inc.setMedico(i.getMedico());           
            inc.setPaciente(i.getPaciente());
            inc.setTipo(i.getTipo());   
            inc.setNumEspera(i.getNumEspera());            
            
          
            sesion.update(inc);   
            transaccion.commit();
             //sesion.close();
        }
        catch(IncidenciaDAOException | HibernateException e)
        {
            throw new IncidenciaDAOException("No se pudo modificar la incidencia con id " +i.getIdIncidencia(),e);
        }
    }
    
    //Devuelve una lista con todas las incidencias
    @Override
    public Incidencia getIncidencia(int idIncidencia) throws IncidenciaDAOException
    {
        Incidencia i=null;
        
        try
        {
            Session sesion = sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            i=(Incidencia)sesion.get(Incidencia.class, idIncidencia);
             //sesion.close();
            transaccion.commit();
        }
        catch(HibernateException e)
        {
            throw new IncidenciaDAOException("No se pudo obtener la incidencia indicada",e);
        }    
            
        return i;
    
    }

    @Override
    public List<Incidencia> getIncidencias() throws IncidenciaDAOException
    {
    
        List<Incidencia> listaIncidencias=null;
        
        try
        {
            Session sesion=sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            //Creamos nuestra query para obtener todas los incidencias
            Query q=sesion.createQuery("FROM Incidencia");
            //Pasamos el resultado de la query a una lista anteriormente creada
            listaIncidencias=q.list();
            //Cerramos la sesion
             //sesion.close();   
            transaccion.commit();
        }
    
        catch(HibernateException e)
        {
            throw new IncidenciaDAOException("No se pudo obtener la lista de incidencias correctamente",e);
        }
        
        return listaIncidencias;
    }

    @Override
    public List<Incidencia> getIncidenciasPaciente(int idPaciente) throws IncidenciaDAOException
    {
        List<Incidencia> listaIncidenciasPaciente=null;
        
        try
        {
            Session sesion=sessionFactory.getCurrentSession();
            //Creamos nuestra query para obtener las incidencias de un paciente
            Transaction transaccion = sesion.beginTransaction();
            Query q=sesion.createQuery("FROM Incidencia,Paciente where id="+ idPaciente);
            //Pasamos el resultado de la query a una lista anteriormente creada
            listaIncidenciasPaciente=q.list();
            //Cerramos la sesion
             //sesion.close(); 
            transaccion.commit();
        }
    
        catch(HibernateException e)
        {
            throw new IncidenciaDAOException("No se pudo obtener la lista de incidencias del paciente correctamente",e);
        }
        
        return listaIncidenciasPaciente;    
    }

    @Override
    public List<Incidencia> getIncidenciasMedico(int idMedico) throws IncidenciaDAOException
    {
        List<Incidencia> listaIncidenciasMedico=null;
        
        try
        {
            Session sesion=sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            //Creamos nuestra query para obtener las incidencias de un medico
            Query q=sesion.createQuery("FROM Incidencia WHERE medico.idMedico="+ idMedico);
            listaIncidenciasMedico=q.list();
            
            //Cerramos la sesion
             //sesion.close(); 
            transaccion.commit();
        }
    
        catch(HibernateException e)
        {
            throw new IncidenciaDAOException("No se pudo obtener la lista de incidencias del medico correctamente",e);
        }
        
        return listaIncidenciasMedico;   
    }

    @Override
    public List<Incidencia> getIncidenciasActivas() throws IncidenciaDAOException
    {
        List<Incidencia> listaIncidenciasActivas=null;
        
        try
        {
            Session sesion=sessionFactory.getCurrentSession();
            //Creamos nuestra query para obtener todas los incidencias activas
            Transaction transaccion = sesion.beginTransaction();
            Query q=sesion.createQuery("FROM Incidencia where EstadoIncidencia=" + EstadoIncidencia.ACTIVA);
            //Pasamos el resultado de la query a una lista anteriormente creada
            listaIncidenciasActivas=q.list();
            //Cerramos la sesion
             //sesion.close();  
            transaccion.commit();
        }
    
        catch(HibernateException e)
        {
            throw new IncidenciaDAOException("No se pudo obtener la lista de incidencias activas correctamente",e);
        }
        
        return listaIncidenciasActivas;
    }

    @Override
    public List<Incidencia> getIncidenciasHistoricas() throws IncidenciaDAOException
    {
        List<Incidencia> listaIncidenciasHistoricas=null;
        
        try
        {
            Session sesion=sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            //Creamos nuestra query para obtener todas los incidencias Historicas
            Query q=sesion.createQuery("FROM Incidencia where EstadoIncidencia=" + EstadoIncidencia.HISTORICA);
            //Pasamos el resultado de la query a una lista anteriormente creada
            listaIncidenciasHistoricas=q.list();
            //Cerramos la sesion
             //sesion.close();  
            transaccion.commit();
        }
    
        catch(HibernateException e)
        {
            throw new IncidenciaDAOException("No se pudo obtener la lista de incidencias historicas correctamente",e);
        }
        
        return listaIncidenciasHistoricas;
    }
    public List<Incidencia> getIncidendias(TipoIncidencia tipo, EstadoIncidencia estado) throws IncidenciaDAOException
    {
        List<Incidencia> li = new ArrayList<>();
        
        try
        {
            Session sesion=sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            Query q = null;
            
            // Generamos la query dependiendo del tipo y estado. Pueden ser null
            if (tipo != null && estado != null)
            {
                q = sesion.createQuery(
                    String.format("FROM Incidencia WHERE tipo=%d AND estado=%d",
                                  tipo.ordinal(), estado.ordinal()));
            }
            else if (tipo !=null)
                q = sesion.createQuery("FROM Incidencia WHERE tipo=" + tipo.ordinal());
            else if (estado != null)
                q = sesion.createQuery("FROM Incidencia WHERE estado=" + estado.ordinal());
            else
                q = sesion.createQuery("FROM Incidencia");
            
            //Pasamos el resultado de la query a una lista anteriormente creada
            li.addAll(q.list());
            
            //Cerramos la sesion
             //sesion.close();  
            transaccion.commit();
        }
        catch(HibernateException e)
        {
            throw new IncidenciaDAOException("No se pudo obtener la lista de incidencias correctamente",e);
        }
        
        return li;
    }

    @Override
    public void finalizar() throws IncidenciaDAOException
    {
        try
        {
            //Cerramos la sessionFactory
            sessionFactory.close();
        }
        
        catch (HibernateException e)
        {
            throw new IncidenciaDAOException("Error al cerrar la sessionFactory", e);
        }
    }           
}
