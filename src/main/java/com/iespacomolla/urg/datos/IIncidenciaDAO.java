/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iespacomolla.urg.datos;

import com.iespacomolla.urg.entidades.Incidencia;
import java.util.List;

/**
 *
 * @author Propietario
 */
public interface IIncidenciaDAO
{
    public int addIncidencia(Incidencia i) throws IncidenciaDAOException;
    public void updateIncidencia(Incidencia i) throws IncidenciaDAOException;
    public Incidencia getIncidencia(int idIncidencia) throws IncidenciaDAOException;
    public List<Incidencia> getIncidencias() throws IncidenciaDAOException;
    public List<Incidencia> getIncidenciasPaciente(int idPaciente) throws IncidenciaDAOException;
    public List<Incidencia> getIncidenciasMedico(int idMedico) throws IncidenciaDAOException;
    public List<Incidencia> getIncidenciasActivas() throws IncidenciaDAOException;
    public List<Incidencia> getIncidenciasHistoricas() throws IncidenciaDAOException;
    public void finalizar() throws IncidenciaDAOException;
}
