/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iespacomolla.urg.datos;

import com.iespacomolla.urg.entidades.EstadoMedico;
import com.iespacomolla.urg.entidades.Medico;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author Propietario
 */
public class MedicoDAO implements IMedicoDAO
{

    private SessionFactory sessionFactory;

    public MedicoDAO() throws MedicoDAOException
    {
        try
        {
            //Constructor donde definimos nuestra sessionFactory en función una configuracion creada
            ServiceRegistry serviceRegistry;
            Configuration configuration = new Configuration();
            configuration.configure();
            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        }
        catch (HibernateException e)
        {
            throw new MedicoDAOException("Error al inicializar el servicio medico", e);
        }
    }

    //Método que añade un médico a la base de datos
    @Override
    public int addMedico(Medico m) throws MedicoDAOException
    {
        try
        {
            //Aquí pedimos una sesión a nuestra sessionFactory y abrimos una transacción
            Session sesion = sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            //Finalmente, guardamos el médico que se nos ha pasado, realizamos un commit y cerramos la sesión
            sesion.save(m);
            transaccion.commit();
             //sesion.close();

            return m.getIdMedico();
        }
        catch (HibernateException e)
        {
            throw new MedicoDAOException("No se pudo realizar la transacción con éxito", e);
        }
    }

    //Método que borra de la base de datos el médico perteneciente a la idMedico que le pasamos
    @Override
    public void delMedico(int idMedico) throws MedicoDAOException
    {
        try
        {
            //Obtenemos el médico que queremos borrar de la bd por medio del id que nos han pasado
            Medico m = this.getMedico(idMedico);
            Session sesion = sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            //Borramos y como siempre, confirmamos la transacción y cerramos la sesión.
            sesion.delete(m);
            transaccion.commit();
             //sesion.close();
        }

        catch (MedicoDAOException | HibernateException e)
        {
            throw new MedicoDAOException("No se pudo borrar el médico indicado", e);
        }
    }

    //Método que devuelve el médico de la bd correspondiente al idMedico que le pasamos
    @Override
    public Medico getMedico(int idMedico) throws MedicoDAOException
    {
        Medico m = null;

        try
        {
            Session sesion = sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            /*Obtenemos el médico mediante el id que se nos pasa, indicando la clase	
             correspondiente a la bd de donde queremos sacar el médico*/
            m = (Medico) sesion.get(Medico.class, idMedico);
             //sesion.close();
            transaccion.commit();
        }
        catch (HibernateException e)
        {
            throw new MedicoDAOException("No se pudo obtener el médico indicado", e);
        }

        return m;
    }

    /*Método que modifica los datos de un médico correspondiente al id del medico
     que le pasamos como parámetro por los datos del mismo médico que le pasamos
     como parámetro*/
    @Override
    public void updateMedico(Medico m) throws MedicoDAOException
    {
        try
        {
            /*Obtenemos el médico con el id del que le pasamos como parámetro y
             lo modificamos*/
            Medico med = getMedico(m.getIdMedico());
            med.setNombre(m.getNombre());
            med.setEstado(m.getEstado());
            //Pedimos sesion y comenzamos la transaccion
            Session sesion = sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            /*Aqui updateamos el medico existente en la bd, realizamos el commit y cerramos
             la sesión*/
            sesion.update(med);
            transaccion.commit();
             //sesion.close();
        }
        catch (MedicoDAOException | HibernateException e)
        {
            throw new MedicoDAOException("No se pudo modificar el médico con éxito", e);
        }
    }

    //Método que devuelve una lista con todos los médicos
    @Override
    public List<Medico> getMedicos() throws MedicoDAOException
    {

        List<Medico> listaMedicos = null;

        try
        {
            Session sesion = sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            //Creamos nuestra query para obtener todos los médicos
            Query q = sesion.createQuery("FROM Medico");
            //Pasamos el resultado de la query a una lista anteriormente creada
            listaMedicos = q.list();
            //Por último, cerramos la sesion
             //sesion.close();
            transaccion.commit();
        }

        catch (HibernateException e)
        {
            throw new MedicoDAOException("No se pudo obtener la lista de médicos correctamente", e);
        }

        return listaMedicos;
    }

    /*Método que devuelve una lista de los médicos que están de servicio
     (EstadoMedico.DISPONIBLE,EstadoMedico.OCUPADO)*/
    @Override
    public List<Medico> getMedicosServicio() throws MedicoDAOException
    {

        List<Medico> listaMedicos = null;

        try
        {
            //Obtenemos una sesión de la sessionFactory
            Session sesion = sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            //Creamos una query para obtener todos los médicos que no estén fuera de servicio
            Query q = sesion.createQuery("FROM Medico WHERE estado!="+EstadoMedico.FUERA_SERV.ordinal());
            //Metemos en resultado de la query en la lista anteriormente creada
            listaMedicos = q.list();
            //Por último, cerramos la sesión
             //sesion.close();
            transaccion.commit();
        }

        catch (HibernateException e)
        {
            throw new MedicoDAOException("No se pudo obtener la lista de médicos que están de servicio", e);
        }

        return listaMedicos;
    }

    public List<Medico> getMedicosDisponibles() throws MedicoDAOException
    {
        // Asegura que siempre se devuelva una lista aunque esté vacía
        List<Medico> listaMedicosDisp = new ArrayList<>();

        try
        {
            //Obtenemos una sesión de la sessionFactory
            Session sesion = sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            //Creamos una query para obtener todos los médicos que estén disponibles
            Query q = sesion.createQuery("FROM Medico WHERE estado=" + EstadoMedico.DISPONIBLE.ordinal());
            //Metemos en resultado de la query en la lista anteriormente creada
            listaMedicosDisp.addAll(q.list());
            //Por último, cerramos la sesión
             //sesion.close();
            transaccion.commit();
        }
        catch (HibernateException e)
        {
            throw new MedicoDAOException("No se pudo obtener la lista de médicos que están de servicio", e);
        }

        return listaMedicosDisp;
    }

    //Método que cierra una sessionFactory
    @Override
    public void finalizar() throws MedicoDAOException
    {
        try
        {
            //Cerramos la sessionFactory
            sessionFactory.close();
        }

        catch (HibernateException e)
        {
            throw new MedicoDAOException("Error al cerrar la sessionFactory", e);
        }
    }
}
