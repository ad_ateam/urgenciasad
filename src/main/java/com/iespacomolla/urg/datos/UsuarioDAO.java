/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iespacomolla.urg.datos;


import com.iespacomolla.urg.entidades.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author Propietario
 */
public class UsuarioDAO implements IUsuarioDAO
{
    //definimos una variable sessionFactory
    private  SessionFactory sessionFactory;
    
    //Constructor de nuestro UsuarioDAO
    public UsuarioDAO() throws UsuarioDAOException 
    {
	try 
        {		
            //Constructor donde definimos nuestra sessionFactory en función una configuracion creada
            ServiceRegistry serviceRegistry;
            Configuration configuration = new Configuration();
	    configuration.configure();
	    serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}
        catch (HibernateException e) 
        {
            throw new UsuarioDAOException("Error al inicializar servicio de usuarios", e);
	}		
    }
    //Método que añade un usuario a nuestra bd
    @Override
    public int addUsuario(Usuario u) throws UsuarioDAOException 
    {
        try
        {
            //Aquí pedimos una sesión a nuestra sessionFactory y abrimos una transacción
            Session sesion =sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            //Finalmente, guardamos el usuario que se nos ha pasado, realizamos un commit y cerramos la sesión
            sesion.save(u);
            transaccion.commit();
             //sesion.close();
            
            return u.getId();
        }
        catch(HibernateException e)
        {
            throw new UsuarioDAOException("No se pudo realizar la transacción con éxito",e);     
        }    
    }
    /*Método que modifica los campos de un usuario con los valores del usuario que le
    pasamos por parámetro*/
    @Override
    public void updateUsuario(Usuario u) throws UsuarioDAOException
    {
        try
        {
            //Aquí sacamos el usuario de la bd en función del id del usuario que se nos pasa
            Usuario usu=getUsuario(u.getId());
            Session sesion =sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            //Le ponemos todos los campos en función del usuario que nos han pasado        
            usu.setNombre(u.getNombre());
            usu.setPwd(u.getPwd());
            usu.setRol(u.getRol());
            usu.setUsu(u.getUsu());
            //Updateamos el usuario, confirmamos la transacción y cerramos la sesión    
            sesion.update(usu);   
            transaccion.commit();
             //sesion.close();
        }
        catch(UsuarioDAOException | HibernateException e)
        {
            throw new UsuarioDAOException("No se pudo modificar el usuario con id " +u.getId(),e);
        }   
    }
    //Método que borra de la base de datos el usuario perteneciente a la idUsuario que le pasamos
    @Override
    public void delUsuario(int idUsuario) throws UsuarioDAOException
    { 
        try
        {
            //Obtenemos el usuario que queremos borrar de la bd por medio del id que nos han pasado
            Usuario u=this.getUsuario(idUsuario);
            Session sesion =sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
	    //Borramos y como siempre, confirmamos la transacción y cerramos la sesión.
            sesion.delete(u);
            transaccion.commit();
             //sesion.close();
        }
        catch(UsuarioDAOException | HibernateException e)
        {
            throw new UsuarioDAOException("No se pudo borrar el usuario indicado",e); 
        }
    }
    //Método que devuelve el usuario de la bd correspondiente al idUsuario que le pasamos
    @Override
    public Usuario getUsuario(int idUsuario) throws UsuarioDAOException
    {
        Usuario u=null;
        
        try
        {
            Session sesion = sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
	    /*Obtenemos el usuario mediante el id que se nos pasa, indicando la clase	
            correspondiente a la bd de donde queremos sacar el usuario*/
            u=(Usuario)sesion.get(Usuario.class, idUsuario);
            transaccion.commit();
             //sesion.close();
        }
        catch(HibernateException e)
        {
            throw new UsuarioDAOException("No se pudo obtener el usuario indicado",e);
        }    
            
        return u;
    }
    //Método que devuelve una lista de usuarios sacados de nuestra bd.
    @Override
    public List<Usuario> getUsuarios() throws UsuarioDAOException
    {
        try
        {
            Session sesion = sessionFactory.getCurrentSession();
            Transaction transaccion = sesion.beginTransaction();
            //Realizamos una query para obtener todos los usuarios
            Query q = sesion.createQuery("FROM Usuario"); //sesion.createQuery("FROM Usuarios");
            //Metemos el resultado en la lista anteriormente definida y cerramos la sesion
            List<Usuario> listaUsuarios = q.list();
             //sesion.close();
            
            if (listaUsuarios ==null)
                listaUsuarios = new ArrayList<>();
            transaccion.commit();
            
            return listaUsuarios;
        }
        catch(HibernateException e)
        {
            throw new UsuarioDAOException("No se pudo obtener la lista de usuarios",e);
        }
    }
    //Método que cierra una sessionFactory
    @Override
    public void finalizar() throws UsuarioDAOException
    {
        try
        {
            //Cerramos la sessionFactory
            sessionFactory.close();
        }
        catch (HibernateException e)
        {
            throw new UsuarioDAOException("Error al cerrar la sessionFactory", e);
        }
    }
}
