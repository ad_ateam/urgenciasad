/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.iespacomolla.urg.vistas;

/**
 * Esta excepción se lanza cuando se produce un error de Login
 * @author ANDRES Angel
 */
public class LoginException extends Exception
{
    public boolean logOut = false;
    
    public LoginException(Throwable causa)
    {
        super("Error en login", causa);
    }
    
    /**
     * Construye un objeto de tipo {@code LoginException}
     * @param usuario Usuario que intenta loguearse
     * @param error Tipo de error {@code TipoErrorLogin} que origina el error
     */
    public LoginException(String usuario, TipoErrorLogin error)
    {
        super(error == TipoErrorLogin.USUARIO_NO_EXISTE
            ?
              String.format("El usuario '%s' no existe", usuario)
            :
              String.format("Contraseña incorrecta para usuario '%s'", usuario)
            );
    }
    
    /**
     * Construye un objeto de la clase {@code LoginException}
     * @param usuario Usuario que intentaba logearse
     * @param cause Causa del error que impidió el logueo de usuario
     */
    public LoginException(String usuario, Throwable causa)
    {
        super("Error en Login para el usuario '" + usuario + "'", causa);
    }
    
    public LoginException(boolean logout)
    {
        super("Logout de usuario");
        this.logOut = logout;
    }
}
