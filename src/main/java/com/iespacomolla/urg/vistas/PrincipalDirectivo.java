/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
package com.iespacomolla.urg.vistas;
 
import com.iespacomolla.urg.entidades.EstadoIncidencia;
import com.iespacomolla.urg.entidades.Incidencia;
import com.iespacomolla.urg.entidades.Medico;
import com.iespacomolla.urg.entidades.Paciente;
import com.iespacomolla.urg.entidades.TipoIncidencia;
import com.iespacomolla.urg.servicio.MedicoService;
import com.iespacomolla.urg.servicio.MedicoServiceException;
import com.iespacomolla.urg.servicio.PacienteService;
import com.iespacomolla.urg.servicio.PacienteServiceException;
import com.iespacomolla.urg.servicio.UrgenciasService;
import com.iespacomolla.urg.servicio.UrgenciasServiceException;
import java.util.Calendar;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
 
/**
 *
 * @author ANDRES Angel
 */
public class PrincipalDirectivo extends VistaBase
{
    private UrgenciasService us = null;
    private PacienteService ps = null;
    private MedicoService ms = null;
   
    public PrincipalDirectivo() throws UrgenciasServiceException, PacienteServiceException, MedicoServiceException
    {
        us = new UrgenciasService();
        ps = new PacienteService();
        ms = new MedicoService();
    }
 
    @Override
    public void mostrarMenu()
    {
        // Controla la salida del bucle do-while
        boolean salir = false;
        do
        {
            System.out.println("-- GESTION DIRECTIVA --");
            System.out.println("1. Todas las incidencias");
            System.out.println("2. Incidencias Históricas");
            System.out.println("3. Todos los pacientes");
            System.out.println("4. Pacientes atendidos en este momento");
            System.out.println("5. Incidencias criticas atendidas en este momento");
            System.out.println("6. Incidencias criticas historicas");
            System.out.println("7. Tiempo medio de atención de incidencias LEVES");
            System.out.println("8. Tiempo medio de atención de incidencias GRAVES");
            System.out.println("9. Tiempo medio de atención de incidencias CRITICAS");
            System.out.println("10. Numero de pacientes en la cola de incidencias GRAVES");
            System.out.println("11. Medico con mas incidencias resueltas");
            System.out.println("12. Medico que ha atendido mas incidencias CRITICAS");
            System.out.println("13. Listar incidencias resueltas por un medico");
            System.out.println("0. Salir");
            System.out.print("Opción -> ");
 
            int opcion = Integer.MIN_VALUE;
            try
            {
                // Lee la opción desde teclado
                opcion = sc.nextInt();
                // Limpia el buffer tras la lectura de un número
                sc.nextLine();
               
                switch (opcion)
                {
                    case 1:
                        listarTodasIncidencias();
                        break;
                    case 2:
                        listarIncidenciasHistoricas();
                        break;
                    case 3:
                        listarTodosPacientes();
                        break;
                    case 4:
                        listarPacientesEnAtencion();
                        break;
                    case 5:
                        listarIncidenciasCriticasActivas();
                        break;
                    case 6:
                        listarIncidenciasCriticasHistoricas();
                        break;
                    case 7:
                        mostrarTiempoMedioAtencionIncidenciasLeves();
                        break;
                    case 8:
                        mostrarTiempoMedioAtencionIncidenciasGraves();
                        break;
                    case 9:
                        mostrarTiempoMedioAtencionIncidenciasCriticas();
                        break;
                    case 10:
                        numeroPacientesEnColaGraves();
                        break;
                    case 11:
                        medicoConMasIncidenciasResueltas();
                        break;
                    case 12:
                        medicoConMasIncidenciasCriticas();
                        break;
                    case 13:
                        listarIncidenciasResueltasMedico();
                        break;
                    case 0:
                        salir = true;
                        break;
                    default:
                        System.out.println("- Opción incorrecta.");
                        break;
                }
            }
            catch (InputMismatchException e)
            {
                sc.nextLine();
               
                // Si la opcion es Integer.MIN_VALUE el error se ha producido al leer desde teclado
                if(opcion == Integer.MIN_VALUE)
                    System.err.println("Opción incorrecta. El valor introducido debe ser numérico");
                // Error numérico producido en alguna de las subopciones del menu
                else
                    mostrarError(e);
            }
            catch (Exception e)
            {
                mostrarError(e);
            }
        } while (!salir);
       
        System.out.println("-- CERRANDO GESTION DIRECTIVA --");
       
        // Controla excepciones en cierre de hibernate y de la entrada estandar
        try
        {
            us.finalizar();
        }
        catch (Exception e)
        {
            System.err.println("Error al finalizar el servicio de Urgencias.\n" + e.getMessage());
        }
        try
        {
            ps.finalizar();
        }
        catch (Exception e)
        {
            System.err.println("Error al finalizar el servicio de Pacientes.\n" + e.getMessage());
        }
    }
   
    private void listarIncidencias(List<Incidencia> li)
    {
        for (Incidencia i : li)
        {
            System.out.printf("%d '%s' %d", i.getTipo(), i.getDescripcion(), i.getEstado());
        }
    }
   
    private void listarPacientes(List<Paciente> li)
    {
        for (Paciente p : li)
        {
            System.out.printf("Codigo: %d, Nombre: %s, Numero S.S.  %s", p.getId(), p.getNombre(), p.getNSegSoc());
        }
    }    
       
    private void listarTodasIncidencias()
    {
       
        System.out.println("Todas las Incidencias");
       
        try
        {
            List<Incidencia> listaIncidencias= us.obtenerTodasIncidencias();
            listarIncidencias(listaIncidencias);
        }
        catch (UrgenciasServiceException e)
        {
            mostrarError(e);
        }
    }
   
    private void listarIncidenciasHistoricas()
    {
        try
        {
            List<Incidencia> li = us.obtenerIncidencias(null, EstadoIncidencia.HISTORICA);
           
            if (li != null && li.size() > 0)
            {
                System.out.println("Incidencias criticas historicas");
                listarIncidencias(li);
            }
            else
                System.out.println("No hay incidencias críticas en el sistema");
        }
        catch (UrgenciasServiceException ex)
        {
            mostrarError(ex);
        }
    }
   
    private void listarIncidenciasCriticasActivas()
    {
        try
        {
            List<Incidencia> li = us.obtenerIncidencias(TipoIncidencia.CRITICA, EstadoIncidencia.ACTIVA);
           
            if (li != null && li.size() > 0)
            {
                System.out.println("Incidencias criticas activas");
                listarIncidencias(li);
            }
            else
                System.out.println("No hay incidencias críticas en el sistema");
        }
        catch (UrgenciasServiceException ex)
        {
            mostrarError(ex);
        }
    }
    private void listarIncidenciasCriticasHistoricas()
    {
        try
        {
            List<Incidencia> li = us.obtenerIncidencias(TipoIncidencia.CRITICA, EstadoIncidencia.HISTORICA);
           
            if (li != null && li.size() > 0)
            {
                System.out.println("Incidencias criticas historicas");
                listarIncidencias(li);
            }
            else
                System.out.println("No hay incidencias críticas en el sistema");
        }
        catch (UrgenciasServiceException ex)
        {
            mostrarError(ex);
        }
    }
    private void listarIncidenciasResueltasMedico()
    {
        int idMedico;
       
        System.out.println("Introduzca el id del medico cuyas incidencias desea consultar:");
       
        idMedico=sc.nextInt();
        sc.nextLine();
       
        System.out.println("Incidencias resueltas por el medico con id: " + idMedico);
       
        try
        {
            List<Incidencia> listaIncidencias= us.listaIncidenciasResueltasMedico(idMedico);
            listarIncidencias(listaIncidencias);
        }
        catch (UrgenciasServiceException e)
        {
            mostrarError(e);
        }
    }
    private void listarTodosPacientes()
    {
        try
        {
            List<Paciente> li = ps.obtenerTodosPacientes();
           
            if (li != null && li.size() > 0)
            {
                System.out.println("Todos los pacientes");
                listarPacientes(li);
            }
            else
                System.out.println("No hay pacientes dados de alta en el sistema");
        }
        catch (PacienteServiceException ex)
        {
            mostrarError(ex);
        }
    }
    private void listarPacientesEnAtencion()
    {
        try
        {
            List<Paciente> li = ps.obtenerPacientesEnAtencion();
           
            if (li != null && li.size() > 0)
            {
                System.out.println("Pacientes en atencion");
                listarPacientes(li);
            }
            else
                System.out.println("No hay pacientes en atencion");
        }
        catch (PacienteServiceException ex)
        {
            mostrarError(ex);
        }
    }
    private void mostrarTiempoMedioIncidencias(List<Incidencia> li)
    {
        System.out.print("Tiempo medio de atención: ");
       
        if (li.size() == 0)
        {
            System.out.println("Sin incidencias");
            return;
        }
       
        int numIncidenciasHistoricas = 0;
        long sumaTiemposIncidencias = 0;
       
        for (Incidencia i : li)
        {
            if (i.getEstado() == EstadoIncidencia.HISTORICA &&
                i.getFechaAtencion() != null &&
                i.getFechaSalida() != null)
            {
                long t1 = i.getFechaAtencion().getTime();
                long t2 = i.getFechaSalida().getTime();
               
                if (t2 < t1)
                {
                    sumaTiemposIncidencias += (t2 - t1);
                    numIncidenciasHistoricas++;
                }
            }
        }
       
        if (sumaTiemposIncidencias  > 0)
        {
            Calendar c = Calendar.getInstance();
            c.setTime(new Date(sumaTiemposIncidencias / numIncidenciasHistoricas));
           
            int h = c.get(Calendar.HOUR_OF_DAY);
            int m  = c.get(Calendar.MINUTE);
            int s = c.get(Calendar.SECOND);
           
            if (h != 0) System.out.println(h + " horas");
            if (m != 0) System.out.println(m + " minutos");
            if (s != 0) System.out.println(s + " segundos");
        }
    }
    private void mostrarTiempoMedioAtencionIncidenciasLeves()
    {
        try
        {
            System.out.println("Incidencias LEVES");
            List<Incidencia> li = us.obtenerIncidencias(TipoIncidencia.LEVE, EstadoIncidencia.HISTORICA);
            mostrarTiempoMedioIncidencias(li);
        }
        catch (UrgenciasServiceException ex)
        {
            mostrarError(ex);
        }
    }
    private void mostrarTiempoMedioAtencionIncidenciasGraves()
    {
        try
        {
            System.out.println("Incidencias GRAVES");
            List<Incidencia> li = us.obtenerIncidencias(TipoIncidencia.GRAVE, EstadoIncidencia.HISTORICA);
            mostrarTiempoMedioIncidencias(li);
        }
        catch (UrgenciasServiceException ex)
        {
            mostrarError(ex);
        }
    }
    private void mostrarTiempoMedioAtencionIncidenciasCriticas()
    {
        try
        {
            System.out.println("Incidencias CRITICAS");
            List<Incidencia> li = us.obtenerIncidencias(TipoIncidencia.CRITICA, EstadoIncidencia.HISTORICA);
            mostrarTiempoMedioIncidencias(li);
        }
        catch (UrgenciasServiceException ex)
        {
            mostrarError(ex);
        }
    }
    private void numeroPacientesEnColaGraves()
    {
        try
        {
            List<Paciente> li = ps.obtenerPacientesEnColaGraves();
           
            if (li != null && li.size() > 0)
            {
                System.out.printf("Hay %d pacientes en espera con incidencias graves", li.size());
                //listarPacientes(li);
            }
            else
                System.out.println("No hay pacientes en espera con incidencias graves");
        }
        catch (PacienteServiceException ex)
        {
            mostrarError(ex);
        }
    }
    private void medicoConMasIncidenciasCriticas()
    {
        try
        {
            Medico m= us.medicoConMasIncidenciasCriticas();
            
           
            if(m!=null)
            {
               
                    System.out.println("id:"+m.getIdMedico()+" Nombre:"+m.getNombre()+" Estado:"+m.getEstado()+"\n");
                
            }
        }
        catch(UrgenciasServiceException e)
        {
            mostrarError(e);
        }
    }
    private void medicoConMasIncidenciasResueltas() throws MedicoServiceException
    {
        try
        {
            Medico med = null;
            int numResueltas;
            int num=0;
            List<Medico> medicos= ms.ObtenerTodosMedicos();
           
            if(medicos!=null && medicos.size() > 0)
            {
                for(Medico m:medicos)
                {
                    List <Incidencia> numIncidencias=us.listaIncidenciasResueltasMedico(m.getIdMedico());
                    numResueltas=numIncidencias.size();
                    //guarda el medico con mas icidencias
                    if(numResueltas>num)
                    {
                        med=m;
                        num=numResueltas;
                    }      
                }
                System.out.println("id:"+med.getIdMedico()+" Nombre:"+med.getNombre()+" Estado:"+med.getEstado()+"\n");
            }
        }
        catch(UrgenciasServiceException e)
        {
            mostrarError(e);
        }
    }
}