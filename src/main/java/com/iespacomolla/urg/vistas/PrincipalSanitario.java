package com.iespacomolla.urg.vistas;

import com.iespacomolla.urg.entidades.Incidencia;
import com.iespacomolla.urg.entidades.Medico;
import com.iespacomolla.urg.entidades.Paciente;
import com.iespacomolla.urg.servicio.MedicoService;
import com.iespacomolla.urg.servicio.MedicoServiceException;
import com.iespacomolla.urg.servicio.PacienteService;
import com.iespacomolla.urg.servicio.PacienteServiceException;
import com.iespacomolla.urg.servicio.UrgenciasService;
import com.iespacomolla.urg.servicio.UrgenciasServiceException;
import com.iespacomolla.urg.servicio.UsuarioService;
import com.iespacomolla.urg.servicio.UsuarioServiceException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Random;

/**
 * Esta clase representa el menu principal del grupo administrativo
 * Permite el Alta, Actualización, Baja y la optención de Informes de Usuarios y personal Sanitario
 *
 * @author ANDRES Angel
 */
public class PrincipalSanitario extends VistaBase
{
    public static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    private static final String ERR_ID_INCORRECTO_STR = "El id debe ser numérico";

    /**
     * Servicio que gestiona Usuarios
     */
    private UsuarioService us = null;
    /**
     * Servicio que gestiona Personal Sanitario
     */
    private MedicoService ms = null;
    /**
     * Servicio que gestiona urgencias
     */
    private PacienteService ps = null;
    /**
     * Servicio que gestiona urgencias
     */
    private UrgenciasService ur = null;

    /**
     * Construye un objeto de clase {@code PrincipalAdministrativo} que representa un menu gestor de usuarios y medicos
     *
     * @throws UsuarioServiceException Se lanza cuando se produce un error en el servicio de Usuarios
     * @throws MedicoServiceException  Se lanza cuando se produce un error en el servicio de Medicos
     */
    public PrincipalSanitario() throws UsuarioServiceException, MedicoServiceException, UrgenciasServiceException, PacienteServiceException
    {
        us = new UsuarioService();
        ms = new MedicoService();
        ps = new PacienteService();
        ur = new UrgenciasService();
    }

    @Override
    public void mostrarMenu()
    {
        // Controla la salida del bucle do-while
        boolean salir = false;
        do
        {
            System.out.println("-- GESTION SANITARIA --");
            System.out.println("1. Nuevo Paciente");
            System.out.println("2. Modificar Paciente");
            System.out.println("3. Eliminar Paciente");
            System.out.println("4. Iniciar Turno Medico");
            System.out.println("5. Finalizar Turno Medico");
            System.out.println("6. Nueva Incidencia");
            System.out.println("7. Finalizar incidencia");
            System.out.println("8. Listar médicos en activo");
            System.out.println("9. Listar médicos disponibles");
            System.out.println("10. Listar incidencias en espera");
            System.out.println("11. Listar incidencias de paciente");
            System.out.println("12. Listar pacientes en espera");
            System.out.println("0. Salir");
            System.out.print("Opción -> ");

            int opcion = Integer.MIN_VALUE;
            try
            {
                // Lee la opción desde teclado
                opcion = sc.nextInt();
                // Limpia el buffer tras la lectura de un número
                sc.nextLine();

                switch (opcion)
                {
                    case 1:
                        nuevoPaciente();
                        break;
                    case 2:
                        modificarPaciente();
                        break;
                    case 3:
                        eliminarPaciente();
                        break;
                    case 4:
                        InicioTurnoMedico();
                        break;
                    case 5:
                        finTurnoMedico();
                        break;
                    case 6:
                        nuevaIncidencia();
                        break;
                    case 7:
                        finalizarIncidencia();
                        break;
                    case 8:
                        medicosActivos();
                        break;
                    case 9:
                        medicosDisponibles();
                        break;
                    case 10:
                        incidenciasEnEspera();
                        break;
                    case 11:
                        incidenciasDePaciente();
                        break;
                    case 12:
                        pacientesEnEspera();
                        break;
                    case 0:
                        salir = true;
                        break;
                    default:
                        System.out.println("- Opción incorrecta.");
                        break;
                }
            }
            catch (InputMismatchException e)
            {
                sc.nextLine();

                // Si la opcion es Integer.MIN_VALUE el error se ha producido al leer desde teclado
                if (opcion == Integer.MIN_VALUE)
                {
                    System.err.println("Opción incorrecta. El valor introducido debe ser numérico");
                }
                // Error numérico producido en alguna de las subopciones del menu
                else
                {
                    mostrarError(e);
                }
            }
            catch (PacienteServiceException | MedicoServiceException | UrgenciasServiceException e)
            {
                mostrarError(e);
            }

        }
        while (!salir);

        System.out.println("-- CERRANDO GESTION ADMINISTRATIVA --");

        // Controla excepciones en cierre de hibernate y de la entrada estandar
        try
        {
            us.finalizar();
        }
        catch (Exception e)
        {
            System.err.println("Error al finalizar el servicio de Usuario.\n" + e.getMessage());
        }
        try
        {
            ms.finalizar();
        }
        catch (Exception e)
        {
            System.err.println("Error al finalizar el servicio de Medicos.\n" + e.getMessage());
        }
        try
        {
            ps.finalizar();
        }
        catch (Exception e)
        {
            System.err.println("Error al finalizar el servicio de Pacientes.\n" + e.getMessage());
        }
        try
        {
            ur.finalizar();
        }
        catch (Exception e)
        {
            System.err.println("Error al finalizar el servicio de Urgencias.\n" + e.getMessage());
        }
    }

    private void InicioTurnoMedico() throws MedicoServiceException, UrgenciasServiceException
    {
        Medico m = null;
        System.out.println("\n-- INICIAR TURNO MEDICO --\n");
        try
        {
            // Piede el id de usuario para buscarlo en la bbdd
            System.out.print("Introduzca el id del usuario a modificar: ");
            int id = sc.nextInt();
            sc.nextLine();

            // Pide al servicio el Usuario correspondiente al id introducido
            m = ms.ObtenerMedico(id);

            // Si el usuario no existe informa y termina
            if (m == null)
            {
                System.out.println("No existe un medico con id " + id);
            }
            else
            {
                ur.ComenzarTurno(m.getIdMedico());
            }
        }
        catch (InputMismatchException e)
        {
            if (m == null)
            {
                throw new InputMismatchException(ERR_ID_INCORRECTO_STR);
            }
            else
            {
                throw new InputMismatchException(e.getMessage());
            }
        }
        catch (MedicoServiceException e)
        {
            throw new MedicoServiceException(e.getMessage());
        }
    }

    private void finTurnoMedico() throws MedicoServiceException, UrgenciasServiceException, InputMismatchException
    {
        Medico m = null;
        System.out.println("\n-- INICIAR TURNO MEDICO --\n");
        try
        {
            // Piede el id de usuario para buscarlo en la bbdd
            System.out.print("Introduzca el id del usuario a modificar: ");
            int id = sc.nextInt();
            sc.nextLine();

            // Pide al servicio el Usuario correspondiente al id introducido
            m = ms.ObtenerMedico(id);

            // Si el usuario no existe informa y termina
            if (m == null)
            {
                System.out.println("No existe un medico con id " + id);
            }
            else
            {
                ur.FinalizarTurno(m.getIdMedico());
            }
        }
        catch (InputMismatchException e)
        {
            if (m == null)
            {
                throw new InputMismatchException(ERR_ID_INCORRECTO_STR);
            }
            else
            {
                throw new InputMismatchException(e.getMessage());
            }
        }
        catch (MedicoServiceException e)
        {
            throw new MedicoServiceException(e.getMessage());
        }
    }

    public void nuevoPaciente() throws PacienteServiceException
    {
        try
        {

            List<Paciente> pacientes = ps.obtenerTodosPacientes();
            String nombre;
            String seguridadSocial;

            System.out.print("Nombre:");
            nombre = br.readLine();
            System.out.print("Numero de seguridad social:");
            seguridadSocial = br.readLine();

            //Recorro todos los pacientes para ver si existe
            for (Paciente p : pacientes)
            //Compruevo si ya hay algun paciente registrado con ese numero de seguridad social
            {
                if (p.getNSegSoc().equals(seguridadSocial))
                {
                    throw new PacienteServiceException("El paciente ya existe");
                }
            }

            ps.nuevoPaciente(nombre, seguridadSocial);

        }
        catch (PacienteServiceException | IOException e)
        {
            throw new PacienteServiceException("Error al insertar el paciente", e);
        }
    }

    public void modificarPaciente() throws PacienteServiceException
    {
        try
        {
            String nombre;
            int id;
            String seguridadSocial;

            System.out.print("Introduzca id del paciente a actualizar:\n");
            System.out.print("Id:");
            id = Integer.parseInt(br.readLine());

            if (ps.obtenerPaciente(id) == null)
            {
                throw new PacienteServiceException("No existe paciente con id " + id);
            }

            System.out.print("Introduzca los datos a actualizar:\n");
            System.out.print("Nombre:");
            nombre = br.readLine();
            System.out.print("Numero de seguridad social:");
            seguridadSocial = br.readLine();

            ps.actualizarPaciente(id, nombre, seguridadSocial);
            System.out.println("Paciente actualizado");

        }
        catch (PacienteServiceException | IOException e)
        {
            throw new PacienteServiceException("Error al actualizar el paciente", e);
        }
    }

    public void eliminarPaciente() throws PacienteServiceException
    {
        try
        {
            Integer id;

            System.out.print("Introduzca id del paciente a eliminar:\n");
            System.out.print("Id:");
            id = Integer.parseInt(br.readLine());

            if (ps.obtenerPaciente(id) == null)
            {
                throw new PacienteServiceException("No existe paciente con id " + id);
            }

            ps.eliminarPaciente(id);
            System.out.println("Paciente eliminado");

        }
        catch (PacienteServiceException | IOException e)
        {
            throw new PacienteServiceException("Error al actualizar el paciente", e);
        }
    }

    public void nuevaIncidencia() throws PacienteServiceException
    {
        try
        {
            System.out.println("Descripcion de la incidencia: ");
            String descripcionIncidencia = br.readLine();
            System.out.println("Selecciona el tipo de incidencia: "
                               + "\n1.Leve"
                               + "\n2.Grave"
                               + "\n3.Critica\n");
            int incidencia = Integer.parseInt(br.readLine());

            switch (incidencia)
            {
                case 1:
                    this.nuevaIncidenciaLeve(descripcionIncidencia);
                    break;
                case 2:
                    this.nuevaIncidenciaGrave(descripcionIncidencia);
                    break;
                case 3:
                    this.nuevaIncidenciaCritica(descripcionIncidencia);
                    break;
                default:
                    break;
            }
        }
        catch (Exception e)
        {
            throw new PacienteServiceException("No se ha podido registrar la incidencia", e);
        }

    }

    public void nuevaIncidenciaLeve(String incidencia) throws Exception
    {
        try
        {
            String nombre;
            String seguridadSocial;
            int idpac = 0;
            Paciente paciente = null;

            System.out.print("Numero de Seguridad Social: ");
            seguridadSocial = br.readLine();

            paciente = ps.obtenerPaciente(seguridadSocial);

            // Es un paciente nuevo
            if (paciente == null)
            {
                System.out.print("Nombre: ");
                nombre = br.readLine();
                idpac = ps.nuevoPaciente(nombre, seguridadSocial);
            }
            else
                idpac = paciente.getId();

            ur.registrarIncidenciaLeve(idpac, incidencia);

        }
        catch (Exception e)
        {
            throw new Exception("Error al insertar la incidencia", e);
        }

    }

    public void nuevaIncidenciaGrave(String incidencia) throws Exception
    {
        try
        {
            List<Paciente> pacientes = ps.obtenerTodosPacientes();

            String nombre;
            String seguridadSocial;
            int idpac = 0;
            Paciente paciente = null;

            System.out.print("Numero de Seguridad Social: ");
            seguridadSocial = br.readLine();

            paciente = ps.obtenerPaciente(seguridadSocial);

            // Es un paciente nuevo
            if (paciente == null)
            {
                System.out.print("Nombre: ");
                nombre = br.readLine();
                idpac = ps.nuevoPaciente(nombre, seguridadSocial);
            }

            ur.registrarIncidenciaGrave(idpac, incidencia);

        }
        catch (Exception e)
        {
            throw new Exception("Error al insertar la incidencia", e);
        }
    }

    public void nuevaIncidenciaCritica(String incidencia) throws Exception
    {

        try
        {

            String seguridadSocial;
            System.out.print("Numero de Seguridad Social: ");
            seguridadSocial = br.readLine();

            Paciente p = ps.obtenerPaciente(seguridadSocial);
            
            //Si existe el paciente lo registro y si no solo le meto id -1 , le paso  la incidencia y lo registro
            if (p == null)
            {
                p.setId(-1);
               
            }
               ur.registrarIncidenciaCritica(p.getId(), incidencia);
            
        }
        catch (Exception e)
        {
            throw new Exception("Error al insertar la incidencia", e);
        }
    }

    public void finalizarIncidencia() throws PacienteServiceException
    {
        try
        {
            System.out.print("Introduce id para finalizar la incidencia: ");
            int idIncidencia = Integer.parseInt(br.readLine());
            ur.finalizarIncidencia(idIncidencia);

        }
        catch (Exception e)
        {
            throw new PacienteServiceException("No se ha pudo finalizar la incidencia", e);
        }
    }

    private void incidenciasDePaciente() throws PacienteServiceException
    {
        System.out.println("\n-- LISTAR INCIDENCIAS DE PACIENTE --\n");
        System.out.print("Introduzca el paciente a buscar en las incidencias: ");
        int id = sc.nextInt();
        sc.nextLine();

        try
        {
            List<Incidencia> lista = ps.obtenerIncidenciasPaciente(id);

            if (lista.isEmpty())
            {
                System.out.println("No hay incidencias de este paciente");
            }
            else
            {
                for (Incidencia i : lista)
                {
                    System.out.println("id:" + i.getIdIncidencia() + " descripcion:" + i.getDescripcion() + "\n");
                }
            }
        }
        catch (PacienteServiceException e)
        {
            throw new PacienteServiceException(e.getMessage());
        }
    }

    private void pacientesEnEspera() throws PacienteServiceException
    {
        System.out.println("\n-- LISTAR PACIENTES EN ESPERA --\n");
        try
        {
            List<Paciente> lista = ps.obtenerPacientesEnEspera();
            if (lista.isEmpty())
            {
                System.out.println("No hay i ncidencias de este paciente");
            }
            else
            {
                for (Paciente p : lista)
                {
                    System.out.println("id:" + p.getId() + " nombre:" + p.getNombre() + " Seguridad Social:" + p.getNSegSoc() + "\n");
                }
            }
        }
        catch (PacienteServiceException e)
        {
            throw new PacienteServiceException(e.getMessage());
        }
    }

    private void incidenciasEnEspera() throws UrgenciasServiceException
    {
        System.out.println("\n-- LISTAR INCIDENCIAS EN ESPERA --\n");
        try
        {
            List<Incidencia> lista = ur.ListaIncidenciasEnEspera();
            if (lista.isEmpty())
            {
                System.out.println("No hay incidencias en espera");
            }
            else
            {
                for (Incidencia i : lista)
                {
                    System.out.println("id:" + i.getIdIncidencia() + " descripcion:" + i.getDescripcion() + "\n");
                }
            }
        }
        catch (UrgenciasServiceException e)
        {
            throw new UrgenciasServiceException(e.getMessage());
        }
    }

    private void medicosDisponibles() throws MedicoServiceException
    {
        System.out.println("\n-- LISTAR MEDICOS DISPONIBLES --\n");
        try
        {
            List<Medico> medicosDisponibles = ms.ObtenerMedicosDisponibles();
            if (medicosDisponibles.isEmpty())
            {
                System.out.println("No hay médicos disponibles");
            }
            else
            {
                for (Medico m : medicosDisponibles)
                {
                    System.out.println("id:" + m.getIdMedico() + " nombre:" + m.getNombre() + "\n");
                }
            }
        }
        catch (MedicoServiceException e)
        {
            throw new MedicoServiceException(e.getMessage());
        }
    }

    private void medicosActivos() throws MedicoServiceException
    {
        System.out.println("\n-- LISTAR MEDICOS EN ACTIVO --\n");
        try
        {
            List<Medico> medicosActivos = ms.ObtenerMedicosServicio();
            if (medicosActivos.isEmpty())
            {
                System.out.println("No hay médicos activos");
            }
            else
            {
                for (Medico m : medicosActivos)
                {
                    System.out.println("id:" + m.getIdMedico() + " nombre:" + m.getNombre() + " estado:" + m.getEstado() + "\n");
                }
            }
        }
        catch (MedicoServiceException e)
        {
            throw new MedicoServiceException(e.getMessage());
        }
    }
}
