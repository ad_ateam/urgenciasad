/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.iespacomolla.urg.vistas;

import java.util.Scanner;

/**
 *
 * @author ANDRES Angel
 */
public abstract class VistaBase
{
    protected static Scanner sc = null;
    
    static
    {
        sc = new Scanner(System.in);
    }
    
    public final void mostrarError(Throwable e)
    {
        System.err.println("ERROR");
        while (e != null)
        {
            System.err.println(e.getMessage());
            e = e.getCause();
        }
    }
    public abstract void mostrarMenu();
    
    public static void close()
    {
        if (sc != null)
        {
            sc.close();
        }
    }
}
