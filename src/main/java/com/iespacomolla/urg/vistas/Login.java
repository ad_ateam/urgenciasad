/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.iespacomolla.urg.vistas;

import com.iespacomolla.urg.entidades.TipoRol;
import com.iespacomolla.urg.entidades.Usuario;
import com.iespacomolla.urg.servicio.MedicoServiceException;
import com.iespacomolla.urg.servicio.UsuarioService;
import com.iespacomolla.urg.servicio.UsuarioServiceException;
import static com.iespacomolla.urg.vistas.VistaBase.sc;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase de tipo vista que permite loguearse
 * @author ANDRES Angel
 */
public class Login extends VistaBase
{
    public Usuario UsuarioLogueado = null;
    public boolean Salir = false;
    
    public Login() throws LoginException
    {
        try
        {
            // Se conecta a la base de datos y si no hay un administrador crea uno por defecto
            UsuarioService us = new UsuarioService();
            if (us.obtenerUsuariosAdministradores().isEmpty())
                us.nuevoUsuario("Admin", "admin", "admin", TipoRol.ADMINISTRATIVO.name());
            us.finalizar();
        }
        catch (Exception e)
        {
            throw new LoginException(e);
        }
    }

    @Override
    public void mostrarMenu()
    {
        UsuarioLogueado = null;
        Salir = false;
        
        System.out.println("1. Loguearse");
        System.out.println("0. Salir");

        int opcion = Integer.MIN_VALUE;
        try
        {
            // Lee la opción desde teclado
            opcion = sc.nextInt();
            // Limpia el buffer tras la lectura de un número
            sc.nextLine();

            switch (opcion)
            {
                case 1:
                    UsuarioLogueado = Loguearse();
                    break;
                case 0:
                    Salir = true;
                    break;
                default:
                    throw new Exception("Opcion incorrecta");
            }
        }
        catch (InputMismatchException e)
        {
            sc.nextLine();

            // Si la opcion es Integer.MIN_VALUE el error se ha producido al leer desde teclado
            if(opcion == Integer.MIN_VALUE)
                System.err.println("Opción incorrecta. El valor introducido debe ser numérico");
            // Error numérico producido en alguna de las subopciones del menu
            else
                mostrarError(e);
        }
        catch (Exception e)
        {
            mostrarError(e);
        }
    }
    
    /**
     * Pide usuario y contraseña, evalua los datos introducidos
     * y si son correctos devuelve el {@link Usuario} correspondiente
     * @return Objeto de tipo Usuario logueado
     * @throws LoginException Se dará cuando el login no pueda realizarse
     */
    private Usuario Loguearse() throws LoginException
    {
        String usu = "", pwd = "";
        
        UsuarioService us = null;
        try
        {
            us = new UsuarioService();
            
            System.out.print("Usuario: ");
            usu = sc.nextLine();
            System.out.print("Contraseña: ");
            pwd = sc.nextLine();
            
            // Obtiene todos los usuarios dados de alta
            List<Usuario> users = us.obtenerTodosUsuarios();
            
            // Busca el usuario que coincida en nombre y contraseña
            for (Usuario u : users)
                if (u.getUsu().equals(usu))
                {
                    // Usuario encontrado
                    if (u.getPwd().equals(pwd))
                        return u;
                    // La contrasela para el usuario introducido no es correcta
                    else
                        throw new LoginException(usu, TipoErrorLogin.PWD_INCORRECTO);
                }
            
            // No se ha encontrado ningún usuario con el nombre especificado
            throw new LoginException(usu, TipoErrorLogin.USUARIO_NO_EXISTE);
        }
        catch (UsuarioServiceException ex)
        {
            // Captura el error producido en el DAO de Usuario
            throw new LoginException(usu, ex);
        }
        finally
        {
            if (us != null)
            {
                try
                {
                    us.finalizar();
                }
                catch (Exception ex)
                {
                    throw new LoginException(ex);
                }
            }
        }
    }
}
