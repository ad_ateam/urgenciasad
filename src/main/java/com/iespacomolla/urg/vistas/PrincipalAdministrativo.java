package com.iespacomolla.urg.vistas;

import com.iespacomolla.urg.entidades.EstadoMedico;
import com.iespacomolla.urg.entidades.Medico;
import com.iespacomolla.urg.entidades.TipoRol;
import com.iespacomolla.urg.entidades.Usuario;
import com.iespacomolla.urg.servicio.MedicoService;
import com.iespacomolla.urg.servicio.MedicoServiceException;
import com.iespacomolla.urg.servicio.UsuarioService;
import com.iespacomolla.urg.servicio.UsuarioServiceException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Esta clase representa el menu principal del grupo administrativo
 * Permite el Alta, Actualización, Baja y la optención de Informes de Usuarios y personal Sanitario
 * @author ANDRES Angel
 */
public class PrincipalAdministrativo extends VistaBase
{
    private static final String ERR_ID_INCORRECTO_STR = "El id debe ser numérico";
    
    /**
     * Servicio que gestiona Usuarios
     */
    private UsuarioService us = null;
    /**
     * Servicio que gestiona Personal Sanitario
     */
    private MedicoService ms = null;
    
    /**
     * Construye un objeto de clase {@code PrincipalAdministrativo} que representa un menu gestor de usuarios y medicos
     * @throws UsuarioServiceException Se lanza cuando se produce un error en el servicio de Usuarios
     * @throws MedicoServiceException Se lanza cuando se produce un error en el servicio de Medicos
     */
    public PrincipalAdministrativo() throws UsuarioServiceException, MedicoServiceException
    {
        us = new UsuarioService();
        ms = new MedicoService();
    }

    /**
     * Muestra el menú principar que permite la gestión de Usuarios y Medicos
     */
    @Override
    public void mostrarMenu()
    {
        // Controla la salida del bucle do-while
        boolean salir = false;
        do
        {
            System.out.println("-- GESTION ADMINISTRATIVA --");
            System.out.println("1. Nuevo usuario");
            System.out.println("2. Actualizar usuario");
            System.out.println("3. Borrar usuario");
            System.out.println("4. Nuevo medico");
            System.out.println("5. Actualizar medico");
            System.out.println("6. Borrar medico");
            System.out.println("7. Listado de usuarios");
            System.out.println("8. Listado de medicos");
            System.out.println("9. Medicos activos");
            System.out.println("0. Salir");
            System.out.print("Opción -> ");

            int opcion = Integer.MIN_VALUE;
            try
            {
                // Lee la opción desde teclado
                opcion = sc.nextInt();
                // Limpia el buffer tras la lectura de un número
                sc.nextLine();
                
                switch (opcion)
                {
                    case 1:
                        nuevoUsuario();
                        break;
                    case 2:
                        actualizarUsuario();
                        break;
                    case 3:
                        borrarUsuario();
                        break;
                    case 4:
                        nuevoMedico();
                        break;
                    case 5:
                        actualizarMedico();
                        break;
                    case 6:
                        borrarMedico();
                        break;
                    case 7:
                        listarUsuarios();
                        break;
                    case 8:
                        listarTodosMedicos();
                        break;
                    case 9:
                        listarMedicosActivos();
                        break;
                    case 0:
                        salir = true;
                        break;
                    default:
                        System.out.println("- Opción incorrecta.");
                        break;
                }
            }
            catch (InputMismatchException e)
            {
                sc.nextLine();
                
                // Si la opcion es Integer.MIN_VALUE el error se ha producido al leer desde teclado
                if(opcion == Integer.MIN_VALUE)
                    System.err.println("Opción incorrecta. El valor introducido debe ser numérico");
                // Error numérico producido en alguna de las subopciones del menu
                else
                    mostrarError(e);
            }
            catch (UsuarioServiceException | MedicoServiceException e)
            {
                mostrarError(e);
            }
        } while (!salir);
        
        System.out.println("-- CERRANDO GESTION ADMINISTRATIVA --");
        
        // Controla excepciones en cierre de hibernate y de la entrada estandar
        try
        {
            us.finalizar();
        }
        catch (Exception e)
        {
            System.err.println("Error al finalizar el servicio de Usuario.\n" + e.getMessage());
        }
        try
        {
            ms.finalizar();
        }
        catch (Exception e)
        {
            System.err.println("Error al finalizar el servicio de Medicos.\n" + e.getMessage());
        }
    }
    
    /* --- USUARIOS --- */
    
    /**
     * Ofrece un menú para la creación de un nuevo Usuario
     * @throws UsuarioServiceException Se lanza cuando se produce un error en la creación de un nuevo Usuario
     */
    private void nuevoUsuario() throws UsuarioServiceException
    {
        System.out.println("\n-- NUEVO USUARIO --\n");
        try
        {
            System.out.print("Nombre    : ");
            String nombre = sc.nextLine();
            
            System.out.print("Usuario   : ");
            String usu = sc.nextLine();
            
            System.out.print("Contraseña: ");
            String pwd = sc.nextLine();
            if (pwd.length() < 5)
                throw new InputMismatchException("La contraseña debe tener al menos 5 caracteres");
            
            System.out.println("Roles:");
            System.out.println("(A) - " + TipoRol.ADMINISTRATIVO.name());
            System.out.println("(D) - " + TipoRol.DIRECTIVO.name());
            System.out.println("(S) - " + TipoRol.SANITARIO.name());
            System.out.print("Rol       : ");
            String rol = sc.nextLine().toLowerCase();
            switch (rol)
            {
                case "a":
                    rol = TipoRol.ADMINISTRATIVO.name();
                    break;
                case "d":
                    rol = TipoRol.DIRECTIVO.name();
                    break;
                case "s":
                    rol = TipoRol.SANITARIO.name();
                    break;
                default:
                    throw new InputMismatchException("Rol incorrecto");
            }
            
            us.nuevoUsuario(nombre, usu, pwd, rol);
            
            System.out.println("\n-- USUARIO CREADO CORRECTAMENTE --\n");
        }
        catch (InputMismatchException e)
        {
            throw new InputMismatchException(e.getMessage());
        }
        catch (UsuarioServiceException e)
        {
            throw new UsuarioServiceException("No se pudo crear el usuario", e);
        }
    }

    /**
     * Ofrece un menú para la actualización de un Usuario
     * @throws UsuarioServiceException Se lanza cuando se produce un error en la actualización de un Usuario
     */
    private void actualizarUsuario() throws InputMismatchException, UsuarioServiceException
    {
        Usuario u = null;
        
        System.out.println("\n-- ACTUALIZAR USUARIO --\n");
        try
        {
            // Piede el id de usuario para buscarlo en la bbdd
            System.out.print("Introduzca el id del usuario a modificar: ");
            int id = sc.nextInt();
            sc.nextLine();
            
            // Pide al servicio el Usuario correspondiente al id introducido
            u = us.obtenerUsuario(id);
            
            // Si el usuario no existe informa y termina
            if (u == null)
            {
                System.out.println("No existe un usuario con id " + id);
                return;
            }
            
            // Valores temporales para la modificación del usuario cargados con los valores actuales
            String nombre = u.getNombre();
            String usu = u.getUsu();
            String pwd = u.getPwd();
            
            // Controla si ha habido algún cambio y activa la opción de actualizar
            boolean cambiosPendientes = false;
            
            do
            {                
                System.out.println("Cambios para el usuario " + u.getNombre());
                
                // No se ha dado un nuevo valor para el nombre
                if (nombre.equals(u.getNombre()))
                    System.out.println("1. Modificar nombre");
                // Muestra el nuevo nombre para el usuario (introducido en la iteración anterior)
                else
                    System.out.println("1. Modificar nombre (" + nombre + ")");
                
                if (usu.equals(u.getUsu()))
                    System.out.println("2. Modificar usuario");
                else
                    System.out.println("2. Modificar usuario (" + usu + ")");
                
                if (pwd.equals(u.getPwd()))
                    System.out.println("3. Modificar contraseña");
                else
                    System.out.println("3. Modificar contraseña (modificada)");
                
                // Activa la opción de aplicar cambios
                if (cambiosPendientes)
                    System.out.println("4. Aplicar cambios");
                
                System.out.println("0. Cancelar");
                System.out.print("Elija opción: ");
                
                int opcion = sc.nextInt();
                sc.nextLine(); // Limpia el buffer
                
                switch (opcion)
                {
                    case 1:
                        System.out.print("Nuevo nombre: ");
                        nombre = sc.nextLine();
                        cambiosPendientes = true;
                        break;
                    case 2:
                        System.out.print("Nuevo usuario: ");
                        usu = sc.nextLine();
                        cambiosPendientes = true;
                        break;
                    case 3:
                        System.out.print("Nueva contraseña: ");
                        pwd = sc.nextLine();
                        if (pwd.length() < 5)
                            throw new InputMismatchException("La contraseña debe tener al menos 5 caracteres");
                        cambiosPendientes = true;
                        break;
                    case 4:
                        // Solo permite la actualización del usuario si se han introducido nuevos valores
                        if (cambiosPendientes)
                        {
                            // Actualiza el usuario con los valores actuales y el mismo rol (ya que no se permite modificarlo)
                            us.actualizarUsuario(id, nombre, usu, pwd, u.getRol().name());
                            System.out.println("\n-- USUARIO ACTUALIZADO CORRECTAMENTE --\n");
                            return;
                        }
                        else
                        {
                            System.err.println("Opción incorrecta");
                            break;
                        }
                    case 0: // Cancelar
                        System.out.println("\n-- OPERACIÓN CANCELADA --\n");
                        return;
                    default:
                        System.err.println("Opción incorrecta");
                        break;
                }
            }
            while (true);
        }
        // Captura y relanza una InputMismatchException para indicar error de conversión
        catch (InputMismatchException e)
        {
            if (u == null)
                throw new InputMismatchException(ERR_ID_INCORRECTO_STR);
            else
                throw new InputMismatchException(e.getMessage());
        }
        catch (UsuarioServiceException e)
        {
            throw new UsuarioServiceException(e.getMessage());
        }
    }

    /**
     * Ofrece un menú para el borrado de un Usuario
     * @throws UsuarioServiceException Se lanza cuando se produce un error en el borrado de un Usuario
     */
    private void borrarUsuario() throws InputMismatchException, UsuarioServiceException
    {
        System.out.println("\n-- BORRAR USUARIO --\n");
        
        try
        {
            // Pide el id del usuario a borrar
            System.out.print("Introduzca el id del usuario a borrar: ");
            int id = sc.nextInt();
            sc.nextLine();
            
            // Si el usuario no existe informa y termina
            if (us.obtenerUsuario(id) == null)
                System.out.println("No existe un usuario con id " + id);
            else
            {
                us.eliminarUsuario(id);
                System.out.println("\n-- USUARIO BORRADO CORRECTAMENTE --\n");
            }
        }
        catch (InputMismatchException e)
        {
            throw new InputMismatchException(ERR_ID_INCORRECTO_STR);
        }
        catch (UsuarioServiceException e)
        {
            throw new UsuarioServiceException(e.getMessage());
        }
    }
    
    /**
     * Muestra un listado con todos los Usuarios almacenados en la bbdd
     */
    private void listarUsuarios() throws UsuarioServiceException
    {
        System.out.println("\n-- LISTADO DE USUARIOS --");
        
        try
        {
            List<Usuario> lu = us.obtenerTodosUsuarios();
            
            if (lu.isEmpty())
                System.out.println("No hay usuarios");
            else
            {
                // Máximas longitudes de las columnas a mostrar. Esto es meramente estético
                int idl = 3;
                int noml = 6;
                int usul = 7;
                for (Usuario u : lu)
                {
                    String idstr = String.valueOf(u.getId());
                    if (idstr.length() > 3)
                        idl = idstr.length();
                    if (u.getNombre().length() > noml)
                        noml = u.getNombre().length();
                    if (u.getUsu().length() > usul)
                        usul = u.getUsu().length();
                }
                
                // Formato utilizado para maquetar la salida por terminal en base a las longitudes máximas
                String formato = "%" + idl + "s | %-" + noml + "s | %" + usul + "s | %s ";

                // Escribe la cabecera con el formato anterior
                System.out.println(String.format(formato, "ID", "NOMBRE", "USUARIO", "ROL"));

                // Formato adaptado al tipo int del id
                formato = "%" + idl + "d | %-" + noml + "s | %" + usul + "s | %s ";

                // Escribe cada Usuario siguiendo el formato de impresión
                for (Usuario u : lu)
                    System.out.println(
                            String.format(
                                    formato,
                                    u.getId(),
                                    u.getNombre(),
                                    u.getUsu(),
                                    u.getRol().name()));
            }
            
            System.out.println();
        }
        catch (UsuarioServiceException e)
        {
            throw new UsuarioServiceException("Error al mostrar todos los usuarios: " + e.getLocalizedMessage());
        }
    }
    
    /* --- MEDICOS --- */
    
    /**
     * Ofrece un menú para la creación de un nuevo Medico
     * @throws MedicoServiceException Se lanza cuando se produce un error en la creación de un nuevo Medico
     */
    private void nuevoMedico() throws MedicoServiceException
    {
	System.out.println("\n-- NUEVO MEDICO --\n");
        try
        {
            System.out.print("Nombre: ");
            ms.nuevoMedico(sc.nextLine());
            
            System.out.println("\n-- MEDICO CREADO CORRECTAMENTE --\n");
        }
        catch (MedicoServiceException e)
        {
            throw new MedicoServiceException("Error al crear medico", e);
        }
    }
    
    /**
     * Ofrece un menú para la actualización de un Medico
     * @throws MedicoServiceException Se lanza cuando se produce un error en la actualización de un Medico
     */
    private void actualizarMedico() throws MedicoServiceException
    {
        Medico m = null;
        
        System.out.println("\n-- ACTUALIZAR MEDICO --\n");
        try
        {
            // Piede el id del medico para buscarlo en la bbdd
            System.out.print("Introduzca el id del medico a modificar: ");
            int id = sc.nextInt();
            sc.nextLine();
            
            // Pide al servicio el Medico correspondiente al id introducido
            m = ms.ObtenerMedico(id);
            
            // Si el medico no existe informa y termina
            if (m == null)
            {
                System.out.println("No existe un medico con id " + id);
                return;
            }
            
            // Valores temporales para la modificación del medico cargados con los valores actuales
            String nombre = m.getNombre();
            EstadoMedico estado = m.getEstado();
            
            // Controla si ha habido algún cambio y activa la opción de actualizar
            boolean cambiosPendientes = false;
            
            do
            {                
                System.out.println("\nModificar medico '" + m.getNombre() + "'");
                
                // No se ha dado un nuevo valor para el nombre
                if (nombre.equals(m.getNombre()))
                    System.out.println("1. Modificar nombre");
                // Muestra el nuevo nombre para el medico (introducido en la iteración anterior)
                else
                    System.out.println("1. Modificar nombre (" + nombre + ")");
                
                if (estado == m.getEstado())
                    System.out.println("2. Modificar estado");
                else
                    System.out.println("2. Modificar estado (" + estado.name() + ")");
                
                // Activa la opción de aplicar cambios
                if (cambiosPendientes)
                    System.out.println("3. Aplicar cambios");
                
                System.out.println("0. Cancelar");
                System.out.print("Elija opción: ");
                
                int opcion = sc.nextInt();
                sc.nextLine(); // Limpia el buffer
                
                switch (opcion)
                {
                    case 1:
                        System.out.print("Nuevo nombre: ");
                        nombre = sc.nextLine();
                        cambiosPendientes = true;
                        break;
                    case 2:
                        System.out.println("Cambiar estado '" + estado.name() + "' por:");
                        if (m.getEstado() != EstadoMedico.DISPONIBLE)
                            System.out.println("(D) - Disponible");
                        if (m.getEstado() != EstadoMedico.FUERA_SERV)
                            System.out.println("(F) - Fuera de servicio");
                        if (m.getEstado() != EstadoMedico.OCUPADO)
                            System.out.println("(O) - Ocupado");
                        
                        System.out.print("Nuevo estado: ");
                        String st = sc.nextLine().toLowerCase();
                        switch (st)
                        {
                            case "d":
                                estado = EstadoMedico.DISPONIBLE;
                                break;
                            case "f":
                                estado = EstadoMedico.FUERA_SERV;
                                break;
                            case "o":
                                estado = EstadoMedico.OCUPADO;
                                break;
                            default:
                                throw new InputMismatchException("Opción de estado de medico incorrecta");
                        }
                        
                        cambiosPendientes = true;
                        break;
                    case 3:
                        // Solo permite la actualización del medico si se han introducido nuevos valores
                        if (cambiosPendientes)
                        {
                            // Actualiza el medico con los valores actuales
                            ms.actualizarMedico(id, nombre, estado.name());
                            System.out.println("\n-- MEDICO ACTUALIZADO CORRECTAMENTE --\n");
                            return;
                        }
                        else
                        {
                            System.err.println("Opción incorrecta");
                            break;
                        }
                    case 0: // Cancelar
                        System.out.println("-- OPERACIÓN CANCELADA --\n");
                        return;
                    default:
                        System.err.println("- Error - Opción incorrecta");
                        break;
                }
            }
            while (true);
        }
        catch (InputMismatchException e)
        {
            if (m == null)
                throw new InputMismatchException("Valor para id de medico incorrecto");
            else
                throw new InputMismatchException("Opción incorrecta");
        }
        catch (MedicoServiceException e)
        {
            throw new MedicoServiceException("Error al actualizar medico", e);
        }
    }
    
    /**
     * Ofrece un menú para el borrado de un Medico
     * @throws UsuarioServiceException Se lanza cuando se produce un error en el borrado de un Medico
     */
    private void borrarMedico() throws InputMismatchException, MedicoServiceException
    {
        System.out.println("\n-- BORRAR MEDICO --\n");
        
        try
        {
            // Pide el id del medico a borrar
            System.out.print("Introduzca el id del medico a borrar: ");
            int id = sc.nextInt();
            sc.nextLine();
            
            // Si el medico no existe informa y termina
            if (ms.ObtenerMedico(id) == null)
                System.out.println("No existe un medico con id " + id);
            else
            {
                ms.eliminaMedico(id);
                System.out.println("\n-- MEDICO BORRADO CORRECTAMENTE --\n");
            }
        }
        catch (InputMismatchException e)
        {
            throw new InputMismatchException("Error. El id debe ser numérico");
        }
        catch (MedicoServiceException e)
        {
            throw new MedicoServiceException("Error al borrar medico", e);
        }
    }
    
    /**
     * Muestra un listado con todos los Medicos almacenados en la bbdd
     */
    private void listarTodosMedicos()
    {
        System.out.println("\n-- LISTADO DE MEDICOS --");
        
        try
        {
            List<Medico> lm = ms.ObtenerTodosMedicos();
            
            if (lm.isEmpty())
                System.out.println("No hay médicos almacenados");
            else
            {
                // Máximas longitudes de las columnas a mostrar. Esto es meramente estético
                int idl = 3;
                int noml = 6;
                for (Medico m : lm)
                {
                    String idstr = String.valueOf(m.getIdMedico());
                    if (idstr.length() > 3)
                        idl = idstr.length();
                    if (m.getNombre().length() > noml)
                        noml = m.getNombre().length();
                }
                
                // Formato utilizado para maquetar la salida por terminal en base a las longitudes maximas
                String formato = "%" + idl + "s | %-" + noml + "s | %s ";

                // Escribe la cabecera con el formato anterior
                System.out.println(String.format(formato, "ID", "NOMBRE", "ESTADO"));

                // Formato adaptado al tipo int del id
                formato = "%" + idl + "d | %-" + noml + "s | %s ";

                // Escribe cada Medico siguiendo el formato de impresión
                for (Medico m : lm)
                    System.out.println(
                            String.format(
                                    formato,
                                    m.getIdMedico(),
                                    m.getNombre(),
                                    m.getEstado()));
            }
            
            System.out.println();
        }
        catch (MedicoServiceException e)
        {
            System.err.println("Error al mostrar todos los medicos: " + e.getLocalizedMessage());
        }
    }
    
    /**
     * Muestra un listado con todos los Medicos activos almacenados en la bbdd
     */
    private void listarMedicosActivos()
    {
        System.out.println("\n-- LISTADO DE MEDICOS EN ACTIVOS --");
        
        try
        {
            List<Medico> lm = ms.ObtenerMedicosServicio();
            
            if(lm.isEmpty())
                System.out.println("No hay médicos en activo");
            else
            {
                int idl = 3;
                for (Medico m : lm)
                {
                    String idstr = String.valueOf(m.getIdMedico());
                    if (idstr.length() > idl)
                        idl = idstr.length();
                }
                
                // Formato utilizado para maquetar la salida por terminal
                String formato = "%" + idl + "s | %s ";

                // Escribe la cabecera con el formato anterior
                System.out.println(String.format(formato, "ID", "NOMBRE"));

                // Formato adaptado al tipo int del id
                formato = "%" + idl + "d | %s ";
        
                // Escribe cada Medico siguiendo el formato de impresión
                for (Medico m : lm)
                    System.out.println(
                            String.format(
                                    formato,
                                    m.getIdMedico(),
                                    m.getNombre()));
            }
            
            System.out.println();
        }
        catch (MedicoServiceException e)
        {
            System.err.println("Error al mostrar los medicos en activo: " + e.getLocalizedMessage());
        }
    }
}


