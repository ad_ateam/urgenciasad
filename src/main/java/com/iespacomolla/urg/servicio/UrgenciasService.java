package com.iespacomolla.urg.servicio;

import com.iespacomolla.urg.datos.IncidenciaDAO;
import com.iespacomolla.urg.datos.IncidenciaDAOException;

import com.iespacomolla.urg.datos.PacienteDAOException;
import com.iespacomolla.urg.entidades.EstadoIncidencia;
import static com.iespacomolla.urg.entidades.EstadoIncidencia.*;
import com.iespacomolla.urg.entidades.EstadoMedico;
import static com.iespacomolla.urg.entidades.EstadoMedico.*;
import com.iespacomolla.urg.entidades.Incidencia;
import com.iespacomolla.urg.entidades.Medico;
import com.iespacomolla.urg.entidades.Paciente;
import com.iespacomolla.urg.entidades.TipoIncidencia;
import static com.iespacomolla.urg.entidades.TipoIncidencia.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 *
 * @author ANDRES Angel
 */
public class UrgenciasService implements IService
{
    // Modificado de DAO a Service para obtenerer control de integridad de datos y evitar 
    // errores de sesiones de hibernate
    private MedicoService ms = null;
    private PacienteService ps = null;
    private IncidenciaDAO idao = null;

    private Queue<Incidencia> incidenciasNormales = new LinkedList();
    private Queue<Incidencia> incidenciasGraves = new LinkedList();

    public UrgenciasService() throws UrgenciasServiceException
    {
        try
        {
            ms = new MedicoService();
            ps = new PacienteService();
            idao = new IncidenciaDAO();

        }

        catch (MedicoServiceException | PacienteServiceException | IncidenciaDAOException e)
        {
            throw new UrgenciasServiceException("No se ha podido crear el servicio de usuarios", e);
        }
    }

    public MedicoService getServicioMedico()
    {
        return ms;
    }

    public PacienteService getServicioPaciente()
    {
        return ps;
    }

    public void ComenzarTurno(int idMedico) throws UrgenciasServiceException
    {
        Medico m = null;
        try
        {
            m = ms.ObtenerMedico(idMedico);
        }
        catch (MedicoServiceException e)
        {
            throw new UrgenciasServiceException("No se ha podido crear el servicio de usuarios", e);
        }

        if (m == null || m.getEstado() != FUERA_SERV)
        {
            throw new UrgenciasServiceException("El medico no es valido o ya esta de servicio");
        }

        try
        {
            setMedicoDisponible(m);
        }
        catch (UrgenciasServiceException e)
        {
            throw new UrgenciasServiceException("No se ha podido actualizar el medico", e);
        }
    }

    private EstadoMedico setMedicoDisponible(Medico m) throws UrgenciasServiceException
    {
        try
        {
            Incidencia i = null;

            if (incidenciasGraves.size() > 0)
            {
                i = incidenciasGraves.remove();
            }
            else if (incidenciasNormales.size() > 0)
            {
                i = incidenciasNormales.remove();
            }

            if (i != null)
            {
                i.setMedico(m);
                m.setEstado(OCUPADO);
                i.setEstado(EstadoIncidencia.ACTIVA);
                idao.updateIncidencia(i);
            }
            else
            {
                m.setEstado(DISPONIBLE);
            }

            ms.actualizarMedico(m);

            return m.getEstado();
        }
        catch (MedicoServiceException | IncidenciaDAOException e)
        {
            throw new UrgenciasServiceException("No se ha podido actualizar el medico", e);
        }
    }

    public void FinalizarTurno(int idMedico) throws UrgenciasServiceException
    {
        Medico m = null;
        try
        {
            m = ms.ObtenerMedico(idMedico);
        }
        catch (MedicoServiceException e)
        {
            throw new UrgenciasServiceException("No se ha podido crear el servicio de usuarios", e);
        }

        if (m == null || m.getEstado() != DISPONIBLE)
        {
            throw new UrgenciasServiceException("El medico no es valido o ya esta de servicio");
        }

        if (EstaAtendiendo(m.getIdMedico()))
        {
            throw new UrgenciasServiceException("El medico no es valido porque esta ocupado atendiendo una incidencia");
        }

        try
        {
            m.setEstado(FUERA_SERV);

            ms.actualizarMedico(m.getIdMedico(), m.getNombre(), m.getEstado().toString());
        }
        catch (MedicoServiceException e)
        {
            throw new UrgenciasServiceException("No se ha podido actualizar el medico", e);
        }
    }

    /**
     * Devuelve true si el médico está atendiento una incidencia.
     * Dicha indidencia estara con estado ACTIVA
     *
     * @param idMedico Identificador del médico a buscar
     *
     * @return true si el medico está asociado a una incidencia ACTIVA
     * @throws UrgenciasServiceException Error al recoger incidencias
     */
    public boolean EstaAtendiendo(int idMedico) throws UrgenciasServiceException
    {
        try
        {
            List<Incidencia> lista = idao.getIncidenciasMedico(idMedico);
            for (Incidencia i : lista)
            {
                if (i.getEstado() == ACTIVA)
                {
                    return true;
                }
            }
        }
        catch (IncidenciaDAOException e)
        {
            throw new UrgenciasServiceException("Error al recoger las incidencias", e);
        }
        return false;
    }

    @Override
    public void finalizar() throws Exception
    {
        try
        {
            ms.finalizar();
        }
        catch (MedicoServiceException e)
        {
            throw new MedicoServiceException("Se ha producido un error al finalizar", e);
        }

        try
        {
            ps.finalizar();
        }
        catch (PacienteDAOException e)
        {
            throw new MedicoServiceException("Se ha producido un error al finalizar", e);
        }
    }

    public Queue<Incidencia> getColaIncidenciasNormales()
    {
        return incidenciasNormales;
    }

    public Queue<Incidencia> getColaIncidenciasGraves()
    {
        return incidenciasGraves;
    }

    public int registrarIncidenciaLeve(int idPaciente, String descripcion) throws UrgenciasServiceException
    {
        Incidencia i = new Incidencia();
        i.setDescripcion(descripcion);
        i.setTipo(LEVE);

        Date fecha = Calendar.getInstance().getTime();
        i.setFechaEntrada(fecha);

        try
        {
            // Busca y asigna el paciente a la incidencia
            Paciente p = ps.obtenerPaciente(idPaciente);
            if (p == null)
            {
                throw new UrgenciasServiceException("No se encuentra el paciente con id " + idPaciente);
            }

            i.setPaciente(p);

            List<Medico> listMed = ms.ObtenerMedicosDisponibles();

            // Si no hay medicos disponibles pone la incidencia en espera en su cola
            if (listMed.isEmpty())
            {
                i.setEstado(ESPERA);
                if (incidenciasNormales.add(i))
                {
                    i.setNumEspera(incidenciasNormales.size());
                }
                else
                {
                    throw new UrgenciasServiceException("No se ha podido añadir la incidencia leve a la cola");
                }
            }
            else
            {
                i.setEstado(ACTIVA);

                // Obtiene el medico y lo asigna a la nueva incidencia
                Medico m = listMed.get(0);
                i.setMedico(m);
                m.setEstado(OCUPADO);
                ms.actualizarMedico(m);

                Date hora = Calendar.getInstance().getTime();
                i.setFechaAtencion(hora);
            }

            return idao.addIncidencia(i);
        }
        catch (PacienteServiceException | UrgenciasServiceException | MedicoServiceException | IncidenciaDAOException e)
        {
            throw new UrgenciasServiceException("No se pudo registrar la incidencia leve", e);
        }

    }

    public int registrarIncidenciaGrave(int idPaciente, String descripcion) throws UrgenciasServiceException
    {
        Incidencia i = new Incidencia();
        i.setDescripcion(descripcion);
        i.setTipo(GRAVE);

        Date fecha = Calendar.getInstance().getTime();
        i.setFechaEntrada(fecha);

        try
        {
            // Busca y asigna el paciente a la incidencia
            Paciente p = ps.obtenerPaciente(idPaciente);
            if (p == null)
            {
                throw new UrgenciasServiceException("No se encuentra el paciente con id " + idPaciente);
            }

            i.setPaciente(p);

            List<Medico> listMed = ms.ObtenerMedicosDisponibles();

            // Si no hay medicos disponibles pone la incidencia en espera en su cola
            if (listMed.isEmpty())
            {
                i.setEstado(ESPERA);
                if (incidenciasGraves.add(i))
                {
                    i.setNumEspera(incidenciasGraves.size());
                }
                else
                {
                    throw new UrgenciasServiceException("No se ha podido añadir la incidencia grave a la cola");
                }
            }
            else
            {
                i.setEstado(ACTIVA);

                // Obtiene el medico y lo asigna a la nueva incidencia
                Medico m = listMed.get(0);
                i.setMedico(m);
                m.setEstado(OCUPADO);
                ms.actualizarMedico(m);

                Date hora = Calendar.getInstance().getTime();
                i.setFechaAtencion(hora);
            }

            return idao.addIncidencia(i);
        }
        catch (PacienteServiceException | UrgenciasServiceException | MedicoServiceException | IncidenciaDAOException e)
        {
            throw new UrgenciasServiceException("No se pudo registrar la incidencia grave", e);
        }
    }

    public int registrarIncidenciaCritica(int idPaciente, String descripcion) throws UrgenciasServiceException
    {
        // El médico elegido para atender la incidencia. Se define aqui por si hay errores restaurar su estado anterior
        Medico m = null;

        // Referencia a incidencia, leve o grave, metida en su correspondiente cola.
        // Si es != null deberá volver al sistema y restaurar su estado inicial
        Incidencia iEnCola = null;

        try
        {
            // Nueva incidencia con descripción y tipo indicados
            Incidencia i = new Incidencia();
            i.setDescripcion(descripcion);
            i.setTipo(CRITICA);

            // Relaciona el paciente con la incidencia
            Paciente p = ps.obtenerPaciente(idPaciente);
            if (p == null && idPaciente != -1)
            {
                throw new UrgenciasServiceException("No se ha encontrado el paciente de id " + idPaciente);
            }
            i.setPaciente(p);

            // Activa la incidencia y ajusta las fechas de entrada y atención
            i.setEstado(ACTIVA);
            Date fecha = Calendar.getInstance().getTime();
            i.setFechaEntrada(fecha);

            // Busca médicos disponibles para atender la incidencia
            List<Medico> lm = ms.ObtenerMedicosDisponibles();

            // Si hay médicos dispobibles elegimos, por defecto, el primer médico de la lista
            if (!lm.isEmpty())
            {
                m = lm.get(0);
            }
            else
            {
                // Si no hay médicos disponibles buscamos el médico en una incidencia leve
                List<Incidencia> ll = obtenerIncidencias(LEVE, ACTIVA);
                // Hay, al menos, una incidencia leve de la que se puede sacar el médico
                if (!ll.isEmpty())
                {
                    // Guardamos la cola por si se produce algún error
                    iEnCola = ll.get(0);

                    // Guardamos la incidencia en su cola
                    if (incidenciasNormales.add(iEnCola))
                    {
                        iEnCola.setNumEspera(incidenciasNormales.size());
                    }
                }
                else
                {
                    // Si no hay medicos libres ni incidencias leves se busca en las incidencias graves
                    ll = obtenerIncidencias(GRAVE, ACTIVA);
                    if (!ll.isEmpty())
                    {
                        // Guardamos la cola por si se produce algún error
                        iEnCola = ll.get(0);

                        // Guardamos la incidencia en su cola
                        if (incidenciasGraves.add(iEnCola))
                        {
                            iEnCola.setNumEspera(incidenciasGraves.size());
                        }
                    }
                    else
                    {
                        throw new UrgenciasServiceException("No hay ningun médico para atender esta incidencia. Llamar por megafonía");
                    }
                }

                // Se ha sacado una incidencia del sistema y se ha puesto en su cola correspondiente
                if (iEnCola != null)
                {
                    // Se indica que la incidencia esta a la espera de un medico
                    iEnCola.setEstado(ESPERA);

                    // Sacamos el médico de la incidencia, que será quien atienda esta incidencia
                    m = iEnCola.getMedico();
                    iEnCola.setMedico(null);

                    // Actualiza la incidencia en el sistema
                    idao.updateIncidencia(iEnCola);
                }
            }

            if (m == null)
            {
                throw new UrgenciasServiceException("No se ha encontrado medico para atender la incidencia");
            }

            // Relaciona el médico con la nueva incidencia
            i.setMedico(m);
            // Cambia el estado al médico, que ahora estará ocupado con esta incidencia
            m.setEstado(OCUPADO);
            ms.actualizarMedico(m);

            i.setFechaAtencion(Calendar.getInstance().getTime());

            return idao.addIncidencia(i);
        }
        catch (MedicoServiceException | UrgenciasServiceException | IncidenciaDAOException | PacienteServiceException e)
        {
            // Se ha sacado una incidencia del sistema y se ha metido en una cola.
            // Deberá restaurarse el estado anterior
            if (iEnCola != null)
            {
                // Quita la incidencia de la cola correspondiente si se habia añadido
                if (iEnCola.getTipo() == LEVE && incidenciasNormales.contains(iEnCola))
                {
                    incidenciasNormales.remove();
                }
                else if (iEnCola.getTipo() == GRAVE && incidenciasGraves.contains(iEnCola))
                {
                    incidenciasGraves.remove();
                }

                // El médico era el que estaba asignado a esta incidencia de la cola. Se debe restaurar su union
                if (m != null)
                {
                    iEnCola.setMedico(m);
                }

                // Restaura el estado de incidencia a ACTIVA
                iEnCola.setEstado(ACTIVA);

                try
                {
                    idao.updateIncidencia(iEnCola);
                }
                catch (IncidenciaDAOException ei)
                {
                    throw new UrgenciasServiceException("Error al restaruar incidencia de cola", ei);
                }
            }
            // Si hay un error y no hay incidencia en cola el médico estaba disponible y puede haber cambiado su estado
            // Se debe restaurar a disponible para evitar errores
            else if (m != null)
            {
                try
                {
                    m.setEstado(DISPONIBLE);
                    ms.actualizarMedico(m.getIdMedico(), m.getNombre(), m.getEstado().toString());
                }
                catch (MedicoServiceException em)
                {
                    throw new UrgenciasServiceException("Error al restaurar estado de médico", em);
                }
            }

            throw new UrgenciasServiceException("No se ha podido registrar la incidencia", e);
        }
    }

    public List<Incidencia> obtenerTodasIncidencias() throws UrgenciasServiceException
    {

        try
        {
            return idao.getIncidencias();
        }
        catch (IncidenciaDAOException ex)
        {
            throw new UrgenciasServiceException("No se ha podido obtener todas las incidencias", ex);
        }
    }

    public List<Incidencia> obtenerIncidencias(TipoIncidencia tipo, EstadoIncidencia estado) throws UrgenciasServiceException
    {
        try
        {
            return idao.getIncidendias(tipo, estado);
        }
        catch (IncidenciaDAOException ex)
        {
            throw new UrgenciasServiceException(
                    String.format(
                            "No se ha podido obtener las incidencias %d - %d",
                            tipo.ordinal(), estado.ordinal()),
                    ex);
        }
    }

    public List<Incidencia> listaIncidenciasResueltasMedico(int idMedico) throws UrgenciasServiceException
    {
        List<Incidencia> incidencias = null;
        List<Incidencia> incidenciasResueltas = null;

        try
        {
            incidencias = idao.getIncidenciasMedico(idMedico);
            incidenciasResueltas = new ArrayList<>();

            if (incidencias != null && incidencias.size() > 0)
            {
                for (Incidencia i : incidencias)
                {
                    if (i.getEstado() == EstadoIncidencia.HISTORICA)
                    {
                        incidenciasResueltas.add(i);
                    }
                }
            }
        }
        catch (IncidenciaDAOException ide)
        {
            throw new UrgenciasServiceException("No se pudo obtener la lista de incidencias resueltas por el medico indicado", ide);
        }
        return incidenciasResueltas;
    }

    public List<Incidencia> ListaIncidenciasEnEspera() throws UrgenciasServiceException
    {
        List<Incidencia> lista = null;
        List<Incidencia> listaenespera = new ArrayList<>();

        try
        {
            lista = idao.getIncidencias();

            for (Incidencia i : lista)
            {
                if (i.getEstado() == EstadoIncidencia.ESPERA)
                {
                    listaenespera.add(i);
                }
            }
        }
        catch (IncidenciaDAOException e)
        {
            throw new UrgenciasServiceException("No se pudo obtener la lista de incidencias en espera", e);
        }
        return listaenespera;
    }

    public void finalizarIncidencia(int idIncidencia) throws UrgenciasServiceException
    {
        try
        {
            Incidencia incidencia = idao.getIncidencia(idIncidencia);

            if (incidenciasGraves.size() > 0
                && incidenciasGraves.peek().getIdIncidencia() == incidencia.getIdIncidencia())
            {
                throw new UrgenciasServiceException("La incidencia esta en la cola de GRAVES");
            }
            else if (incidenciasNormales.size() > 0
                     && incidenciasNormales.peek().getIdIncidencia() == incidencia.getIdIncidencia())
            {
                throw new UrgenciasServiceException("La incidencia esta en la cola de LEVES");
            }

            incidencia.setEstado(HISTORICA);
            Date fecha = Calendar.getInstance().getTime();
            incidencia.setFechaSalida(fecha);
            idao.updateIncidencia(incidencia);

            setMedicoDisponible(incidencia.getMedico());
        }
        catch (IncidenciaDAOException | UrgenciasServiceException e)
        {
            throw new UrgenciasServiceException("No se ha podido finalizar la incidencia");
        }
    }

    public void finalizarIncidenciaFinTurno(int idIncidencia) throws UrgenciasServiceException
    {
        try
        {
            Incidencia incidencia = idao.getIncidencia(idIncidencia);

            if (incidenciasGraves.size() > 0
                && incidenciasGraves.peek().getIdIncidencia() == incidencia.getIdIncidencia())
            {
                throw new UrgenciasServiceException("La incidencia esta en la cola de GRAVES");
            }
            else if (incidenciasNormales.size() > 0
                     && incidenciasNormales.peek().getIdIncidencia() == incidencia.getIdIncidencia())
            {
                throw new UrgenciasServiceException("La incidencia esta en la cola de LEVES");
            }

            incidencia.setEstado(HISTORICA);
            Date fecha = Calendar.getInstance().getTime();
            incidencia.setFechaSalida(fecha);
            idao.updateIncidencia(incidencia);

            incidencia.getMedico().setEstado(FUERA_SERV);
            ms.actualizarMedico(incidencia.getMedico());
        }
        catch (IncidenciaDAOException | MedicoServiceException | UrgenciasServiceException e)
        {
            throw new UrgenciasServiceException("No se ha podido finalizar la incidencia");
        }
    }

    public Medico medicoConMasIncidenciasCriticas() throws UrgenciasServiceException
    {
        List<Medico> medicosIncidencias = new ArrayList<Medico>();
        List<List<Incidencia>> incidenciasPorMedico = new ArrayList<>();
        Medico med = null;
        try
        {
            int max = 0;
            List<Medico> medicos = ms.ObtenerTodosMedicos();
            List<Incidencia> incidencias = idao.getIncidendias(CRITICA, HISTORICA);

            for (Medico m : medicos)
            {
                int num = 0;
                List<Incidencia> ipormed = new ArrayList<>();
                for (Incidencia i : incidencias)
                {
                    if (i.getMedico() != null && i.getMedico().getIdMedico() == m.getIdMedico())
                    {
                        num++;
                        ipormed.add(i);
                    }
                }
                if (num > max)
                {
                    max = num;
                    medicosIncidencias.clear();
                    medicosIncidencias.add(m);
                    
                    incidenciasPorMedico.clear();
                    incidenciasPorMedico.add(ipormed);
                }
                else if (num == max)
                {
                    medicosIncidencias.add(m);
                    incidenciasPorMedico.add(ipormed);
                }
            }

            if (medicosIncidencias.isEmpty())
                return null;
            else if (medicosIncidencias.size() == 1)
                return medicosIncidencias.get(0);
            
            Date fecha = null;
            Incidencia IncidenciaReciente = null;
            int indice = 0;
            // Recorre las listas de incidencias por medico
            for (int i = 0; i < incidenciasPorMedico.size(); i++)
            {
                // Para cada incidencia se evalua fecha posterior
                for (Incidencia in : incidenciasPorMedico.get(i))
                {
                    if (fecha == null ||
                       (in.getFechaSalida() != null && in.getFechaSalida().after(fecha)))
                    {
                        fecha = in.getFechaSalida();
                        IncidenciaReciente = in;
                        indice = i; // Guarda el indice de la lista con la incidencia mas reciente
                    }
                }
            }
            
            if (IncidenciaReciente !=null)
            {
                return medicosIncidencias.get(indice);
            }
            
            /*
             * Este codigo buscaba en todas las incidencias
             * La optimizacion anterior solo buscara por las incidencias de los medicos
             * con el mismo numero de incidencias
            for (Medico m : medicosIncidencias)
            {
                for (Incidencia i : incidencias)
                {
                    if (i.getMedico() != null && i.getFechaSalida() != null &&
                        i.getMedico().getIdMedico() == m.getIdMedico())
                    {
                        if (fecha == null || i.getFechaSalida().after(fecha))
                        {
                            fecha = i.getFechaSalida();
                            IncidenciaReciente = i;
                        }
                    }
                }
            }

            if (IncidenciaReciente != null)
            {
                med = IncidenciaReciente.getMedico();
            }
            else
            {
                throw new UrgenciasServiceException("ha habido un problema al encontrar al medico");
            }*/
        }
        catch (MedicoServiceException | IncidenciaDAOException e)
        {
            throw new UrgenciasServiceException("Ha habido un problema al encontrar a los medicos con mas incidencias", e);
        }

        return med;
    }
}
