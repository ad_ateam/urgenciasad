/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iespacomolla.urg.servicio;

import com.iespacomolla.urg.datos.IPacienteDAO;
import com.iespacomolla.urg.datos.IncidenciaDAOException;
import com.iespacomolla.urg.datos.MedicoDAOException;
import com.iespacomolla.urg.datos.PacienteDAO;
import com.iespacomolla.urg.datos.PacienteDAOException;
import com.iespacomolla.urg.entidades.EstadoIncidencia;
import com.iespacomolla.urg.entidades.Incidencia;
import com.iespacomolla.urg.entidades.Medico;
import com.iespacomolla.urg.entidades.Paciente;
import com.iespacomolla.urg.entidades.TipoIncidencia;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Hibernate;

/**
 *
 * @author Propietario
 */
public class PacienteService implements IService
{
    private IPacienteDAO idao = null;

    public PacienteService() throws PacienteServiceException
    {
        try
        {
            idao = new PacienteDAO();
        }

        catch (Exception e)
        {
            throw new PacienteServiceException("No se pudo crear el servicio de pacientes", e);
        }
    }

    public int nuevoPaciente(String nombre, String nSoc) throws PacienteServiceException
    {
        Paciente p = new Paciente();

        if (nombre.length() < 5)
        {
            throw new PacienteServiceException("El nombre del paciente es demasiado corto");
        }

        if (nSoc.length() != 12)
        {
            throw new PacienteServiceException("El código de la SS introducido no es correcto");
        }

        p.setNombre(nombre);
        p.setNSegSoc(nSoc);

        try
        {
            return idao.addPaciente(p);
        }

        catch (PacienteDAOException pde)
        {
            throw new PacienteServiceException("No se pudo crear el paciente", pde);
        }
    }

    public void actualizarPaciente(int id, String nombre, String nSoc) throws PacienteServiceException
    {
        Paciente p = null;
        try
        {
            p = idao.getPaciente(id);
            if (p == null)
            {
                throw new PacienteServiceException("No existe un paciente con id " + id);
            }
        }
        catch (PacienteDAOException e)
        {
            throw new PacienteServiceException("No se pudo crear el paciente", e);
        }

        if (nombre.length() < 5)
        {
            throw new PacienteServiceException("El nombre del paciente es demasiado corto");
        }

        if (nSoc.length() != 12)
        {
            throw new PacienteServiceException("El código de la SS introducido no es correcto");
        }

        try
        {
            p.setNombre(nombre);
            p.setNSegSoc(nSoc);

            idao.updatePaciente(p);
        }

        catch (PacienteDAOException pde)
        {
            throw new PacienteServiceException("No se pudo crear el paciente", pde);
        }
    }

    public void eliminarPaciente(int idPaciente) throws PacienteServiceException
    {
        try
        {
            Paciente p = idao.getPaciente(idPaciente);
            if (p == null)
                throw new PacienteServiceException("El paciente a eliminar no existe");
            if (p.getIncidencias() != null && !p.getIncidencias().isEmpty())
                throw new PacienteServiceException("El paciente no se pudo eliminar, ya que tiene incidencias en su historial");
            /*if (idao.getPaciente(idPaciente).getIncidencias() != null)
            {
                throw new PacienteServiceException("El paciente no se pudo eliminar, ya que tiene incidencias en su historial");
            }*/
        }
        catch (PacienteDAOException pde)
        {
            throw new PacienteServiceException("No se pudo comprobar si el paciente tiene incidencias");
        }
        catch (NullPointerException e)
        {
            throw new PacienteServiceException("No existe el paciente con id " + idPaciente, e);
        }

        try
        {
            idao.delPaciente(idPaciente);
        }
        catch (PacienteDAOException pde)
        {
            throw new PacienteServiceException("No se pudo realizar el borrado del paciente", pde);
        }
    }
    
    public Paciente obtenerPaciente(int idPaciente) throws PacienteServiceException
    {
        try
        {
            return idao.getPaciente(idPaciente);
        }
        catch (PacienteDAOException e)
        {
            throw new PacienteServiceException("No se ha podido obtener el paciente con id " + idPaciente, e);
        }
    }
    public Paciente obtenerPaciente(String numSS) throws PacienteServiceException
    {
        try
        {
            return idao.getPaciente(numSS);
        }
        catch (PacienteDAOException e)
        {
            throw new PacienteServiceException("No se ha podido obtener el paciente con numero de SS " + numSS, e);
        }
    }

    public List<Paciente> obtenerTodosPacientes() throws PacienteServiceException
    {
        try
        {
            return idao.getPacientes();
        }
        catch (PacienteDAOException pde)
        {
            throw new PacienteServiceException("No se pudo obtener la lista de pacientes", pde);
        }
    }

    public List<Paciente> obtenerPacientesEnEspera() throws PacienteServiceException
    {
        List<Paciente> pacientes = null;
        List<Paciente> pacientesEnEspera = new ArrayList<>();
        List<Incidencia> incidencias = null;

        try
        {
            pacientes = idao.getPacientes();

            for (Paciente p : pacientes)
            {
                incidencias = p.getIncidencias();

                for (Incidencia i : incidencias)
                {
                    if (i.getEstado() == EstadoIncidencia.ESPERA)
                    {
                        pacientesEnEspera.add(p);
                        break;
                    }
                }
            }
        }

        catch (PacienteDAOException pde)
        {
            throw new PacienteServiceException("No se pudo obtener la lista de pacientes en espera", pde);
        }

        return pacientesEnEspera;
    }

    public List<Paciente> obtenerPacientesEnAtencion() throws PacienteServiceException
    {
        List<Paciente> pacientes = null;

        List<Paciente> pacientesEnAtencion = null;
        List<Incidencia> incidencias = null;

        try
        {
            pacientes = idao.getPacientes();
            pacientesEnAtencion = new ArrayList();

            for (Paciente p : pacientes)
            {
                incidencias = p.getIncidencias();

                for (Incidencia i : incidencias)
                {
                    if (i.getEstado() == EstadoIncidencia.ACTIVA)
                    {
                        pacientesEnAtencion.add(p);
                        break;
                    }
                }
            }
        }

        catch (PacienteDAOException pde)
        {
            throw new PacienteServiceException("No se pudo obtener la lista de pacientes en atencion", pde);
        }

        return pacientesEnAtencion;
    }

    public List<Paciente> obtenerPacientesEnAtencionCritica() throws PacienteServiceException
    {
        List<Paciente> pacientes = null;

        List<Paciente> pacientesEnAtencionCritica = null;
        List<Incidencia> incidencias = null;

        try
        {
            pacientes = idao.getPacientes();
            pacientesEnAtencionCritica = new ArrayList();

            for (Paciente p : pacientes)
            {
                incidencias = p.getIncidencias();

                for (Incidencia i : incidencias)
                {
                    if ((i.getEstado() == EstadoIncidencia.ACTIVA) && (i.getTipo() == TipoIncidencia.CRITICA))
                    {
                        pacientesEnAtencionCritica.add(p);
                        break;
                    }
                }
            }
        }

        catch (PacienteDAOException pde)
        {
            throw new PacienteServiceException("No se pudo obtener la lista de pacientes en atencion por una incidencia critica", pde);
        }

        return pacientesEnAtencionCritica;
    }

    public List<Paciente> obtenerPacientesEnColaGraves() throws PacienteServiceException
    {
        List<Paciente> listaPacEspGrav = null;

        try
        {
            listaPacEspGrav = idao.getPacientesColaIncidenciasGraves();
        }

        catch (PacienteDAOException e)
        {
            throw new PacienteServiceException("No se pudo obtener la lista de pacientes con incidencias graves en espera", e);
        }

        return listaPacEspGrav;
    }

    public List<Incidencia> obtenerIncidenciasPaciente(int idPaciente) throws PacienteServiceException
    {
        Paciente p = null;
        List<Incidencia> listaIncidencias = null;
        List<Incidencia> incidencias = null;

        try
        {
            p = idao.getPaciente(idPaciente);
            listaIncidencias = new ArrayList();
            incidencias = p.getIncidencias();

            for (Incidencia i : incidencias)
            {
                listaIncidencias.add(i);
            }
        }

        catch (PacienteDAOException pde)
        {
            throw new PacienteServiceException("No se pudo obtener la lista de incidencias del paciente indicado", pde);
        }

        return listaIncidencias;
    }

    @Override
    public void finalizar() throws Exception
    {
        try
        {
            idao.finalizar();
        }

        catch (PacienteDAOException e)
        {
            throw new PacienteServiceException("Se ha producido un error al finalizar", e);
        }
    }

    
}
