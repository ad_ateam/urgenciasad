/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iespacomolla.urg.servicio;

/**
 *
 * @author Propietario
 */
public class PacienteServiceException extends Exception
{
    
    public PacienteServiceException (String mensaje)
    {
        super(mensaje);
    }
    public PacienteServiceException(String mensaje, Throwable causa)
    {
        super(mensaje, causa);
    }
    
}
