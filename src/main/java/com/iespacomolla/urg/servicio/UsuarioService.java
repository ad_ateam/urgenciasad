package com.iespacomolla.urg.servicio;

import com.iespacomolla.urg.datos.UsuarioDAO;
import com.iespacomolla.urg.datos.UsuarioDAOException;
import com.iespacomolla.urg.entidades.TipoRol;
import com.iespacomolla.urg.entidades.Usuario;
import java.util.ArrayList;
import java.util.List;

/**
 * Servicio para gestionar Usuarios
 * @author ANDRES Angel
 */
public class UsuarioService implements IService
{
    private UsuarioDAO idao = null;

    public UsuarioService() throws UsuarioServiceException
    {
        try
        {
            idao = new UsuarioDAO();
        }
        catch (UsuarioDAOException e)
        {
            throw new UsuarioServiceException("No se ha podido crear el servicio de usuarios", e);
        }
    }

    /**
     * Evalua la integridad de los datos de Usuario
     * @param nombre Nombre de Usuario
     * @param usu Nick de Usuario 
     * @param pwd Contraseña de Usuario
     * @param rol Rol de Usuario
     * @return Devuelve el TipoRol correspondiente a la cadena rol
     * @throws UsuarioServiceException Error de integridad de datos o de conversión de rol
     */
    private TipoRol evaluaDatos(String nombre, String usu, String pwd, String rol) throws UsuarioServiceException
    {
        if (nombre.length() < 3)
            throw new UsuarioServiceException("El nombre de usuario debe tener, al menos, 3 caracteres");

        if (usu.length() < 3)
            throw new UsuarioServiceException("El usuario debe tener, al menos, 3 caracteres");

        if (pwd.length() < 5)
            throw new UsuarioServiceException("La contraseña debe tener, al menos, 5 caracteres");

        // Tipo de rol por defecto, puede ser cualquiera
        TipoRol tr = TipoRol.ADMINISTRATIVO;
        try
        {
            // Convierte el string en tipo rol. Si el rol no es valido, lanzara una excepcion
            tr = TipoRol.valueOf(rol);
        }
        catch (Exception e)
        {
            throw new UsuarioServiceException("El rol introducido no es valido", e);
        }
        
        return tr;
    }
    
    /**
     * Da de alta un nuevo Usuario evaluando previamente la integridad de los datos
     * @param nombre Nombre del nuevo Usuario
     * @param usu Nick del Usuario
     * @param pwd Contraseña del Usuario
     * @param rol Rol de Usuario
     * @throws UsuarioServiceException Error producido en la evaluación de los datos o al guardar el Usuario
     */
    public int nuevoUsuario(String nombre, String usu, String pwd, String rol) throws UsuarioServiceException
    {
        try
        {
            // Evalua los datos de usuario antes de guardarlo
            TipoRol tr = evaluaDatos(nombre, usu, pwd, rol);
        
            // Crea un nuevo Usuario con los datos correctos
            Usuario u = new Usuario(0, nombre, usu, pwd, tr);
            
            // Pasa el nuevo al dao para que lo guarde
            return idao.addUsuario(u);
        }
        catch (UsuarioDAOException e)
        {
            throw new UsuarioServiceException("No se ha podido crear el usuario", e);
        }
    }

    /**
     * Actualiza los datod del Usuario correspondiente al id indicado
     * @param id Identificador de Usuario a actualizar
     * @param nombre Nuevo nombre para el Usuario
     * @param usu Nuevo nick para el Usuario
     * @param pwd Nueva contraseña para el Usuario
     * @param rol Nuevo rol para el Usuario
     * @throws UsuarioServiceException Error de integridad de datos o de actualización
     */
    public void actualizarUsuario(int id, String nombre, String usu, String pwd, String rol) throws UsuarioServiceException
    {
        // Referencia al usuario a actualizar
        Usuario u = null;
        
        try
        {
            // Pide al DAO el usuario con el id indicado
            u = idao.getUsuario(id);
        }
        catch (UsuarioDAOException e)
        {
            throw new UsuarioServiceException("No se ha podido obtener el usuario con id " + id, e);
        }
        
        // Si no existe el usuario lanza una excepcion
        if (u == null)
            throw new UsuarioServiceException("No existe usuario con id " + id);
        
        try
        {
            // Evalua los datos antes de actualizar
            TipoRol tr = evaluaDatos(nombre, usu, pwd, rol);
            
            // Actualiza los campos del usuario
            u.setNombre(nombre);
            u.setUsu(usu);
            u.setPwd(pwd);
            u.setRol(tr);
            
            // Pasa el usuario al DAO para actualizarlo en la bbdd
            idao.updateUsuario(u);
        }
        catch (UsuarioDAOException e)
        {
            throw new UsuarioServiceException("No se ha podido actualizar el usuario", e);
        }
        catch (UsuarioServiceException e)
        {
            throw new UsuarioServiceException("Los datos para actualizar el usuario son incorrectos", e);
        }
    }

    /**
     * Intenta eliminar el usuario correspondiente al id indicado
     * @param idUsuario Identificador de usuario a borrar
     * @throws UsuarioServiceException Se lanza un error cuando el borrado no es posible
     */
    public void eliminarUsuario(int idUsuario) throws UsuarioServiceException
    {
        try
        {
            // Busca el usuario con el id indicado
            Usuario u = idao.getUsuario(idUsuario);
            
            // Si el usuario no existe lanza un error
            if (u == null)
                throw new UsuarioServiceException("No existe un usuario con id " + idUsuario);
            
            // Evita que se borre el usuario si es un administrativo
            if (u.getRol() == TipoRol.ADMINISTRATIVO)
                throw new UsuarioServiceException("No se puede borrar un Usuario de tipo " + TipoRol.ADMINISTRATIVO.name());
            
            // Intenta borrar el usuario
            idao.delUsuario(idUsuario);
        }
        catch (UsuarioDAOException e)
        {
            throw new UsuarioServiceException("No se ha podido eliminar el usuario", e);
        }
    }

    /**
     * Obtiene todos los usuarios almacenados en la bbdd
     * @return Devuelve una lista con todos los usuarios
     * @throws UsuarioServiceException Error en la obtención de los usuarios
     */
    public List<Usuario> obtenerTodosUsuarios() throws UsuarioServiceException
    {
        try
        {
            return idao.getUsuarios();
        }
        catch (UsuarioDAOException e)
        {
            throw new UsuarioServiceException("No se han podido devolver los usuarios", e);
        }
    }

    /**
     * Obtiene todos los usuarios administrativos almacenados en la bbdd
     * @return Devuelve una lista con todos los usuarios administrativos
     * @throws UsuarioServiceException Error en la obtención de los usuarios administrativos
     */
    public List<Usuario> obtenerUsuariosAdministradores() throws UsuarioServiceException
    {
        try
        {
            List<Usuario> usuarios = idao.getUsuarios();
            List<Usuario> administradores = new ArrayList<>();

            for (Usuario usu : usuarios)
                if (usu.getRol() == TipoRol.ADMINISTRATIVO)
                    administradores.add(usu);
            
            return administradores;
        }
        catch (UsuarioDAOException e)
        {
            throw new UsuarioServiceException("No se ha podido obtener los usuarios administradores", e);
        }
    }

    /**
     * Obtiene el usuario correspondiente al id indicado
     * @param id Idenficador de usuario
     * @return Usuario con el id indicado
     * @throws UsuarioServiceException Error al obtener el usuario indicado
     */
    public Usuario obtenerUsuario(int id) throws UsuarioServiceException
    {
        try
        {
            return idao.getUsuario(id);
        }
        catch (UsuarioDAOException e)
        {
            throw new UsuarioServiceException("No se ha podido obtener el usuario", e);
        }
    }

    @Override
    public void finalizar() throws Exception
    {
        try
        {
            idao.finalizar();
        }
        catch (UsuarioDAOException e)
        {
            throw new UsuarioServiceException("Se ha producido un error al finalizar", e);
        }
    }
}
