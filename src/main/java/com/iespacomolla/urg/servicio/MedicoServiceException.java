/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.iespacomolla.urg.servicio;

/**
 *
 * @author ANDRES Angel
 */
public class MedicoServiceException extends Exception
{
    public MedicoServiceException(String mensaje)
    {
        super(mensaje);
    }
    public MedicoServiceException(String mensaje, Throwable causa)
    {
        super(mensaje, causa);
    }
}
