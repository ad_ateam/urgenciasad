package com.iespacomolla.urg.servicio;

import com.iespacomolla.urg.datos.MedicoDAO;
import com.iespacomolla.urg.datos.MedicoDAOException;
import com.iespacomolla.urg.entidades.EstadoMedico;
import com.iespacomolla.urg.entidades.Medico;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Saul
 */
public class MedicoService implements IService
{
    private MedicoDAO idao = null;

    public MedicoService() throws MedicoServiceException
    {
        try
        {
            idao = new MedicoDAO();
        }
        catch (MedicoDAOException e)
        {
            throw new MedicoServiceException("No se ha podido crear el servicio de usuarios", e);
        }
    }

    public int nuevoMedico(String nombre) throws MedicoServiceException
    {
        //Miro si el nombre tiene mas de 5 letras
        if (nombre.length() < 5)
        {
            throw new MedicoServiceException("El nombre introducido es demasiado corto");
        }

        try
        {
            return idao.addMedico(new Medico(0, nombre, EstadoMedico.FUERA_SERV));
        }
        catch (MedicoDAOException e)
        {
            throw new MedicoServiceException("No se ha podido crear el medico", e);
        }
    }

    public void actualizarMedico(Medico m) throws MedicoServiceException
    {
        try
        {
            actualizarMedico(m.getIdMedico(), m.getNombre(), m.getEstado().toString());
        }
        catch (MedicoServiceException e)
        {
            throw e;
        }
    }
    public void actualizarMedico(int id, String nombre, String estado) throws MedicoServiceException
    {

        Medico m = null;
        try
        {
            // Pide al DAO el medico con el id indicado
            m = idao.getMedico(id);
        }
        catch (MedicoDAOException e)
        {
            throw new MedicoServiceException("No se ha podido obtener el medico con id " + id, e);
        }

        // Si no existe el medico lanza una excepcion
        if (m == null)
        {
            throw new MedicoServiceException("No existe ningun medico con id " + id);
        }

        if (nombre.length() < 5)
        {
            throw new MedicoServiceException("El nombre introducido es demasiado corto");
        }

        //Miro si se mete un estado valido
        try
        {
            m.setEstado(EstadoMedico.valueOf(estado));
        }
        catch (Exception e)
        {
            throw new MedicoServiceException("El estado introducido no es valido", e);
        }

        try
        {
            m.setNombre(nombre);
            idao.updateMedico(m);
        }
        catch (Exception e)
        {
            throw new MedicoServiceException("No se ha podido actualizar el medico", e);
        }
    }

    public void eliminaMedico(int idMedico) throws MedicoServiceException
    {
        try
        {
            // Busca el medico con el id indicado
            Medico m = idao.getMedico(idMedico);

            // Si el medico no existe lanza un error
            if (m == null)
            {
                throw new MedicoServiceException("No existe un medico con id " + idMedico);
            }
            //Si el usuario esta OCUPADO no lo borra
            if (m.getEstado() == EstadoMedico.OCUPADO)
            {
                throw new MedicoServiceException("El medico esta ocupado y no se puede borrar");
            }
            //Borro el medico
            idao.delMedico(idMedico);

        }
        catch (MedicoDAOException e)
        {
            throw new MedicoServiceException("No se ha podido eliminar el medico", e);
        }

    }

    public Medico ObtenerMedico(int idMedico) throws MedicoServiceException
    {
        try
        {
            return idao.getMedico(idMedico);
        }
        catch (MedicoDAOException e)
        {
            throw new MedicoServiceException("No se ha podido obtener el medico con id " + idMedico, e);
        }
    }

    public List<Medico> ObtenerTodosMedicos() throws MedicoServiceException
    {
        try
        {
            /*------CODIGO ANTIGUO-------
             List<Medico> getMedicos = idao.getMedicos();

             if (getMedicos == null) {
             getMedicos = new ArrayList<>();
             }
             return getMedicos;
             */
            return idao.getMedicos();
        }
        catch (MedicoDAOException e)
        {
            throw new MedicoServiceException("No se han podido devolver los medicos ", e);
        }
    }

    public List<Medico> ObtenerMedicosServicio() throws MedicoServiceException
    {
        try
        {
            List<Medico> medicos = idao.getMedicos();
            List<Medico> enServicio = new ArrayList<>();
            
            for (Medico m : medicos)
                if (m.getEstado() == EstadoMedico.DISPONIBLE || m.getEstado() == EstadoMedico.OCUPADO)
                    enServicio.add(m);
                    
            return enServicio;
        }
        catch (MedicoDAOException e)
        {
            throw new MedicoServiceException("No se ha podido obtener los medicos en servicio", e);
        }
    }
    
        public List<Medico> ObtenerMedicosDisponibles() throws MedicoServiceException
    {
        try
        {
            List<Medico> medicos = idao.getMedicosDisponibles();
            List<Medico> medicosDisponibles = new ArrayList<>();
            
            for (Medico m : medicos)
                if (m.getEstado() == EstadoMedico.DISPONIBLE)
                    medicosDisponibles.add(m);
                    
            return medicosDisponibles;
        }
        catch (MedicoDAOException e)
        {
            throw new MedicoServiceException("No se ha podido obtener los medicos en servicio", e);
        }
    }

    @Override
    public void finalizar() throws Exception
    {
        try
        {
            idao.finalizar();
        }
        catch (MedicoDAOException e)
        {
            throw new MedicoServiceException("Se ha producido un error al finalizar", e);
        }
    }
}
