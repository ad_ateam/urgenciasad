/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.iespacomolla.urg.servicio;

/**
 *
 * @author Saul
 */
public class UsuarioServiceException extends Exception
{
    public UsuarioServiceException (String mensaje)
    {
        super(mensaje);
    }
    public UsuarioServiceException (String mensaje, Exception causa)
    {
        super(mensaje, causa);
    }
}
