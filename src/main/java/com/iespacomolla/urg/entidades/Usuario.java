package com.iespacomolla.urg.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ANDRES Angel
 */
@Entity
@Table(name="Usuarios")
public class Usuario implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue
    private int id;
    String nombre;
    String usu;
    String pwd;
    TipoRol rol;

    public Usuario()
    {
        this(0, "", "", "", null);
    }
    public Usuario(int id, String nombre, String usu, String pwd, TipoRol rol)
    {
        this.id = id;
        this.nombre = nombre;
        this.usu = usu;
        this.pwd = pwd;
        this.rol = rol;
    }
    
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }

    public String getUsu() { return usu; }
    public void setUsu(String usu) { this.usu = usu; }

    public String getPwd() { return pwd; }
    public void setPwd(String pwd) { this.pwd = pwd; }

    public TipoRol getRol() { return rol; }
    public void setRol(TipoRol rol) { this.rol = rol; }
}
