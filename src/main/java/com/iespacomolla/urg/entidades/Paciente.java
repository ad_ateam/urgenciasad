package com.iespacomolla.urg.entidades;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Adrian
 */
@Entity
@Table(name="Pacientes")
public class Paciente implements Serializable
{
    private static final long serialVersionUID = 3L;
    
    @Id
    @GeneratedValue
    private int id;
    String nombre;
    String nSegSoc;
    @OneToMany
    List<Incidencia> incidencias;

    public Paciente()
    {
        this(0, "", "",null);
    }
    public Paciente(int id, String nombre, String nSegSoc, List<Incidencia> incidencias)
    {
        this.id = id;
        this.nombre = nombre;
        this.nSegSoc = nSegSoc;
        this.incidencias= incidencias;
    }
    
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }
    
    public String getNSegSoc() { return nSegSoc; }
    public void setNSegSoc(String nSegSoc) { this.nSegSoc = nSegSoc; }

    public List<Incidencia> getIncidencias(){ return incidencias; }
    public void setIncidencias(List<Incidencia> incidencias){ this.incidencias = incidencias; }
}
