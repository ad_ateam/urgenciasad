/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iespacomolla.urg.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Propietario
 */
@Entity
@Table(name="Incidencias")
public class Incidencia implements Serializable
{   
    private static final long serialVersionUID = 4L; 
    
    @Id
    @GeneratedValue
    private int idIncidencia;
    private String descripcion;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEntrada;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAtencion;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSalida;
    private EstadoIncidencia estado;
    @ManyToOne
    private Medico medico;
    @ManyToOne
    private Paciente paciente;
    private TipoIncidencia tipo;
    private int numEspera;
  
    public Incidencia()
    {
        this(0,"",null,null,null,null,null,null,null,0);
    }
    
    public Incidencia(int idIncidencia, String descripcion, Date fechaEntrada, Date fechaAtencion, 
    Date fechaSalida, EstadoIncidencia estado, Medico medico, Paciente paciente, TipoIncidencia tipo, int numEspera)
    {
        this.idIncidencia = idIncidencia;
        this.descripcion = descripcion;
        this.fechaEntrada = fechaEntrada;
        this.fechaAtencion = fechaAtencion;
        this.fechaSalida = fechaSalida;
        this.estado = estado;
        this.medico = medico;
        this.paciente = paciente;
        this.tipo = tipo;
        this.numEspera = numEspera;
    }    
    
    public int getIdIncidencia()
    {
        return idIncidencia;
    }

    public void setIdIncidencia(int idIncidencia)
    {
        this.idIncidencia = idIncidencia;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Date getFechaEntrada()
    {
        return fechaEntrada;
    }

    public void setFechaEntrada(Date fechaEntrada)
    {
        this.fechaEntrada = fechaEntrada;
    }

    public Date getFechaAtencion()
    {
        return fechaAtencion;
    }

    public void setFechaAtencion(Date fechaAtencion)
    {
        this.fechaAtencion = fechaAtencion;
    }

    public Date getFechaSalida()
    {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida)
    {
        this.fechaSalida = fechaSalida;
    }

    public EstadoIncidencia getEstado()
    {
        return estado;
    }

    public void setEstado(EstadoIncidencia estado)
    {
        this.estado = estado;
    }

    public Medico getMedico()
    {
        return medico;
    }

    public void setMedico(Medico medico)
    {
        this.medico = medico;
    }

    public Paciente getPaciente()
    {
        return paciente;
    }

    public void setPaciente(Paciente paciente)
    {
        this.paciente = paciente;
    }

    public TipoIncidencia getTipo()
    {
        return tipo;
    }

    public void setTipo(TipoIncidencia tipo)
    {
        this.tipo = tipo;
    }

    public int getNumEspera()
    {
        return numEspera;
    }

    public void setNumEspera(int numEspera)
    {
        this.numEspera = numEspera;
    }
    
}
