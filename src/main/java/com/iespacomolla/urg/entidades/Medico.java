/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iespacomolla.urg.entidades;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Propietario
 */
@Entity
@Table(name="Medicos")
public class Medico implements Serializable
{
   private static final long serialVersionUID = 2L; 
    
   @Id
   @GeneratedValue
   private int idMedico;
   private String nombre;
   private EstadoMedico estado;
   
   public Medico()
   {
       this(0, "" , EstadoMedico.FUERA_SERV);
   }
   
   public Medico(int idMedico, String nombre, EstadoMedico estado)
   {
       this.idMedico = idMedico;
       this.nombre = nombre;
       this.estado = estado;
   }
   
   public int getIdMedico()
   {
        return idMedico;
   }

   public void setIdMedico(int idMedico)
   {
        this.idMedico = idMedico;
   }

   public String getNombre()
   {
        return nombre;
   }

   public void setNombre(String nombre)
   {
        this.nombre = nombre;
   }

   public EstadoMedico getEstado()
   {
        return estado;
   }

   public void setEstado(EstadoMedico estado)
   {
        this.estado = estado;
   }
   
}
