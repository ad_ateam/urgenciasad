package com.iespacomolla.urg.entidades;

/**
 * Enumera los tipos de rol de Usuarios
 * @author ANDRES Angel
 */
public enum TipoRol
{
    ADMINISTRATIVO,
    SANITARIO,
    DIRECTIVO
}
