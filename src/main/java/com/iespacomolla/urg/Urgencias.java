/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.iespacomolla.urg;

import com.iespacomolla.urg.entidades.Usuario;
import com.iespacomolla.urg.servicio.MedicoServiceException;
import com.iespacomolla.urg.servicio.PacienteServiceException;
import com.iespacomolla.urg.servicio.UrgenciasServiceException;
import com.iespacomolla.urg.servicio.UsuarioServiceException;
import com.iespacomolla.urg.vistas.Login;
import com.iespacomolla.urg.vistas.LoginException;
import com.iespacomolla.urg.vistas.PrincipalAdministrativo;
import com.iespacomolla.urg.vistas.PrincipalDirectivo;
import com.iespacomolla.urg.vistas.PrincipalSanitario;
import com.iespacomolla.urg.vistas.VistaBase;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ANDRES Angel
 */
public class Urgencias
{
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        startApp();
        
        VistaBase.close();
    }
     
    private static void startApp()
    {
        Login l = null;
        boolean salir = false;
        
        try
        {
            l = new Login();
        }
        catch (LoginException e)
        {
            System.err.println("Error de Login: ");
            e.printStackTrace();    
            salir = true;
        }
        
        while (!salir)
        {
            try
            {
                l.mostrarMenu();
                if (l.Salir)
                    salir = true;
                else if (l.UsuarioLogueado != null)
                {
                    switch (l.UsuarioLogueado.getRol())
                    {
                        case ADMINISTRATIVO:
                            PrincipalAdministrativo pa = new PrincipalAdministrativo();
                            pa.mostrarMenu();
                            break;
                        case SANITARIO:
                            PrincipalSanitario ps = new PrincipalSanitario();
                            ps.mostrarMenu();
                            break;
                        case DIRECTIVO:
                            PrincipalDirectivo pd = new PrincipalDirectivo();
                            pd.mostrarMenu();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                l.mostrarError(ex);
            }
        } 
        
        System.out.println("FIN DE PROGRAMA");
    }
    
    private static void TestPpalAdmin()
    {
        try
        {
            PrincipalAdministrativo pa = new PrincipalAdministrativo();
            pa.mostrarMenu();
        }
        catch (MedicoServiceException | UsuarioServiceException e)
        {
            e.printStackTrace();
        }
    }
    
    private static void TestPpalSanitario()
    {
        PrincipalSanitario ps;
        try
        {
            ps = new PrincipalSanitario();
            ps.mostrarMenu();
        }
        catch (UsuarioServiceException | MedicoServiceException | UrgenciasServiceException | PacienteServiceException ex)
        {
            ex.printStackTrace();
            //Logger.getLogger(Urgencias.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    private static void TestPpalDirectivo()
    {
        PrincipalDirectivo pd;
        try
        {
            pd = new PrincipalDirectivo();
            pd.mostrarMenu();
        }
        catch (UrgenciasServiceException | MedicoServiceException | PacienteServiceException ex)
        {
            ex.printStackTrace();
            //Logger.getLogger(Urgencias.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
